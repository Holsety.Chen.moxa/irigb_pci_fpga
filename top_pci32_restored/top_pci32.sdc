## Generated SDC file "top_pci32.sdc"

## Copyright (C) 1991-2013 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Full Version"

## DATE    "Thu Mar 13 16:36:52 2014"

##
## DEVICE  "EP4CE10F17C8"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {PCI_CLK} -period 14.000 -waveform { 0.000 7.000 } [get_ports {clk}]
create_clock -name {SYS_CLK} -period 40.000 -waveform { 0.000 20.000 } [get_ports {osc_clk1}]


#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {SYS_CLK}] -rise_to [get_clocks {SYS_CLK}]  0.150  
set_clock_uncertainty -rise_from [get_clocks {SYS_CLK}] -fall_to [get_clocks {SYS_CLK}]  0.150  
set_clock_uncertainty -fall_from [get_clocks {SYS_CLK}] -fall_to [get_clocks {SYS_CLK}]  0.150  
set_clock_uncertainty -rise_from [get_clocks {PCI_CLK}] -rise_to [get_clocks {PCI_CLK}]  0.150  
set_clock_uncertainty -rise_from [get_clocks {PCI_CLK}] -fall_to [get_clocks {PCI_CLK}]  0.150  
set_clock_uncertainty -fall_from [get_clocks {PCI_CLK}] -fall_to [get_clocks {PCI_CLK}]  0.150  


#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[0]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[1]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[2]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[3]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[4]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[5]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[6]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[7]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[8]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[9]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[10]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[11]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[12]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[13]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[14]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[15]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[16]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[17]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[18]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[19]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[20]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[21]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[22]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[23]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[24]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[25]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[26]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[27]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[28]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[29]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[30]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {ad[31]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {cben[0]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {cben[1]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {cben[2]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {cben[3]}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {framen}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {idsel}]
set_input_delay -add_delay  -clock [get_clocks {SYS_CLK}]  3.000 [get_ports {inport0_i}]
set_input_delay -add_delay  -clock [get_clocks {SYS_CLK}]  3.000 [get_ports {inport1_i}]
set_input_delay -add_delay  -clock [get_clocks {SYS_CLK}]  3.000 [get_ports {inport2_i}]
set_input_delay -add_delay  -clock [get_clocks {SYS_CLK}]  3.000 [get_ports {inport3_i}]
set_input_delay -add_delay  -clock [get_clocks {SYS_CLK}]  3.000 [get_ports {inport4_i}]
set_input_delay -add_delay  -clock [get_clocks {SYS_CLK}]  3.000 [get_ports {inport5_i}]
set_input_delay -add_delay  -clock [get_clocks {SYS_CLK}]  3.000 [get_ports {inport6_i}]
set_input_delay -add_delay  -clock [get_clocks {SYS_CLK}]  3.000 [get_ports {inport7_i}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {irdyn}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {par}]
set_input_delay -add_delay  -clock [get_clocks {PCI_CLK}]  2.000 [get_ports {rstn}]


#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[0]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[1]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[2]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[3]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[4]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[5]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[6]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[7]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[8]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[9]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[10]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[11]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[12]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[13]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[14]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[15]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[16]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[17]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[18]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[19]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[20]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[21]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[22]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[23]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[24]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[25]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[26]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[27]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[28]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[29]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[30]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {ad[31]}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {devseln}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {intan}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {par}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {perrn}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {serrn}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {stopn}]
set_output_delay -add_delay  -clock [get_clocks {PCI_CLK}]  3.000 [get_ports {trdyn}]


#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -exclusive -group [get_clocks {PCI_CLK}] 
set_clock_groups -exclusive -group [get_clocks {SYS_CLK}] 

#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

