# Copyright (C) 1991-2004 Altera Corporation
# Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
# support information,  device programming or simulation file,  and any other
# associated  documentation or information  provided by  Altera  or a partner
# under  Altera's   Megafunction   Partnership   Program  may  be  used  only
# to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
# other  use  of such  megafunction  design,  netlist,  support  information,
# device programming or simulation file,  or any other  related documentation
# or information  is prohibited  for  any  other purpose,  including, but not
# limited to  modification,  reverse engineering,  de-compiling, or use  with
# any other  silicon devices,  unless such use is  explicitly  licensed under
# a separate agreement with  Altera  or a megafunction partner.  Title to the
# intellectual property,  including patents,  copyrights,  trademarks,  trade
# secrets,  or maskworks,  embodied in any such megafunction design, netlist,
# support  information,  device programming or simulation file,  or any other
# related documentation or information provided by  Altera  or a megafunction
# partner, remains with Altera, the megafunction partner, or their respective
# licensors. No other licenses, including any licenses needed under any third
# party's intellectual property, are provided herein.

#############################################################################
# PCI NATIVELINK FILE
#
# Purposes:
# A tcl script to assign NativeLink simulation testbench settings 
# to the Quartus project
#
#
# To use this NativeLink setup file:
# source <instance_name>_nativelink.tcl
#
##############################################################################

package require ::quartus::project

#------------------------------------------------------------------------------
# Project details, which is replaced by the generator 
#------------------------------------------------------------------------------
set proj_dir "D:/Moxa/project_PCI_IRIG/FPGA/IRIG_Board_cycloneVI/altera_ip"
set instance_name "t32"
set pci_language "verilog"
set pci_variant "pci_t32"

set testbench_name [set instance_name]_[set pci_variant]_tb
set nlink_sim_folder [set instance_name]_nativelink

#------------------------------------------------------------------------------
# EDA netlist writer options 
#------------------------------------------------------------------------------
set_global_assignment -name eda_output_data_format VERILOG -section_id eda_simulation
set_global_assignment -name eda_time_scale "1 ps" -section_id eda_simulation

#------------------------------------------------------------------------------
# Switch to test bench mode 
#------------------------------------------------------------------------------
set_global_assignment -name eda_test_bench_enable_status test_bench_mode -section_id eda_simulation

#------------------------------------------------------------------------------
# Testbench top level name and module name 
#------------------------------------------------------------------------------
set_global_assignment -name eda_test_bench_name $testbench_name -section_id eda_simulation
set_global_assignment -name eda_test_bench_module_name altera_tb -section_id $testbench_name

#------------------------------------------------------------------------------
# Design Instance Name 
#------------------------------------------------------------------------------
set_global_assignment -name eda_design_instance_name $instance_name -section_id $testbench_name

#------------------------------------------------------------------------------
# Duration of test bench simulation 
#------------------------------------------------------------------------------
set_global_assignment -name eda_test_bench_run_sim_for 10ms -section_id $testbench_name

#------------------------------------------------------------------------------
# Compile IP functional simulation model
#------------------------------------------------------------------------------
# set_global_assignment -name eda_test_bench_file $proj_dir/$instance_name.vo -section_id $testbench_name

#--------------------------------------------------------------------------------
# Altera Testbench Components, refer to PCI Testbench User guide for more information 
#--------------------------------------------------------------------------------
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/tb_src/clk_gen.v -section_id $testbench_name
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/tb_src/pull_up.v -section_id $testbench_name
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/tb_src/arbiter.v -section_id $testbench_name
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/tb_src/monitor.v -section_id $testbench_name
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/tb_src/trgt_tranx.v -section_id $testbench_name
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/tb_src/mstr_tranx.v -section_id $testbench_name
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/tb_src/trgt_tranx_mem_init.dat  -section_id $testbench_name

#--------------------------------------------------------------------------------
# Local Simple Reference Design
#--------------------------------------------------------------------------------
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/local_bfm/lpm_ram_32.v -section_id $testbench_name
#set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/local_bfm/dma.v -section_id $testbench_name
#set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/local_bfm/lm_lastn_gen.v -section_id $testbench_name
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/local_bfm/prefetch.v -section_id $testbench_name
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/local_bfm/local_target.v -section_id $testbench_name
#set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/local_bfm/local_master.v -section_id $testbench_name
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/local_bfm/top_local.v -section_id $testbench_name

#--------------------------------------------------------------------------------
# Top level file
#--------------------------------------------------------------------------------
set_global_assignment -name eda_test_bench_file $proj_dir/$nlink_sim_folder/$pci_language/$pci_variant/tb_src/altera_tb.v -section_id $testbench_name


