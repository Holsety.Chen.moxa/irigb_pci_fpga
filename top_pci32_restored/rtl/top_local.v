`include "define.v"

module top_local (
   output wire [31:0]          l_adi,
   output wire                 lt_rdyn,
 
   // SRAM Interface signals 
   output wire                 sram0_wen_o,  // Write Enable
   output wire                 sram0_ren_o,  // Read enable
   output reg  [31:0]          sram0_din_o,  // write data
   output wire [`BAR0_ADW-1:0] sram0_adr_o,  // sram address 4k

   input  wire [31:0]          bar0_dout_i,   
   input  wire [31:0]          l_adro,
   input  wire [31:0]          l_dato,
   input  wire [3:0]           l_cmdo,
   input  wire                 lt_framen,
   input  wire                 lt_ackn,
   input  wire                 lt_dxfrn,
   input  wire [11:0]          lt_tsr,
   
   input  wire                 clk,
   input  wire                 rstn
   ) ;

// Determine the start of a cycle
   reg  lt_framen_q;
   always @ (posedge clk, negedge rstn)
     if (~rstn) lt_framen_q <= 1'b1;
     else       lt_framen_q <= lt_framen;

   wire v_cycle_start = lt_framen_q & ~lt_framen;
   wire bar0_hit      = lt_tsr[0];

// 0:Read / 1:Write Signals
   reg  write_readn;
   always @ (posedge clk) write_readn <= l_cmdo[0];

// Drive the data on the l_adi bus
   assign l_adi = bar0_hit ? bar0_dout_i : 32'd0;

   reg  [31:0] write_data;
   always @ (posedge clk) write_data  <= l_dato;
   always @ (posedge clk) sram0_din_o <= write_data;

   wire bar0_lt_rdyn;
   assign lt_rdyn = bar0_lt_rdyn;
   
pci_sram_ctrl bar0_ctrl(
   .lt_rdyn_o     (bar0_lt_rdyn),
   .sram_wen_o    (sram0_wen_o),
   .sram_ren_o    (sram0_ren_o),
   .sram_adr_o    (sram0_adr_o),

   .l_adro        (l_adro       ),         
   .lt_framen     (lt_framen    ),
   .lt_framen_q   (lt_framen_q  ),   
   .lt_ackn       (lt_ackn      ),        
   .lt_dxfrn      (lt_dxfrn     ),       
   .cycle_start   (v_cycle_start),
   .bar_hit       (bar0_hit     ),        
   .write_readn   (write_readn  ),    

   .clk           (clk ),            
   .rstn          (rstn)  
   ) ;
   
   
   
   

endmodule
