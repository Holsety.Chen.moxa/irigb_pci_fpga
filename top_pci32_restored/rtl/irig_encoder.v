`include "define.v"

module  irig_encoder
(
   output reg                  encode_irig_o,
   output reg                  encirig_end_o,

   input  wire                 enc_set_par_i,
   input  wire                 rtc_irigrdy_i,
   input  wire [88:0]          rtc_irigdat_i,
   input  wire                 sysclk_i,
   input  wire                 rstn_i
);

// IRIG Encoder Generator
  reg  [19:0]   bit_widcnt;      // One Bit Width Counter
  reg  [ 7:0]   bit_number;      // Bit Address
  
  wire v_bit_ending  = (bit_widcnt == `ONEBIT_UNIT);
  wire v_encirig_end = (bit_widcnt == `ONEBIT_UNIT - 1'b1) && (bit_number == 8'd99); 
  
  reg    encirig_done;           // IRIG Encoder Busy
  wire v_encirig_done = rtc_irigrdy_i ? 1'b1 : v_encirig_end ? 1'b0 : encirig_done;
					  
  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) encirig_done <= 1'b0;
    else         encirig_done <= v_encirig_done;

  wire [19:0] v_bit_widcnt = (rtc_irigrdy_i | v_bit_ending | ~encirig_done) ? 20'd0 
                           : (encirig_done ? bit_widcnt + 20'd1 : bit_widcnt); 	

  wire [ 7:0] v_bit_number = (rtc_irigrdy_i | ~encirig_done) ? 8'd0 
                           : (v_bit_ending ? bit_number + 8'd1 : bit_number);

  always @ (posedge sysclk_i, negedge rstn_i) 
    if (~rstn_i) begin
	             bit_widcnt <= 20'd0;
	             bit_number <=  8'd0;
	end	 
    else begin
	             bit_widcnt <= v_bit_widcnt;	
	             bit_number <= v_bit_number;
    end

  reg  encirig_end;	
  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) begin
	encirig_end   <= 1'b0;
	encirig_end_o <= 1'b0;
    end
	else begin
	encirig_end   <= v_encirig_end;
	encirig_end_o <= encirig_end;
    end
	
// RTC IRIG Buffer
  reg  [88:0]   rtc_irigbuf;
  wire [88:0] v_rtc_irigbuf  = rtc_irigrdy_i ? rtc_irigdat_i : rtc_irigbuf;
  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) rtc_irigbuf <= 89'd0;
    else         rtc_irigbuf <= v_rtc_irigbuf;

  wire v_bitp_format = encirig_done & (bit_widcnt < `POSBIT_UNIT );
  wire v_bit1_format = encirig_done & (bit_widcnt < `HIGHBIT_UNIT);
  wire v_bit0_format = encirig_done & (bit_widcnt < `LOWBIT_UNIT );

  // IRIG Encoder Parity Bit
  reg    enc_irig_par;
  wire v_enc_irig_par = enc_set_par_i ? (~^rtc_irigbuf[66:0]) : (^rtc_irigbuf[66:0]);
  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) enc_irig_par <= 1'b0;
    else         enc_irig_par <= v_enc_irig_par;

  reg  v_encode_irig_o;
  always @ (*) begin
    case(bit_number)
      8'd0 , 8'd9 , 8'd19, 8'd29, 8'd39,
      8'd49, 8'd59, 8'd69, 8'd79, 8'd89,
      8'd99 : v_encode_irig_o = v_bitp_format;

      8'd1  : v_encode_irig_o = rtc_irigbuf[0 ] ? v_bit1_format : v_bit0_format;
      8'd2  : v_encode_irig_o = rtc_irigbuf[1 ] ? v_bit1_format : v_bit0_format;
      8'd3  : v_encode_irig_o = rtc_irigbuf[2 ] ? v_bit1_format : v_bit0_format;
      8'd4  : v_encode_irig_o = rtc_irigbuf[3 ] ? v_bit1_format : v_bit0_format;
      8'd5  : v_encode_irig_o = rtc_irigbuf[4 ] ? v_bit1_format : v_bit0_format;
      8'd6  : v_encode_irig_o = rtc_irigbuf[5 ] ? v_bit1_format : v_bit0_format;
      8'd7  : v_encode_irig_o = rtc_irigbuf[6 ] ? v_bit1_format : v_bit0_format;
      8'd8  : v_encode_irig_o = rtc_irigbuf[7 ] ? v_bit1_format : v_bit0_format;

      8'd10 : v_encode_irig_o = rtc_irigbuf[8 ] ? v_bit1_format : v_bit0_format;
      8'd11 : v_encode_irig_o = rtc_irigbuf[9 ] ? v_bit1_format : v_bit0_format;
      8'd12 : v_encode_irig_o = rtc_irigbuf[10] ? v_bit1_format : v_bit0_format;
      8'd13 : v_encode_irig_o = rtc_irigbuf[11] ? v_bit1_format : v_bit0_format;
      8'd14 : v_encode_irig_o = rtc_irigbuf[12] ? v_bit1_format : v_bit0_format;
      8'd15 : v_encode_irig_o = rtc_irigbuf[13] ? v_bit1_format : v_bit0_format;
      8'd16 : v_encode_irig_o = rtc_irigbuf[14] ? v_bit1_format : v_bit0_format;
      8'd17 : v_encode_irig_o = rtc_irigbuf[15] ? v_bit1_format : v_bit0_format;
      8'd18 : v_encode_irig_o = rtc_irigbuf[16] ? v_bit1_format : v_bit0_format;

      8'd20 : v_encode_irig_o = rtc_irigbuf[17] ? v_bit1_format : v_bit0_format;
      8'd21 : v_encode_irig_o = rtc_irigbuf[18] ? v_bit1_format : v_bit0_format;
      8'd22 : v_encode_irig_o = rtc_irigbuf[19] ? v_bit1_format : v_bit0_format;
      8'd23 : v_encode_irig_o = rtc_irigbuf[20] ? v_bit1_format : v_bit0_format;
      8'd24 : v_encode_irig_o = rtc_irigbuf[21] ? v_bit1_format : v_bit0_format;
      8'd25 : v_encode_irig_o = rtc_irigbuf[22] ? v_bit1_format : v_bit0_format;
      8'd26 : v_encode_irig_o = rtc_irigbuf[23] ? v_bit1_format : v_bit0_format;
      8'd27 : v_encode_irig_o = rtc_irigbuf[24] ? v_bit1_format : v_bit0_format;
      8'd28 : v_encode_irig_o = rtc_irigbuf[25] ? v_bit1_format : v_bit0_format;

      8'd30 : v_encode_irig_o = rtc_irigbuf[26] ? v_bit1_format : v_bit0_format;
      8'd31 : v_encode_irig_o = rtc_irigbuf[27] ? v_bit1_format : v_bit0_format;
      8'd32 : v_encode_irig_o = rtc_irigbuf[28] ? v_bit1_format : v_bit0_format;
      8'd33 : v_encode_irig_o = rtc_irigbuf[29] ? v_bit1_format : v_bit0_format;
      8'd34 : v_encode_irig_o = rtc_irigbuf[30] ? v_bit1_format : v_bit0_format;
      8'd35 : v_encode_irig_o = rtc_irigbuf[31] ? v_bit1_format : v_bit0_format;
      8'd36 : v_encode_irig_o = rtc_irigbuf[32] ? v_bit1_format : v_bit0_format;
      8'd37 : v_encode_irig_o = rtc_irigbuf[33] ? v_bit1_format : v_bit0_format;
      8'd38 : v_encode_irig_o = rtc_irigbuf[34] ? v_bit1_format : v_bit0_format;

      8'd40 : v_encode_irig_o = rtc_irigbuf[35] ? v_bit1_format : v_bit0_format;
      8'd41 : v_encode_irig_o = rtc_irigbuf[36] ? v_bit1_format : v_bit0_format;
      8'd42 : v_encode_irig_o = rtc_irigbuf[37] ? v_bit1_format : v_bit0_format;
      8'd43 : v_encode_irig_o = rtc_irigbuf[38] ? v_bit1_format : v_bit0_format;
      8'd44 : v_encode_irig_o = rtc_irigbuf[39] ? v_bit1_format : v_bit0_format;
      8'd45 : v_encode_irig_o = rtc_irigbuf[40] ? v_bit1_format : v_bit0_format;
      8'd46 : v_encode_irig_o = rtc_irigbuf[41] ? v_bit1_format : v_bit0_format;
      8'd47 : v_encode_irig_o = rtc_irigbuf[42] ? v_bit1_format : v_bit0_format;
      8'd48 : v_encode_irig_o = rtc_irigbuf[43] ? v_bit1_format : v_bit0_format;

      8'd50 : v_encode_irig_o = rtc_irigbuf[44] ? v_bit1_format : v_bit0_format;
      8'd51 : v_encode_irig_o = rtc_irigbuf[45] ? v_bit1_format : v_bit0_format;
      8'd52 : v_encode_irig_o = rtc_irigbuf[46] ? v_bit1_format : v_bit0_format;
      8'd53 : v_encode_irig_o = rtc_irigbuf[47] ? v_bit1_format : v_bit0_format;
      8'd54 : v_encode_irig_o = rtc_irigbuf[48] ? v_bit1_format : v_bit0_format;
      8'd55 : v_encode_irig_o = rtc_irigbuf[49] ? v_bit1_format : v_bit0_format;
      8'd56 : v_encode_irig_o = rtc_irigbuf[50] ? v_bit1_format : v_bit0_format;
      8'd57 : v_encode_irig_o = rtc_irigbuf[51] ? v_bit1_format : v_bit0_format;
      8'd58 : v_encode_irig_o = rtc_irigbuf[52] ? v_bit1_format : v_bit0_format;

      8'd60 : v_encode_irig_o = rtc_irigbuf[53] ? v_bit1_format : v_bit0_format;
      8'd61 : v_encode_irig_o = rtc_irigbuf[54] ? v_bit1_format : v_bit0_format;
      8'd62 : v_encode_irig_o = rtc_irigbuf[55] ? v_bit1_format : v_bit0_format;
      8'd63 : v_encode_irig_o = rtc_irigbuf[56] ? v_bit1_format : v_bit0_format;
      8'd64 : v_encode_irig_o = rtc_irigbuf[57] ? v_bit1_format : v_bit0_format;
      8'd65 : v_encode_irig_o = rtc_irigbuf[58] ? v_bit1_format : v_bit0_format;
      8'd66 : v_encode_irig_o = rtc_irigbuf[59] ? v_bit1_format : v_bit0_format;
      8'd67 : v_encode_irig_o = rtc_irigbuf[60] ? v_bit1_format : v_bit0_format;
      8'd68 : v_encode_irig_o = rtc_irigbuf[61] ? v_bit1_format : v_bit0_format;

      8'd70 : v_encode_irig_o = rtc_irigbuf[62] ? v_bit1_format : v_bit0_format;
      8'd71 : v_encode_irig_o = rtc_irigbuf[63] ? v_bit1_format : v_bit0_format;
      8'd72 : v_encode_irig_o = rtc_irigbuf[64] ? v_bit1_format : v_bit0_format;
      8'd73 : v_encode_irig_o = rtc_irigbuf[65] ? v_bit1_format : v_bit0_format;
      8'd74 : v_encode_irig_o = rtc_irigbuf[66] ? v_bit1_format : v_bit0_format;
      8'd75 : v_encode_irig_o = enc_irig_par    ? v_bit1_format : v_bit0_format;
      8'd76 : v_encode_irig_o = rtc_irigbuf[68] ? v_bit1_format : v_bit0_format;
      8'd77 : v_encode_irig_o = rtc_irigbuf[69] ? v_bit1_format : v_bit0_format;
      8'd78 : v_encode_irig_o = rtc_irigbuf[70] ? v_bit1_format : v_bit0_format;

      8'd80 : v_encode_irig_o = rtc_irigbuf[71] ? v_bit1_format : v_bit0_format;
      8'd81 : v_encode_irig_o = rtc_irigbuf[72] ? v_bit1_format : v_bit0_format;
      8'd82 : v_encode_irig_o = rtc_irigbuf[73] ? v_bit1_format : v_bit0_format;
      8'd83 : v_encode_irig_o = rtc_irigbuf[74] ? v_bit1_format : v_bit0_format;
      8'd84 : v_encode_irig_o = rtc_irigbuf[75] ? v_bit1_format : v_bit0_format;
      8'd85 : v_encode_irig_o = rtc_irigbuf[76] ? v_bit1_format : v_bit0_format;
      8'd86 : v_encode_irig_o = rtc_irigbuf[77] ? v_bit1_format : v_bit0_format;
      8'd87 : v_encode_irig_o = rtc_irigbuf[78] ? v_bit1_format : v_bit0_format;
      8'd88 : v_encode_irig_o = rtc_irigbuf[79] ? v_bit1_format : v_bit0_format;

      8'd90 : v_encode_irig_o = rtc_irigbuf[80] ? v_bit1_format : v_bit0_format;
      8'd91 : v_encode_irig_o = rtc_irigbuf[81] ? v_bit1_format : v_bit0_format;
      8'd92 : v_encode_irig_o = rtc_irigbuf[82] ? v_bit1_format : v_bit0_format;
      8'd93 : v_encode_irig_o = rtc_irigbuf[83] ? v_bit1_format : v_bit0_format;
      8'd94 : v_encode_irig_o = rtc_irigbuf[84] ? v_bit1_format : v_bit0_format;
      8'd95 : v_encode_irig_o = rtc_irigbuf[85] ? v_bit1_format : v_bit0_format;
      8'd96 : v_encode_irig_o = rtc_irigbuf[86] ? v_bit1_format : v_bit0_format;
      8'd97 : v_encode_irig_o = rtc_irigbuf[87] ? v_bit1_format : v_bit0_format;
      8'd98 : v_encode_irig_o = rtc_irigbuf[88] ? v_bit1_format : v_bit0_format;
    default : v_encode_irig_o = 1'b0;
    endcase
  end

  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) encode_irig_o <= 1'b0;
    else         encode_irig_o <= v_encode_irig_o;

endmodule
