// ==================================================================
// FILE :
//       irig_decoder.v
// DESCRIPTION:
//       This is the IRIG-b module decoder
// Author:
//       nick.kuan@gfec.com.tw
// Revision History:
//       2014/01/20 : first veriosn
//
// ==================================================================

`include "define.v"

module  irig_decoder #(
        parameter SRAM_ORDER = 2'B00
)
(
   output reg  [ 3:0] irig_drdy_o,  // Write sram order 0~3
   output reg  [88:0] irig_data_o,
   output reg  [31:0] irig_fcnt_o,  // One of total IRIG format cnt
   output reg         irig_fine_o,

   output reg         irig_off_o ,  // Irig off line
   output reg         irig_fair_o,
   output reg         parity_err_o,
   output reg         irig_right_o, // Irig complete

   input  wire [ 1:0] ram_order_i,
   input  wire        parity_set_i, // 0: EVEN, 1: Odd
   input  wire        non_parity_i,
   input  wire        irig_i,
   input  wire        sysclk_i,
   input  wire        resetn_i
);

reg  [2:0] irig_buf;
always @ (posedge sysclk_i, negedge resetn_i)
  if(~resetn_i) irig_buf <= 3'd0;
  else          irig_buf <= {irig_buf[1:0], irig_i};

wire v_irig_rise = (irig_buf[2:1] == 2'b01);
wire v_irig_fall = (irig_buf[2:1] == 2'b10);

// No signal counter
reg  [23:0]   nop_count;
wire [23:0] v_nop_count = (v_irig_rise | v_irig_fall | (nop_count == `TIME_OUT_NUM))
                        ? 24'd0 : nop_count + 1'b1;

always @ (posedge sysclk_i, negedge resetn_i)
  if(~resetn_i) nop_count <= 24'd0;
  else          nop_count <= v_nop_count;

// ------------------------------------------------------------
// Bit decide
// ------------------------------------------------------------
// Bit high period
reg    bit_hi_pulse;
wire v_bit_hi_pulse = v_irig_rise ? 1'b1
                    : v_irig_fall ? 1'b0 : bit_hi_pulse;

// Bit low period
reg    bit_lo_pulse;
wire v_bit_lo_pulse = v_irig_fall ? 1'b1
                    : v_irig_rise ? 1'b0 : bit_lo_pulse;

// Bit high period counter
reg  [19:0]   bit_hi_count;
wire [19:0] v_bit_hi_count = v_irig_rise  ? 20'd0
                           : bit_hi_pulse ? bit_hi_count + 1'b1 : bit_hi_count;

// Bit low period counter
reg  [19:0]   bit_lo_count;
wire [19:0] v_bit_lo_count = v_irig_fall  ? 20'd0
                           : bit_lo_pulse ? bit_lo_count + 1'b1 : bit_lo_count;

// 0: position, 1: Bit1, 2: Bit0, 3: Fair
reg  [ 1:0]   bit_type;

`ifdef  FOR_SIM
   wire [ 1:0] v_bit_type = (v_irig_fall & bit_hi_pulse)
                          ?  (bit_hi_count[19:4] == `PERIOD_8MS ) ? 2'b00
                           : (bit_hi_count[19:3] == `PERIOD_5MS ) ? 2'b01
                           : (bit_hi_count[19:3] == `PERIOD_2MS ) ? 2'b10 : 2'b11
                          : bit_type;
`else
   wire [ 1:0] v_bit_type = (v_irig_fall & bit_hi_pulse)
                          ?  (bit_hi_count[19:13] == `PERIOD_8MS ) ? 2'b00
                           : (bit_hi_count[19:13] == `PERIOD_5MS ) ? 2'b01
                           : (bit_hi_count[19:12] == `PERIOD_2MS ) ? 2'b10
                           : (bit_hi_count[19:12] == `PERIOD_2MS1) ? 2'b10: 2'b11
                          : bit_type;
`endif

// Bit low period fair (0:ok, 1:fair)
reg  v_bit_lo_fair, bit_lo_fair;

`ifdef  FOR_SIM
   always @ (*) begin
     if (v_irig_rise & bit_lo_pulse)
       case (bit_type)
         2'd0 : v_bit_lo_fair = ~(bit_lo_count[19:3] == `PERIOD_2MS);
         2'd1 : v_bit_lo_fair = ~(bit_lo_count[19:3] == `PERIOD_5MS);
         2'd2 : v_bit_lo_fair = ~(bit_lo_count[19:4] == `PERIOD_8MS);
         2'd3 : v_bit_lo_fair = 1'b1;
       endcase
     else       v_bit_lo_fair = bit_lo_fair;
   end
`else
   always @ (*) begin
     if (v_irig_rise & bit_lo_pulse)
       case (bit_type)
         2'd0 : v_bit_lo_fair = ((bit_lo_count[19:12] == `PERIOD_2MS ) ||
                                 (bit_lo_count[19:12] == `PERIOD_2MS1)) ? 1'b0 : 1'b1;
         2'd1 : v_bit_lo_fair = ~(bit_lo_count[19:13] == `PERIOD_5MS);
         2'd2 : v_bit_lo_fair = ~(bit_lo_count[19:13] == `PERIOD_8MS);
         2'd3 : v_bit_lo_fair = 1'b1;
       endcase
     else       v_bit_lo_fair = bit_lo_fair;
   end
`endif

always @ (posedge sysclk_i, negedge resetn_i) begin
  if(~resetn_i) begin
                bit_hi_pulse <= 1'b0;
                bit_lo_pulse <= 1'b0;
                bit_hi_count <= 20'd0;
                bit_lo_count <= 20'd0;
                bit_type     <= 2'd0;
                bit_lo_fair  <= 1'b0;
  end
  else          begin
                bit_hi_pulse <= v_bit_hi_pulse;
                bit_lo_pulse <= v_bit_lo_pulse;
                bit_hi_count <= v_bit_hi_count;
                bit_lo_count <= v_bit_lo_count;
                bit_type     <= v_bit_type;
                bit_lo_fair  <= v_bit_lo_fair;
  end
end

reg bit_check_rdy;
always @ (posedge sysclk_i, negedge resetn_i)
  if(~resetn_i) bit_check_rdy <= 1'b0;
  else          bit_check_rdy <= v_irig_fall;

// ------------------------------------------------------------
// IRIG-B State mechine
// ------------------------------------------------------------
reg [ 2:0] v_irig_state , irig_state;
reg [ 3:0] v_irig_bitcnt, irig_bitcnt;
reg [ 8:0] v_irig_data  , irig_data;

reg [ 1:0] v_fcnt_state, fcnt_state;
reg [31:0] v_irig_fcnt_o;

reg    irig_dataend;
wire v_irig_dataend = v_irig_fall & (irig_bitcnt == 4'd8);

reg  [3:0]   irig_pos_num;
wire [3:0] v_irig_pos_num = (irig_state == `IRIG_START) ? 4'd0
                          : (bit_check_rdy && (irig_state == `IRIG_POSITION))
                          ? (irig_pos_num + 1'b1) : irig_pos_num;

wire v_irig_off_o = (nop_count  == `TIME_OUT_NUM) ? 1'b1
                  : (v_irig_rise | v_irig_fall  ) ? 1'b0 : irig_off_o;

wire v_irig_fair_o  = (irig_state == `IRIG_ERROR);
wire v_irig_right_o = (fcnt_state == `FCNT_LAST ) & (v_irig_rise | (irig_state == `IRIG_IDLE));

always @ (posedge sysclk_i, negedge resetn_i) begin
  if(~resetn_i) begin
      irig_state      <=  3'd0;
      irig_bitcnt     <=  4'd0;
      irig_data       <=  9'd0;
      fcnt_state      <=  2'd0;
      irig_fcnt_o     <= 32'd0;
      irig_dataend    <=  1'b0;
      irig_pos_num    <=  4'd0;
      irig_off_o      <=  1'b0;
      irig_fair_o     <=  1'b0;
      irig_right_o    <=  1'b0;
  end
  else begin
      irig_state      <= v_irig_state;
      irig_bitcnt     <= v_irig_bitcnt;
      irig_data       <= v_irig_data;
      fcnt_state      <= v_fcnt_state;
      irig_fcnt_o     <= v_irig_fcnt_o;
      irig_dataend    <= v_irig_dataend;
      irig_pos_num    <= v_irig_pos_num;
      irig_off_o      <= v_irig_off_o;
      irig_fair_o     <= v_irig_fair_o;
      irig_right_o    <= v_irig_right_o;
  end
end

// ------------------------------------------------------------
// IRIG-B data catch
// ------------------------------------------------------------
wire v_irig_data_rdy = bit_check_rdy & (irig_state == `IRIG_POSITION) & (bit_type == 2'b00);

wire [88:0] v_irig_data_o;
assign v_irig_data_o[ 7: 0] = ((irig_pos_num == 4'd0) && v_irig_data_rdy)
                              ? irig_data[8:1] : irig_data_o[7:0];

assign v_irig_data_o[16: 8] = ((irig_pos_num == 4'd1) && v_irig_data_rdy)
                              ? irig_data : irig_data_o[16: 8];

assign v_irig_data_o[25:17] = ((irig_pos_num == 4'd2) && v_irig_data_rdy)
                              ? irig_data : irig_data_o[25:17];

assign v_irig_data_o[34:26] = ((irig_pos_num == 4'd3) && v_irig_data_rdy)
                              ? irig_data : irig_data_o[34:26];

assign v_irig_data_o[43:35] = ((irig_pos_num == 4'd4) && v_irig_data_rdy)
                              ? irig_data : irig_data_o[43:35];

assign v_irig_data_o[52:44] = ((irig_pos_num == 4'd5) && v_irig_data_rdy)
                              ? irig_data : irig_data_o[52:44];

assign v_irig_data_o[61:53] = ((irig_pos_num == 4'd6) && v_irig_data_rdy)
                              ? irig_data : irig_data_o[61:53];

assign v_irig_data_o[70:62] = ((irig_pos_num == 4'd7) && v_irig_data_rdy)
                              ? irig_data : irig_data_o[70:62];

assign v_irig_data_o[79:71] = ((irig_pos_num == 4'd8) && v_irig_data_rdy)
                              ? irig_data : irig_data_o[79:71];

assign v_irig_data_o[88:80] = ((irig_pos_num == 4'd9) && v_irig_data_rdy)
                              ? irig_data : irig_data_o[88:80];

always @ (posedge sysclk_i, negedge resetn_i) begin
  if(~resetn_i) irig_data_o  <= 89'd0;
  else          irig_data_o  <= v_irig_data_o;
end

// ============================
// IRIG-B total format ready
// ============================
wire v_sram_wen = (irig_state == `IRIG_FINISH) & (ram_order_i == SRAM_ORDER);

reg  [1:0]   sram_wcnt;
wire [1:0] v_sram_wcnt = (irig_state != `IRIG_FINISH) ? 2'd0
                       : (ram_order_i == SRAM_ORDER ) ? sram_wcnt + 1'b1 : sram_wcnt;

wire [3:0] v_irig_drdy_o;
assign v_irig_drdy_o[0] = v_sram_wen & (sram_wcnt == 2'd0);
assign v_irig_drdy_o[1] = v_sram_wen & (sram_wcnt == 2'd1);
assign v_irig_drdy_o[2] = v_sram_wen & (sram_wcnt == 2'd2);
assign v_irig_drdy_o[3] = v_sram_wen & (sram_wcnt == 2'd3);

always @ (posedge sysclk_i, negedge resetn_i) begin
  if(~resetn_i) begin
      sram_wcnt   <=  2'd0;
      irig_drdy_o <=  3'd0;
  end
  else begin
      sram_wcnt   <= v_sram_wcnt;
      irig_drdy_o <= v_irig_drdy_o;
  end
end

reg    irig_fine;
wire v_irig_fine = (irig_state == `IRIG_FINISH);
always @ (posedge sysclk_i, negedge resetn_i)
  if(~resetn_i) irig_fine <= 1'b0;
  else          irig_fine <= v_irig_fine;
  
wire v_irig_fine_o = ({irig_fine, v_irig_fine} == 2'b01);  
always @ (posedge sysclk_i, negedge resetn_i)
  if(~resetn_i) irig_fine_o <= 1'b0;
  else          irig_fine_o <= v_irig_fine_o;

// =========================
// IRIG-B parity check
// =========================

wire v_parity_bit = irig_data_o[67];
wire v_parity_rdy = (irig_state == `IRIG_POSITION) && bit_check_rdy && (irig_pos_num == 4'd9);

reg    parity_fair;
wire v_parity_fair = non_parity_i ? 1'b0
                   : parity_set_i ? (v_parity_bit != (~^irig_data_o[66:0]))   // ODD
                                  : (v_parity_bit != ( ^irig_data_o[66:0]));  // EVEN

wire v_parity_err_o = v_parity_rdy & parity_fair;

always @ (posedge sysclk_i, negedge resetn_i)
  if(~resetn_i) begin
    parity_err_o <= 1'b0;
    parity_fair  <= 1'b0;
  end
  else begin
    parity_err_o <= v_parity_err_o;
    parity_fair  <= v_parity_fair;
  end

always @(*) begin
  case(irig_state)
    `IRIG_IDLE : begin
      if (bit_check_rdy)
        v_irig_state = (bit_type == 2'b00) ? `IRIG_DETECT
                     : (bit_type == 2'b11) ? `IRIG_ERROR : irig_state;
      else
        v_irig_state = irig_state;

      v_irig_bitcnt = 4'd0;
      v_irig_data   = irig_data;
    end

    `IRIG_DETECT : begin
      if (irig_off_o)
        v_irig_state = `IRIG_IDLE;
      else begin
        if (bit_check_rdy)
          v_irig_state = (bit_type == 2'b00) ? `IRIG_START
                       : (bit_type == 2'b11) ? `IRIG_ERROR : `IRIG_IDLE;
        else
          v_irig_state = irig_state;
      end

      v_irig_bitcnt = 4'd0;
      v_irig_data   = irig_data;
    end

    `IRIG_START : begin
      v_irig_state  = `IRIG_DATA;
      v_irig_bitcnt = 4'd1;
      v_irig_data   = irig_data;
    end

    `IRIG_DATA : begin
      if (irig_off_o)
        v_irig_state = `IRIG_IDLE;
      else begin
        if (bit_check_rdy)
          v_irig_state = bit_lo_fair         ? `IRIG_ERROR
                       : (bit_type == 2'b00) ? `IRIG_ERROR
                       : (bit_type == 2'b11) ? `IRIG_ERROR
                       : irig_dataend        ? `IRIG_POSITION : irig_state;
        else
          v_irig_state = irig_state;
      end

      v_irig_bitcnt = bit_check_rdy ? (irig_bitcnt + 1'b1) : irig_bitcnt;
      v_irig_data   = bit_check_rdy ? {bit_type[0], irig_data[8:1]} : irig_data;
    end

    `IRIG_POSITION : begin
      if (irig_off_o)
        v_irig_state = `IRIG_IDLE;
      else begin
        if (bit_check_rdy) begin
          if (irig_pos_num == 4'd9)
            v_irig_state = bit_lo_fair         ? `IRIG_ERROR
                         : parity_fair         ? `IRIG_IDLE
                         : (bit_type != 2'b00) ? `IRIG_ERROR : `IRIG_FINISH;
          else
            v_irig_state = bit_lo_fair         ? `IRIG_ERROR
                         : (bit_type != 2'b00) ? `IRIG_ERROR : `IRIG_DATA;
        end
        else
          v_irig_state = irig_state;
      end

      v_irig_bitcnt = 3'd0;
      v_irig_data   = irig_data;
    end

    `IRIG_ERROR : begin
      v_irig_state  = `IRIG_IDLE;
      v_irig_bitcnt = 3'd0;
      v_irig_data   = irig_data;
    end

    `IRIG_FINISH : begin
      v_irig_state  = v_irig_drdy_o[3] ? `IRIG_DETECT : irig_state;
      v_irig_bitcnt = 3'd0;
      v_irig_data   = irig_data;
    end

    default : begin
      v_irig_state  = `IRIG_IDLE;
      v_irig_bitcnt = irig_bitcnt;
      v_irig_data   = irig_data;
    end
  endcase
end

always @(*) begin
  case(fcnt_state)
    `FCNT_IDLE : begin
      v_fcnt_state  = (irig_state == `IRIG_START ) ? `FCNT_DONE : fcnt_state;
      v_irig_fcnt_o = (irig_state == `IRIG_START ) ? `POSBIT_UNIT + 32'd5 : 32'd0;
    end

    `FCNT_DONE : begin
      v_fcnt_state  = (irig_state == `IRIG_FINISH) ? `FCNT_LAST
                    : (irig_state == `IRIG_ERROR ) ? `FCNT_IDLE 
                    : (irig_state == `IRIG_IDLE  ) ? `FCNT_IDLE : fcnt_state;
      v_irig_fcnt_o = (irig_state == `IRIG_ERROR ) ? 32'd0
                    : (irig_state == `IRIG_IDLE  ) ? 32'd0 : irig_fcnt_o + 1'b1;
    end

    `FCNT_LAST : begin
      v_fcnt_state  = (v_irig_rise | (irig_state == `IRIG_IDLE)) ? `FCNT_IDLE : fcnt_state;
      v_irig_fcnt_o = (v_irig_rise | (irig_state == `IRIG_IDLE)) ? 32'd0      : irig_fcnt_o + 1'b1;
    end

    default : begin
      v_fcnt_state  = `FCNT_IDLE;
      v_irig_fcnt_o = irig_fcnt_o;
    end
  endcase  
end
	
endmodule
