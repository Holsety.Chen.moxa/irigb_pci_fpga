`include "define.v"

module system_sram_ctrl (
   // For sram interface
   output reg  [`BAR0_ADW-1:0] sram_adr_o,
   output reg                  sram_wen_o,
   output reg  [31:0]          sram_din_o,

   // Reset Control Status
   input wire                  rst_ready_i,
   input wire  [31:0]          rst_status_i,   
   
   // IRIG0-B
   input wire  [ 3:0]          irig0_drdy_i,
   input wire  [88:0]          irig0_data_i,

   // IRIG1-B
   input wire  [ 3:0]          irig1_drdy_i,
   input wire  [88:0]          irig1_data_i,

   // RTC Control
   input wire  [ 2:0]          rtc_drdy_i,
   input wire  [31:0]          rtc_dat0_i,
   input wire  [31:0]          rtc_dat1_i,
   input wire  [31:0]          rtc_dat3_i,
   
   input wire                  sysclk_i,
   input wire                  rstn_i
   );


// ---------------------------------------------
//  For IRIG0-B, IRIG1-B, ...  module interface
// ---------------------------------------------
// Memory address
wire [`BAR0_ADW-1:0] v_sram_adr_o = rst_ready_i      ? `SYSCON
                                  : irig0_drdy_i[0]  ? `IRIG0DAT0
                                  : irig0_drdy_i[1]  ? `IRIG0DAT1
                                  : irig0_drdy_i[2]  ? `IRIG0DAT2
                                  : irig0_drdy_i[3]  ? `IRIG0DAT3
                                  : irig1_drdy_i[0]  ? `IRIG1DAT0
                                  : irig1_drdy_i[1]  ? `IRIG1DAT1
                                  : irig1_drdy_i[2]  ? `IRIG1DAT2
                                  : irig1_drdy_i[3]  ? `IRIG1DAT3
								  : rtc_drdy_i[0]    ? `RTCDAT0
								  : rtc_drdy_i[1]    ? `RTCDAT1
								  : rtc_drdy_i[2]    ? `RTCDAT3							  
								  : sram_adr_o;

// Memory write enable
wire v_sram_wen_o = (rst_ready_i  | (|irig0_drdy_i) | (|irig1_drdy_i) | (|rtc_drdy_i));
					 
// Memory write data
wire [31:0] v_sram_din_o = rst_ready_i      ? rst_status_i
                         : irig0_drdy_i[0]  ? { 6'd0, irig0_data_i[25: 0]}
                         : irig0_drdy_i[1]  ? { 5'd0, irig0_data_i[52:26]}
                         : irig0_drdy_i[2]  ? {14'd0, irig0_data_i[70:53]}
                         : irig0_drdy_i[3]  ? {14'd0, irig0_data_i[88:71]}
                         : irig1_drdy_i[0]  ? { 6'd0, irig1_data_i[25: 0]}
                         : irig1_drdy_i[1]  ? { 5'd0, irig1_data_i[52:26]}
                         : irig1_drdy_i[2]  ? {14'd0, irig1_data_i[70:53]}
                         : irig1_drdy_i[3]  ? {14'd0, irig1_data_i[88:71]}
						 : rtc_drdy_i[0]    ? rtc_dat0_i
						 : rtc_drdy_i[1]    ? rtc_dat1_i
						 : rtc_drdy_i[2]    ? rtc_dat3_i					 
						 : sram_din_o;

always @ (posedge sysclk_i, negedge rstn_i) begin
  if (~rstn_i) begin
       sram_wen_o  <= 1'b0;
       sram_adr_o  <= {`BAR0_ADW{1'B0}};
       sram_din_o  <= 32'd0;
  end
  else begin
       sram_wen_o  <= v_sram_wen_o;
       sram_adr_o  <= v_sram_adr_o;
       sram_din_o  <= v_sram_din_o;
  end
end

endmodule

