`include "define.v"

module output_config(
   output wire [7:0]           outpin_o,

   // Local Bus address
   input  wire [`BAR0_ADW-1:0] offset_ad_i, // Offset addr
   input  wire                 offset_wr_i, // Offset write
   input  wire [31:0]          write_dat_i,

   // Input pin
   input  wire                 inport0_i,
   input  wire                 inport1_i,
   input  wire                 inport2_i,
   input  wire                 inport3_i,
   input  wire                 inport4_i,
   input  wire                 inport5_i,
   input  wire                 inport6_i,
   input  wire                 inport7_i,

   input  wire                 encode_irig_i,
   input  wire                 encode_pps_i,
   input  wire                 fifo_pulse_i,
   input  wire                 loopbackpps_i,
   input  wire [7:0]           gpio_data_i,

   input  wire                 pciclk_i,
   input  wire                 rstn_i
);

// --------------------------------------------------------------------
// Output Port Config Register
// --------------------------------------------------------------------
  wire v_outport_hit = (offset_wr_i && (offset_ad_i == `OUTPORTCON));

  reg  [7:0]   out_invert;
  wire [7:0] v_out_invert;

  assign v_out_invert[0] = v_outport_hit ? write_dat_i[ 4] : out_invert[0];
  assign v_out_invert[1] = v_outport_hit ? write_dat_i[ 9] : out_invert[1];
  assign v_out_invert[2] = v_outport_hit ? write_dat_i[14] : out_invert[2];
  assign v_out_invert[3] = v_outport_hit ? write_dat_i[19] : out_invert[3];
  assign v_out_invert[4] = v_outport_hit ? write_dat_i[24] : out_invert[4];
  assign v_out_invert[5] = v_outport_hit ? write_dat_i[29] : out_invert[5];
  assign v_out_invert[6] = v_outport_hit ? write_dat_i[30] : out_invert[6];
  assign v_out_invert[7] = v_outport_hit ? write_dat_i[31] : out_invert[7];

  always @ (posedge pciclk_i, negedge rstn_i) begin
    if (~rstn_i) out_invert <= 8'd0;
    else         out_invert <= v_out_invert;
  end

  // Output Channel Select
  reg  [3:0]   output0_select, output1_select, output2_select,
               output3_select, output4_select, output5_select;

  wire [3:0] v_output0_select = v_outport_hit ? write_dat_i[ 3: 0] : output0_select;
  wire [3:0] v_output1_select = v_outport_hit ? write_dat_i[ 8: 5] : output1_select;
  wire [3:0] v_output2_select = v_outport_hit ? write_dat_i[13:10] : output2_select;
  wire [3:0] v_output3_select = v_outport_hit ? write_dat_i[18:15] : output3_select;
  wire [3:0] v_output4_select = v_outport_hit ? write_dat_i[23:20] : output4_select;
  wire [3:0] v_output5_select = v_outport_hit ? write_dat_i[28:25] : output5_select;

  always @ (posedge pciclk_i, negedge rstn_i) begin
    if (~rstn_i) begin
       output0_select <= 4'h0;
       output1_select <= 4'h0;
       output2_select <= 4'h0;
       output3_select <= 4'h0;
       output4_select <= 4'h0;
       output5_select <= 4'h0;
    end
    else begin
       output0_select <= v_output0_select;
       output1_select <= v_output1_select;
       output2_select <= v_output2_select;
       output3_select <= v_output3_select;
       output4_select <= v_output4_select;
       output5_select <= v_output5_select;
    end
  end

  assign outpin_o[0] = (output0_select == 4'h0) ? (out_invert[0] ? ~gpio_data_i[0] : gpio_data_i[0])
                     : (output0_select == 4'h1) ? (out_invert[0] ? ~encode_irig_i  : encode_irig_i )
                     : (output0_select == 4'h3) ? (out_invert[0] ? ~encode_pps_i   : encode_pps_i  )
					 : (output0_select == 4'h5) ? 1'bz
                     : (output0_select == 4'h6) ? (out_invert[0] ? ~fifo_pulse_i   : fifo_pulse_i  )
                     : (output0_select == 4'h7) ? (out_invert[0] ? ~loopbackpps_i  : loopbackpps_i )
                     : (output0_select == 4'h8) ? (out_invert[0] ? ~inport0_i      : inport0_i     )
                     : (output0_select == 4'h9) ? (out_invert[0] ? ~inport1_i      : inport1_i     )
                     : (output0_select == 4'ha) ? (out_invert[0] ? ~inport2_i      : inport2_i     )
                     : (output0_select == 4'hb) ? (out_invert[0] ? ~inport3_i      : inport3_i     )
                     : (output0_select == 4'hc) ? (out_invert[0] ? ~inport4_i      : inport4_i     )
                     : (output0_select == 4'hd) ? (out_invert[0] ? ~inport5_i      : inport5_i     )
                     : (output0_select == 4'he) ? (out_invert[0] ? ~inport6_i      : inport6_i     )
                     : (output0_select == 4'hf) ? (out_invert[0] ? ~inport7_i      : inport7_i     )
                     : 1'b0;

  assign outpin_o[1] = (output1_select == 4'h0) ? (out_invert[1] ? ~gpio_data_i[1] : gpio_data_i[1])
                     : (output1_select == 4'h1) ? (out_invert[1] ? ~encode_irig_i  : encode_irig_i )
                     : (output1_select == 4'h3) ? (out_invert[1] ? ~encode_pps_i   : encode_pps_i  )
					 : (output0_select == 4'h5) ? 1'bz                    
					 : (output1_select == 4'h6) ? (out_invert[1] ? ~fifo_pulse_i   : fifo_pulse_i  )
                     : (output1_select == 4'h7) ? (out_invert[1] ? ~loopbackpps_i  : loopbackpps_i )
                     : (output1_select == 4'h8) ? (out_invert[1] ? ~inport0_i      : inport0_i     )
                     : (output1_select == 4'h9) ? (out_invert[1] ? ~inport1_i      : inport1_i     )
                     : (output1_select == 4'ha) ? (out_invert[1] ? ~inport2_i      : inport2_i     )
                     : (output1_select == 4'hb) ? (out_invert[1] ? ~inport3_i      : inport3_i     )
                     : (output1_select == 4'hc) ? (out_invert[1] ? ~inport4_i      : inport4_i     )
                     : (output1_select == 4'hd) ? (out_invert[1] ? ~inport5_i      : inport5_i     )
                     : (output1_select == 4'he) ? (out_invert[1] ? ~inport6_i      : inport6_i     )
                     : (output1_select == 4'hf) ? (out_invert[1] ? ~inport7_i      : inport7_i     )
                     : 1'b0;

  assign outpin_o[2] = (output2_select == 4'h0) ? (out_invert[2] ? ~gpio_data_i[2] : gpio_data_i[2])
                     : (output2_select == 4'h1) ? (out_invert[2] ? ~encode_irig_i  : encode_irig_i )
                     : (output2_select == 4'h3) ? (out_invert[2] ? ~encode_pps_i   : encode_pps_i  )
					 : (output0_select == 4'h5) ? 1'bz                     
					 : (output2_select == 4'h6) ? (out_invert[2] ? ~fifo_pulse_i   : fifo_pulse_i  )
                     : (output2_select == 4'h7) ? (out_invert[2] ? ~loopbackpps_i  : loopbackpps_i )
                     : (output2_select == 4'h8) ? (out_invert[2] ? ~inport0_i      : inport0_i     )
                     : (output2_select == 4'h9) ? (out_invert[2] ? ~inport1_i      : inport1_i     )
                     : (output2_select == 4'ha) ? (out_invert[2] ? ~inport2_i      : inport2_i     )
                     : (output2_select == 4'hb) ? (out_invert[2] ? ~inport3_i      : inport3_i     )
                     : (output2_select == 4'hc) ? (out_invert[2] ? ~inport4_i      : inport4_i     )
                     : (output2_select == 4'hd) ? (out_invert[2] ? ~inport5_i      : inport5_i     )
                     : (output2_select == 4'he) ? (out_invert[2] ? ~inport6_i      : inport6_i     )
                     : (output2_select == 4'hf) ? (out_invert[2] ? ~inport7_i      : inport7_i     )
                     : 1'b0;

  assign outpin_o[3] = (output3_select == 4'h0) ? (out_invert[3] ? ~gpio_data_i[3] : gpio_data_i[3])
                     : (output3_select == 4'h1) ? (out_invert[3] ? ~encode_irig_i  : encode_irig_i )
                     : (output3_select == 4'h3) ? (out_invert[3] ? ~encode_pps_i   : encode_pps_i  )
					 : (output0_select == 4'h5) ? 1'bz                     
					 : (output3_select == 4'h6) ? (out_invert[3] ? ~fifo_pulse_i   : fifo_pulse_i  )
                     : (output3_select == 4'h7) ? (out_invert[3] ? ~loopbackpps_i  : loopbackpps_i )
                     : (output3_select == 4'h8) ? (out_invert[3] ? ~inport0_i      : inport0_i     )
                     : (output3_select == 4'h9) ? (out_invert[3] ? ~inport1_i      : inport1_i     )
                     : (output3_select == 4'ha) ? (out_invert[3] ? ~inport2_i      : inport2_i     )
                     : (output3_select == 4'hb) ? (out_invert[3] ? ~inport3_i      : inport3_i     )
                     : (output3_select == 4'hc) ? (out_invert[3] ? ~inport4_i      : inport4_i     )
                     : (output3_select == 4'hd) ? (out_invert[3] ? ~inport5_i      : inport5_i     )
                     : (output3_select == 4'he) ? (out_invert[3] ? ~inport6_i      : inport6_i     )
                     : (output3_select == 4'hf) ? (out_invert[3] ? ~inport7_i      : inport7_i     )
                     : 1'b0;
					 
  assign outpin_o[4] = (output4_select == 4'h0) ? (out_invert[4] ? ~gpio_data_i[4] : gpio_data_i[4])
                     : (output4_select == 4'h1) ? (out_invert[4] ? ~encode_irig_i  : encode_irig_i )
                     : (output4_select == 4'h3) ? (out_invert[4] ? ~encode_pps_i   : encode_pps_i  )
					 : (output0_select == 4'h5) ? 1'bz                     
					 : (output4_select == 4'h6) ? (out_invert[4] ? ~fifo_pulse_i   : fifo_pulse_i  )
                     : (output4_select == 4'h7) ? (out_invert[4] ? ~loopbackpps_i  : loopbackpps_i )
                     : (output4_select == 4'h8) ? (out_invert[4] ? ~inport0_i      : inport0_i     )
                     : (output4_select == 4'h9) ? (out_invert[4] ? ~inport1_i      : inport1_i     )
                     : (output4_select == 4'ha) ? (out_invert[4] ? ~inport2_i      : inport2_i     )
                     : (output4_select == 4'hb) ? (out_invert[4] ? ~inport3_i      : inport3_i     )
                     : (output4_select == 4'hc) ? (out_invert[4] ? ~inport4_i      : inport4_i     )
                     : (output4_select == 4'hd) ? (out_invert[4] ? ~inport5_i      : inport5_i     )
                     : (output4_select == 4'he) ? (out_invert[4] ? ~inport6_i      : inport6_i     )
                     : (output4_select == 4'hf) ? (out_invert[4] ? ~inport7_i      : inport7_i     )
                     : 1'b0;					 

  assign outpin_o[5] = (output5_select == 4'h0) ? (out_invert[5] ? ~gpio_data_i[5] : gpio_data_i[5])
                     : (output5_select == 4'h1) ? (out_invert[5] ? ~encode_irig_i  : encode_irig_i )
                     : (output5_select == 4'h3) ? (out_invert[5] ? ~encode_pps_i   : encode_pps_i  )
					 : (output0_select == 4'h5) ? 1'bz                     
					 : (output5_select == 4'h6) ? (out_invert[5] ? ~fifo_pulse_i   : fifo_pulse_i  )
                     : (output5_select == 4'h7) ? (out_invert[5] ? ~loopbackpps_i  : loopbackpps_i )
                     : (output5_select == 4'h8) ? (out_invert[5] ? ~inport0_i      : inport0_i     )
                     : (output5_select == 4'h9) ? (out_invert[5] ? ~inport1_i      : inport1_i     )
                     : (output5_select == 4'ha) ? (out_invert[5] ? ~inport2_i      : inport2_i     )
                     : (output5_select == 4'hb) ? (out_invert[5] ? ~inport3_i      : inport3_i     )
                     : (output5_select == 4'hc) ? (out_invert[5] ? ~inport4_i      : inport4_i     )
                     : (output5_select == 4'hd) ? (out_invert[5] ? ~inport5_i      : inport5_i     )
                     : (output5_select == 4'he) ? (out_invert[5] ? ~inport6_i      : inport6_i     )
                     : (output5_select == 4'hf) ? (out_invert[5] ? ~inport7_i      : inport7_i     )
                     : 1'b0;

  assign outpin_o[6] = (out_invert[6] ? ~gpio_data_i[6] : gpio_data_i[6]);
  assign outpin_o[7] = (out_invert[7] ? ~gpio_data_i[7] : gpio_data_i[7]);  
					 
					 
endmodule
