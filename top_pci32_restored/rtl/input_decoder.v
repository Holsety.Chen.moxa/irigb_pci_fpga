`include "define.v"

module input_decoder(
   output wire                 irigb0_o,
   output wire                 irigb1_o,
   output wire                 ppsdec_o,
   output wire                 loopps_o,

   // Local Bus address
   input  wire [`BAR0_ADW-1:0] offset_ad_i, // Offset addr
   input  wire                 offset_wr_i, // Offset write
   input  wire [31:0]          write_dat_i,

   // Input pin
   input  wire                 inport0_i,
   input  wire                 inport1_i,
   input  wire                 inport2_i,
   input  wire                 inport3_i,
   input  wire                 inport4_i,
   input  wire                 inport5_i,
   input  wire                 inport6_i,
   input  wire                 inport7_i,

   input  wire                 pciclk_i,
   input  wire                 rstn_i

);

// --------------------------------------------------------------------
//  Input Port Config Register
// --------------------------------------------------------------------
  reg  [7:0]   invert_en;
  wire [7:0] v_invert_en;
  wire       v_inportcon_hit = (offset_wr_i && (offset_ad_i == `INPORTCON));

  assign v_invert_en[0] = v_inportcon_hit ? write_dat_i[16] : invert_en[0];
  assign v_invert_en[1] = v_inportcon_hit ? write_dat_i[17] : invert_en[1];
  assign v_invert_en[2] = v_inportcon_hit ? write_dat_i[18] : invert_en[2];
  assign v_invert_en[3] = v_inportcon_hit ? write_dat_i[19] : invert_en[3];
  assign v_invert_en[4] = v_inportcon_hit ? write_dat_i[20] : invert_en[4];
  assign v_invert_en[5] = v_inportcon_hit ? write_dat_i[21] : invert_en[5];
  assign v_invert_en[6] = v_inportcon_hit ? write_dat_i[22] : invert_en[6];
  assign v_invert_en[7] = v_inportcon_hit ? write_dat_i[23] : invert_en[7];

  always @ (posedge pciclk_i, negedge rstn_i) begin
    if (~rstn_i) invert_en <= 8'd0;
    else         invert_en <= v_invert_en;
  end

  // IRIG decoder0
  reg  [2:0]   irigb0_select;
  wire [2:0] v_irigb0_select = v_inportcon_hit ? write_dat_i[2:0] : irigb0_select;
  reg          irigb0_enable;
  wire       v_irigb0_enable = v_inportcon_hit ? write_dat_i[3]   : irigb0_enable;

  // IRIG decoder1
  reg  [2:0]   irigb1_select;
  wire [2:0] v_irigb1_select = v_inportcon_hit ? write_dat_i[6:4] : irigb1_select;
  reg          irigb1_enable;
  wire       v_irigb1_enable = v_inportcon_hit ? write_dat_i[7]   : irigb1_enable;
  
  // Pulse Per Second Encoder
  reg  [2:0]   ppsenc_select;
  wire [2:0] v_ppsenc_select = v_inportcon_hit ? write_dat_i[10:8] : ppsenc_select;
  reg          ppsenc_enable;
  wire       v_ppsenc_enable = v_inportcon_hit ? write_dat_i[11]   : ppsenc_enable;
 
  // Loop Back Pulse
  reg  [2:0]   loopps_select;
  wire [2:0] v_loopps_select = v_inportcon_hit ? write_dat_i[14:12] : loopps_select;
  reg          loopps_enable;
  wire       v_loopps_enable = v_inportcon_hit ? write_dat_i[15]    : loopps_enable; 
  
  always @ (posedge pciclk_i, negedge rstn_i) begin
    if (~rstn_i) begin
       irigb0_select  <= 3'd0;
       irigb0_enable  <= 1'b0;
       irigb1_select  <= 3'd0;
       irigb1_enable  <= 1'b0;
       ppsenc_select  <= 3'd0;
	   ppsenc_enable  <= 1'b0;
	   loopps_select  <= 3'd0;
	   loopps_enable  <= 1'b0;
    end
    else begin
       irigb0_select  <= v_irigb0_select;
       irigb0_enable  <= v_irigb0_enable;
       irigb1_select  <= v_irigb1_select;
       irigb1_enable  <= v_irigb1_enable;
       ppsenc_select  <= v_ppsenc_select;
	   ppsenc_enable  <= v_ppsenc_enable;
       loopps_select  <= v_loopps_select;
       loopps_enable  <= v_loopps_enable;	   
    end
  end

  assign irigb0_o = ~irigb0_enable ? 1'b0
                  : (irigb0_select == 3'd0) ? invert_en[0] ? ~inport0_i : inport0_i  
                  : (irigb0_select == 3'd1) ? invert_en[1] ? ~inport1_i : inport1_i
                  : (irigb0_select == 3'd2) ? invert_en[2] ? ~inport2_i : inport2_i  
                  : (irigb0_select == 3'd3) ? invert_en[3] ? ~inport3_i : inport3_i  
                  : (irigb0_select == 3'd4) ? invert_en[4] ? ~inport4_i : inport4_i
                  : (irigb0_select == 3'd5) ? invert_en[5] ? ~inport5_i : inport5_i  
                  : (irigb0_select == 3'd6) ? invert_en[6] ? ~inport6_i : inport6_i  
                                            : invert_en[7] ? ~inport7_i : inport7_i;
  
  assign irigb1_o = ~irigb1_enable ? 1'b0
                  : (irigb1_select == 3'd0) ? invert_en[0] ? ~inport0_i : inport0_i  
                  : (irigb1_select == 3'd1) ? invert_en[1] ? ~inport1_i : inport1_i
                  : (irigb1_select == 3'd2) ? invert_en[2] ? ~inport2_i : inport2_i  
                  : (irigb1_select == 3'd3) ? invert_en[3] ? ~inport3_i : inport3_i  
                  : (irigb1_select == 3'd4) ? invert_en[4] ? ~inport4_i : inport4_i
                  : (irigb1_select == 3'd5) ? invert_en[5] ? ~inport5_i : inport5_i  
                  : (irigb1_select == 3'd6) ? invert_en[6] ? ~inport6_i : inport6_i  
                                            : invert_en[7] ? ~inport7_i : inport7_i;
  
  assign ppsdec_o = ~ppsenc_enable ? 1'b0
                  : (ppsenc_select == 3'd0) ? invert_en[0] ? ~inport0_i : inport0_i  
                  : (ppsenc_select == 3'd1) ? invert_en[1] ? ~inport1_i : inport1_i
                  : (ppsenc_select == 3'd2) ? invert_en[2] ? ~inport2_i : inport2_i  
                  : (ppsenc_select == 3'd3) ? invert_en[3] ? ~inport3_i : inport3_i  
                  : (ppsenc_select == 3'd4) ? invert_en[4] ? ~inport4_i : inport4_i
                  : (ppsenc_select == 3'd5) ? invert_en[5] ? ~inport5_i : inport5_i  
                  : (ppsenc_select == 3'd6) ? invert_en[6] ? ~inport6_i : inport6_i  
                                            : invert_en[7] ? ~inport7_i : inport7_i;  

  assign loopps_o = ~loopps_enable ? 1'b0
                  : (loopps_select == 3'd0) ? invert_en[0] ? ~inport0_i : inport0_i  
                  : (loopps_select == 3'd1) ? invert_en[1] ? ~inport1_i : inport1_i
                  : (loopps_select == 3'd2) ? invert_en[2] ? ~inport2_i : inport2_i  
                  : (loopps_select == 3'd3) ? invert_en[3] ? ~inport3_i : inport3_i  
                  : (loopps_select == 3'd4) ? invert_en[4] ? ~inport4_i : inport4_i
                  : (loopps_select == 3'd5) ? invert_en[5] ? ~inport5_i : inport5_i  
                  : (loopps_select == 3'd6) ? invert_en[6] ? ~inport6_i : inport6_i  
                                            : invert_en[7] ? ~inport7_i : inport7_i; 											
  
endmodule
