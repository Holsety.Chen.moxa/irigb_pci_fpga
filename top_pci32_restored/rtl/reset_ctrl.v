`include "define.v"

module reset_ctrl(
   output reg                  loopbacken_o,
   output wire                 system_rst_o,
   output wire [31:0]          rst_status_o,
   output reg                  rst_ready_o,

   // Local Bus address
   input  wire [`BAR0_ADW-1:0] offset_ad_i, // Offset addr
   input  wire                 offset_wr_i, // Offset write
   input  wire [31:0]          write_dat_i,

   input  wire                 pciclk_i,
   input  wire                 sysclk_i
);

// --------------------------------------------------------------------
//  System Config Register
// --------------------------------------------------------------------
  // Internal Counter
  reg  [3:0]   reset_cnt = 4'd0;
  wire [3:0] v_reset_cnt = (reset_cnt == `RESET_LP) ? `RESET_LP : reset_cnt + 1'b1;
  always @ (posedge sysclk_i) reset_cnt <= v_reset_cnt;

  wire       v_setrst_hit = (offset_wr_i && (offset_ad_i == `SYSCON));

  reg          rst_catch = 1'b1;
  reg          set_reset = 1'b0;
  wire       v_set_reset = v_setrst_hit ? write_dat_i[0]
                         : (~rst_catch  ? 1'b0 : set_reset);

  always @ (posedge pciclk_i) set_reset <= v_set_reset;

  wire       v_rst_catch = (~rst_catch & ~set_reset) ? 1'b1
                         : (set_reset ? 1'b0 : rst_catch);

  always @ (posedge sysclk_i) rst_catch <= v_rst_catch;

  assign system_rst_o = (reset_cnt == `RESET_LP) ? rst_catch : 1'b0;

  reg  [1:0]   rst_fifo = 2'b11;
  wire [1:0] v_rst_fifo = {rst_fifo[0], rst_catch};

  wire v_rst_ready_o = (rst_fifo == 2'b01);

  always @ (posedge sysclk_i) begin
        rst_fifo    <= v_rst_fifo;
        rst_ready_o <= v_rst_ready_o;
  end

  wire v_loopbacken_o = v_setrst_hit ? write_dat_i[31] : loopbacken_o;
  always @ (posedge pciclk_i) loopbacken_o <= v_loopbacken_o;

  assign rst_status_o = {loopbacken_o, 30'd0, ~system_rst_o};

endmodule
