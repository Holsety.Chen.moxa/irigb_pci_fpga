`include "define.v"
module  rtc_control #(
        parameter SRAM_ORDER = 2'B10
)
(
   output reg  [ 2:0]          rtc_drdy_o,  // Write sram order 0~2
   output wire [31:0]          rtc_dat0_o,
   output wire [31:0]          rtc_dat1_o,
   output reg  [31:0]          rtc_dat2_o,
   output wire [31:0]          rtc_dat3_o,
   output reg                  rdy_stream_o,
   output wire [88:0]          rtc_stream_o,
   output reg                  second_hit_o,

   input  wire                 irig0_end_i,
   input  wire [88:0]          irig0_dat_i,
   input  wire                 irig1_end_i,
   input  wire [88:0]          irig1_dat_i,

   input  wire                 pps_decode_i,
   input  wire                 decpps_err_i,
   // Local Bus address
   input  wire [`BAR0_ADW-1:0] offset_ad_i, // Offset addr
   input  wire                 offset_wr_i, // Offset write
   input  wire [31:0]          write_dat_i,

   input  wire [ 1:0]          ram_order_i,

   input  wire                 pciclk_i,
   input  wire                 sysclk_i,
   input  wire                 rstn_i
);

// =======================================================
// BCD Register(second/minute/hour/date/month/year)
// =======================================================
  reg [ 6:0] bcd_sec;
  reg [ 6:0] bcd_min;
  reg [ 5:0] bcd_hour;
  reg [ 9:0] bcd_days;
  reg [ 5:0] bcd_date;
  reg [ 4:0] bcd_mon;
  reg [15:0] bcd_year;

// =======================================================
// Hit Register(minute/hour/date/month/year)
// =======================================================
  reg  minute_hit;
  wire v_hour_hit, v_day_hit, v_month_hit, v_year_hit;
  reg  leapyear_hit;

// =======================================================
// PCI Setting Time & Control
// =======================================================
//~~~~~~~~~~~~~~~~~~ (`RTCCON Offset) ~~~~~~~~~
// RTC Input/Output Select
  wire v_rtcset_hit = (offset_wr_i && (offset_ad_i == `RTCCON));
  reg  [2:0]   rtc_dinsel;
  wire [2:0] v_rtc_dinsel = v_rtcset_hit ? write_dat_i[2:0] : rtc_dinsel;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) rtc_dinsel <= 3'd0;
      else        rtc_dinsel <= v_rtc_dinsel;

  reg    leap_sec_sign;
  wire v_leap_sec_sign = v_rtcset_hit ? write_dat_i[5] : leap_sec_sign;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) leap_sec_sign <= 1'b0;
      else        leap_sec_sign <= v_leap_sec_sign;

//~~~~~~~~~~~~~~~~~~ (`RTCDAT0 Offset) ~~~~~~~~~
// Setting second
  wire v_rtcdat0_hit = (offset_wr_i && (offset_ad_i == `RTCDAT0));
  reg  [6:0]   system_sec;
  wire [6:0] v_system_sec = v_rtcdat0_hit ? write_dat_i[6:0] : system_sec;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) system_sec <= 7'd0;
      else        system_sec <= v_system_sec;

// Setting minute
  reg  [6:0]   system_min;
  wire [6:0] v_system_min = v_rtcdat0_hit ? write_dat_i[14:8] : system_min;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) system_min <= 7'd0;
      else        system_min <= v_system_min;

// Setting hour
  reg  [5:0]   system_hour;
  wire [5:0] v_system_hour = v_rtcdat0_hit ? write_dat_i[21:16] : system_hour;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) system_hour <= 6'd0;
      else        system_hour <= v_system_hour;

// Setting date
  reg  [5:0]   system_date;
  wire [5:0] v_system_date = v_rtcdat0_hit ? write_dat_i[29:24] : system_date;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) system_date <= 6'd0;
      else        system_date <= v_system_date;

//~~~~~~~~~~~~~~~~~~ (`RTCDAT1 Offset) ~~~~~~~~~
// Setting month
  wire v_rtcdat1_hit = (offset_wr_i && (offset_ad_i == `RTCDAT1));
  reg  [4:0]   system_mon;
  wire [4:0] v_system_mon = v_rtcdat1_hit ? write_dat_i[4:0] : system_mon;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) system_mon <= 5'd0;
      else        system_mon <= v_system_mon;

// Setting year
  reg  [15:0]   system_year;
  wire [15:0] v_system_year = v_rtcdat1_hit ? write_dat_i[23:8] : system_year;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) system_year <= 16'd0;
      else        system_year <= v_system_year;

// RTC Commit Setting
  reg    rtc_commit, rtc_config;
  wire v_rtc_commit = v_rtcdat1_hit ? write_dat_i[31] : (rtc_config ? 1'b0 : rtc_commit);
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) rtc_commit <= 1'b0;
      else        rtc_commit <= v_rtc_commit;

  wire v_rtc_config = rtc_config ? 1'b0 : rtc_commit;
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) rtc_config <= 1'b0;
      else        rtc_config <= v_rtc_config;

//~~~~~~~~~~~~~~~~~~ (`RTCDAT3 Offset) ~~~~~~~~~
// Setting Control Bit
  wire v_rtcdat3_hit = (offset_wr_i && (offset_ad_i == `RTCDAT3));
  wire [13:0] irig0_ctrl_dat = {irig0_dat_i[66:63], irig0_dat_i[61:58], irig0_dat_i[62], irig0_dat_i[57:53]};
  wire [13:0] irig1_ctrl_dat = {irig1_dat_i[66:63], irig1_dat_i[61:58], irig1_dat_i[62], irig1_dat_i[57:53]};

  reg  [13:0] v_irig_ctrlbit, irig_ctrlbit;
  always @(*) begin
      if (v_rtcdat3_hit)
          v_irig_ctrlbit = write_dat_i[13:0];
      else
          if (irig0_end_i && (rtc_dinsel == 3'd1))
              v_irig_ctrlbit = irig0_ctrl_dat;
          else if (irig1_end_i && (rtc_dinsel == 3'd2))
              v_irig_ctrlbit = irig1_ctrl_dat;
          else
              v_irig_ctrlbit = irig_ctrlbit;
  end
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) irig_ctrlbit <= 14'd0;
      else        irig_ctrlbit <= v_irig_ctrlbit;

//~~~~~~~~~~~~~~~~~~ (`RTCLS Offset) ~~~~~~~~~
// Leap Second Setting
  wire v_rtcls_hit = (offset_wr_i && (offset_ad_i == `RTCLS));

  reg  [6:0]   set_leap_min;
  wire [6:0] v_set_leap_min  = v_rtcls_hit ? write_dat_i[ 6: 0] : set_leap_min;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) set_leap_min <= 7'd0;
      else        set_leap_min <= v_set_leap_min ;

  reg  [5:0]   set_leap_hour;
  wire [5:0] v_set_leap_hour = v_rtcls_hit  ? write_dat_i[12: 7] : set_leap_hour;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) set_leap_hour <= 6'd0;
      else        set_leap_hour <= v_set_leap_hour;

  reg  [9:0]   set_leap_day;
  wire [9:0] v_set_leap_day  = v_rtcls_hit  ? write_dat_i[22:13] : set_leap_day;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) set_leap_day <= 10'd0;
      else        set_leap_day <= v_set_leap_day;

  reg  [7:0]   set_leap_year;
  wire [7:0] v_set_leap_year = v_rtcls_hit  ? write_dat_i[30:23] : set_leap_year;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) set_leap_year <= 8'd0;
      else        set_leap_year <= v_set_leap_year;

//~~~~~~~~~~~~~~~~~~ (`RTCDST Offset) ~~~~~~~~~
// Start Daylight Saving hour Register
  wire v_rtcdst_hit = (offset_wr_i && (offset_ad_i == `RTCDST));
  reg  [5:0]   set_dst_hour0;
  wire [5:0] v_set_dst_hour0 = ~v_rtcdst_hit ? set_dst_hour0
                             : (write_dat_i[ 5: 0] == 6'h00) ? 6'h23
                             : (write_dat_i[ 5: 0] == 6'h10) ? 6'h09
                             : (write_dat_i[ 5: 0] == 6'h20) ? 6'h19 : {write_dat_i[ 5: 4], (write_dat_i[3:0] - 4'h1)};
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) set_dst_hour0 <= 6'd0;
      else        set_dst_hour0 <= v_set_dst_hour0;

// Ending Daylight Saving hour Register
  reg  [5:0]   set_dst_hour1;
  wire [5:0] v_set_dst_hour1 = ~v_rtcdst_hit ? set_dst_hour1
                             : (write_dat_i[21:16] == 6'h00) ? 6'h23
                             : (write_dat_i[21:16] == 6'h10) ? 6'h09
                             : (write_dat_i[21:16] == 6'h20) ? 6'h19 : {write_dat_i[21:20], (write_dat_i[19:16] - 4'h1)};
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) set_dst_hour1 <= 6'd0;
      else        set_dst_hour1 <= v_set_dst_hour1;

// Start Daylight Saving day Register
  reg  [9:0]   set_dst_days0;
  wire [9:0] v_set_dst_days0 = v_rtcdst_hit ? write_dat_i[15:6] : set_dst_days0;
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) set_dst_days0 <= 10'd0;
      else        set_dst_days0 <= v_set_dst_days0;

// Ending Daylight Saving day Register
  reg dst_start, dst_finish;
  reg  [9:0]   set_dst_days1;
  wire [9:0] v_set_dst_days1 = v_rtcdst_hit ? write_dat_i[31:22] : (dst_finish & minute_hit) ? 10'd0 : set_dst_days1; // Prevent Repeat
  always @ (posedge pciclk_i, negedge rstn_i)
      if(~rstn_i) set_dst_days1 <= 10'd0;
      else        set_dst_days1 <= v_set_dst_days1;

// =======================================================
// Frequency counter (sysclk_i : 25MHz)
// =======================================================
  reg v_second_hit_o;
  reg  [31:0]   freq_count;
  reg  [31:0] v_freq_count;
  always @(*) begin
      if (v_second_hit_o | ((rtc_dinsel == 3'd3) & ~decpps_err_i))
          v_freq_count = 32'd0;
      else
	      if ((irig0_end_i & (rtc_dinsel == 3'd1)) || (irig1_end_i & (rtc_dinsel == 3'd2)))
		      v_freq_count = `UNIT_SEC - `LOWBIT_UNIT + 32'd8;
		  else
		      v_freq_count = freq_count + 32'd1;
  end  
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) freq_count <= 32'd0;
      else        freq_count <= v_freq_count;

  always @(*) begin
      if (freq_count == `UNIT_SEC)
          v_second_hit_o = 1'b1;
      else
	      if (rtc_dinsel == 3'd0)
		      v_second_hit_o = v_rtc_config;
		  else if ((rtc_dinsel == 3'd3) & ~decpps_err_i)
		      v_second_hit_o = pps_decode_i;
		  else
		      v_second_hit_o = 1'b0;
  end
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) second_hit_o <= 1'b0;
      else        second_hit_o <= v_second_hit_o;

// =======================================================
// Leap Year Register
// =======================================================
  wire v_leapyear_hit = (bcd_year[13:0] == 14'h0) ? 1'b0                  // 4000, 8000 not leap
                      : (bcd_year[ 9:0] == 10'h0) ? 1'b1                  //  400, 800 leap
                      : (bcd_year[ 7:0] ==  8'h0) ? 1'b0                  //  100, 200 not leap
                      : (bcd_year[ 1:0] ==  2'd0) ? ~bcd_year[4]          //    4, 8 leap (20, 40, 60, ..)
                      : (bcd_year[   0] ==  1'b0) ?  bcd_year[4] : 1'b0;  //    4, 8 leap (12, 32, 52, ..)
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) leapyear_hit <= 1'b0;
      else        leapyear_hit <= v_leapyear_hit;

// =======================================================
// Transfer Month/Date to Days
// =======================================================
  wire  [9:0]   system_days;
  date_to_days date_to_days(
      .system_days_o(system_days),
      .month_sel_i  (v_system_mon),
      .date_sel_i   (system_date ),
      .leapyear_i   (leapyear_hit),
      .pciclk_i     (pciclk_i    ),
      .rstn_i       (rstn_i      ));

// =======================================================
// Transfer Days to Date
// =======================================================
  wire [5:0] normal_date0;
  wire [5:0] normal_date1;
  date_normal date_normal(
      .address_a({irig0_dat_i[36:31], irig0_dat_i[29:26]}),
      .address_b({irig1_dat_i[36:31], irig1_dat_i[29:26]}),
      .clock    (sysclk_i),
      .q_a      (normal_date0),
      .q_b      (normal_date1));

  wire [5:0] leapyear_date0;
  wire [5:0] leapyear_date1;
  date_leapyear date_leapyear(
      .address_a({irig0_dat_i[36:31], irig0_dat_i[29:26]}),
      .address_b({irig1_dat_i[36:31], irig1_dat_i[29:26]}),
      .clock    (sysclk_i),
      .q_a      (leapyear_date0),
      .q_b      (leapyear_date1));

// =======================================================
// Transfer Days to Month
// =======================================================
  wire [4:0] normal_month0;
  wire [4:0] normal_month1;
  month_normal month_normal(
      .address_a({irig0_dat_i[36:31], irig0_dat_i[29:26]}),
      .address_b({irig1_dat_i[36:31], irig1_dat_i[29:26]}),
      .clock    (sysclk_i),
      .q_a      (normal_month0),
      .q_b      (normal_month1));

  wire [4:0] leapyear_month0;
  wire [4:0] leapyear_month1;
  month_leapyear month_leapyear(
      .address_a({irig0_dat_i[36:31], irig0_dat_i[29:26]}),
      .address_b({irig1_dat_i[36:31], irig1_dat_i[29:26]}),
      .clock    (sysclk_i),
      .q_a      (leapyear_month0),
      .q_b      (leapyear_month1));

// =======================================================
// The BCD Data Of Second
// =======================================================
  reg    leap_second;
  wire v_leap_second = ((bcd_min  == set_leap_min) && (bcd_hour      == set_leap_hour) &&
                        (bcd_days == set_leap_day) && (bcd_year[7:0] == set_leap_year));
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) leap_second <=  1'b0;
      else        leap_second <= v_leap_second;

  reg [6:0] v_bcd_sec;
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) bcd_sec <= 7'd0;
      else        bcd_sec <= v_bcd_sec;

  always @(*) begin
    if (rtc_config && (rtc_dinsel == 3'd0))
        v_bcd_sec = system_sec;
    else if (irig0_end_i && (rtc_dinsel == 3'd1))
		    v_bcd_sec = {irig0_dat_i[7:5], irig0_dat_i[3:0]};
    else if (irig1_end_i && (rtc_dinsel == 3'd2))
            v_bcd_sec = {irig1_dat_i[7:5], irig1_dat_i[3:0]};
    else begin
        case({second_hit_o, leap_second, leap_sec_sign})
            3'b000, 3'b001, 3'b010, 3'b011 :
                  v_bcd_sec = bcd_sec;
            3'b100, 3'b101 : begin
                if (bcd_sec[3:0] == 4'h9)
                    if (bcd_sec[6:4] == 3'd5) // 59second
                      v_bcd_sec = 7'd0;
                    else begin                // 9, 19, 29, 39, 49 second
                      v_bcd_sec[6:4] = bcd_sec[6:4] + 3'd1;
                      v_bcd_sec[3:0] = 4'h0;
                    end
                else begin
                      v_bcd_sec[6:4] = bcd_sec[6:4];
                      v_bcd_sec[3:0] = bcd_sec[3:0] + 4'h1;
                end
            end
            3'b110, 3'b111 : begin
                if ((~leap_sec_sign && (bcd_sec == 7'h60))||
                    ( leap_sec_sign && (bcd_sec == 7'h58)))
                      v_bcd_sec = 7'd0;
                else begin
                    if (bcd_sec[3:0] == 4'h9) begin
                      v_bcd_sec[6:4] = bcd_sec[6:4] + 3'd1;
                      v_bcd_sec[3:0] = 4'h0;
                    end
                    else begin
                      v_bcd_sec[6:4] = bcd_sec[6:4];
                      v_bcd_sec[3:0] = bcd_sec[3:0] + 4'h1;
                    end
                end
            end
        endcase
    end
  end

// =======================================================
// The BCD Data Of Minute
// =======================================================
  reg [6:0] v_bcd_min;
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) bcd_min <= 7'd0;
      else        bcd_min <= v_bcd_min;

  wire v_minute_hit = (bcd_sec == 7'h58) ? (v_second_hit_o &  leap_second &  leap_sec_sign)
                    : (bcd_sec == 7'h60) ? (v_second_hit_o &  leap_second & ~leap_sec_sign)
                    : (bcd_sec == 7'h59) ? (v_second_hit_o & ~leap_second) : 1'b0;
  always @ (posedge sysclk_i, negedge rstn_i)
    if(~rstn_i) minute_hit <= 1'b0;
    else        minute_hit <= v_minute_hit;

  always @(*) begin
      if (rtc_config  && (rtc_dinsel == 3'd0))
                v_bcd_min = system_min;
      else if (irig0_end_i && (rtc_dinsel == 3'd1))
                v_bcd_min = {irig0_dat_i[15:13], irig0_dat_i[11:8]};
      else if (irig1_end_i && (rtc_dinsel == 3'd2))
                v_bcd_min = {irig1_dat_i[15:13], irig1_dat_i[11:8]};
      else begin
          if (minute_hit) begin
              if (bcd_min[3:0] == 4'h9) begin
                  if (bcd_min[6:4] == 3'd5) // 59 minute
                      v_bcd_min = 7'd0;
                  else begin                // 9, 19, 29, 39, 49 minute
                      v_bcd_min[6:4] = bcd_min[6:4] + 3'd1;
                      v_bcd_min[3:0] = 4'h0;
                  end
              end
              else begin
                      v_bcd_min[6:4] = bcd_min[6:4];
                      v_bcd_min[3:0] = bcd_min[3:0] + 4'h1;
              end
          end
          else        v_bcd_min = bcd_min;
      end
  end

// =======================================================
// The BCD Data Of Hour
// =======================================================
  wire v_dst_start  = ((bcd_days == set_dst_days0) && (bcd_hour == set_dst_hour0) && (bcd_min == 7'h59)) 
                    ? 1'b1 : (minute_hit ? 1'b0 : dst_start);
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) dst_start <= 1'b0;
      else        dst_start <= v_dst_start;

  wire v_dst_finish = ((bcd_days == set_dst_days1) && (bcd_hour == set_dst_hour1) && (bcd_min == 7'h59)) 
                    ? 1'b1 : (minute_hit ? 1'b0 : dst_finish);
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) dst_finish <= 1'b0;
      else        dst_finish <= v_dst_finish;
	  
  reg [5:0] v_bcd_hour;
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) bcd_hour <= 6'd0;
      else        bcd_hour <= v_bcd_hour;

  assign v_hour_hit = (minute_hit && (bcd_min == 7'h59));

  always @(*) begin
      if (rtc_config  && (rtc_dinsel == 3'd0))
                v_bcd_hour = system_hour;
      else if (irig0_end_i && (rtc_dinsel == 3'd1))
                v_bcd_hour = {irig0_dat_i[23:22], irig0_dat_i[20:17]};
      else if (irig1_end_i && (rtc_dinsel == 3'd2))
                v_bcd_hour = {irig1_dat_i[23:22], irig1_dat_i[20:17]};
      else begin
          case({v_hour_hit, dst_finish, dst_start})
              3'b000, 3'b001, 3'b010, 3'b011, 3'b110 :
                    v_bcd_hour = bcd_hour;
              3'b100, 3'b111 : begin
                if (bcd_hour == 6'h23)
                    v_bcd_hour = 6'd0;
                else begin
                    if (bcd_hour[3:0] == 4'h9) begin
                        v_bcd_hour[5:4] = bcd_hour[5:4] + 2'd1;
                        v_bcd_hour[3:0] = 4'h0;
                    end
                    else begin
                        v_bcd_hour[5:4] = bcd_hour[5:4];
                        v_bcd_hour[3:0] = bcd_hour[3:0] + 4'h1;
                    end
                end
              end
              3'b101 : begin // Daylight Saving Time Start
                  if (bcd_hour == 6'h22)            // 22 -> 00
                      v_bcd_hour = 6'd0;
                  else if (bcd_hour == 6'h23)       // 23 -> 01
                      v_bcd_hour = 6'd1;
                  else begin
                      if (bcd_hour[3:0] == 4'h8) begin       // 8->10, 18->20
                          v_bcd_hour[5:4] = bcd_hour[5:4] + 2'd1;
                          v_bcd_hour[3:0] = 4'h0;
                      end
                      else if (bcd_hour[3:0] == 4'h9) begin  // 9->11, 19->21
                          v_bcd_hour[5:4] = bcd_hour[5:4] + 2'd1;
                          v_bcd_hour[3:0] = 4'h1;
                      end
                      else begin
                          v_bcd_hour[5:4] = bcd_hour[5:4];   // (0->2, 1->3, 2->4 ... ...)
                          v_bcd_hour[3:0] = bcd_hour[3:0] + 4'h2;
                      end
                  end
              end
          endcase
      end
  end

// =======================================================
// The BCD Data Of Days
// =======================================================
  reg [9:0] v_bcd_days;
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) bcd_days <= 10'd1;
      else        bcd_days <= v_bcd_days;

  assign v_day_hit = (v_hour_hit && (bcd_hour == 6'h23)) | ((v_hour_hit && (bcd_hour == 6'h22)) && dst_start);
  
  always @ (*) begin
    if (rtc_config  && (rtc_dinsel == 3'd0))
             v_bcd_days = system_days;
    else if (irig0_end_i && (rtc_dinsel == 3'd1))
             v_bcd_days = {irig0_dat_i[36:31], irig0_dat_i[29:26]};
    else if (irig1_end_i && (rtc_dinsel == 3'd2))
             v_bcd_days = {irig1_dat_i[36:31], irig1_dat_i[29:26]};
    else begin
        if (v_day_hit) begin
            if (((bcd_days == 10'h365) && ~leapyear_hit) || ((bcd_days == 10'h366) && leapyear_hit))
                 v_bcd_days = 10'h1;
            else begin
                case({(bcd_days[7:4] == 4'h9), (bcd_days[3:0] == 4'h9)})
                    2'b00, 2'b10 : begin
                        v_bcd_days[9:4] = bcd_days[9:4];
                        v_bcd_days[3:0] = bcd_days[3:0] + 4'h1;
                    end
                    2'b01 : begin
                        v_bcd_days[9:8] = bcd_days[9:8];
                        v_bcd_days[7:4] = bcd_days[7:4] + 4'h1;
                        v_bcd_days[3:0] = 4'h0;
                    end
                    2'b11 : begin
                        v_bcd_days[9:8] = bcd_days[9:8] + 2'h1;
                        v_bcd_days[7:0] = 8'h0;
                    end
                endcase
            end
        end
        else            v_bcd_days = bcd_days;
    end
  end
  
// =======================================================
// The BCD Data Of Date
// =======================================================
  wire v_max_month = (bcd_mon == 5'h01) | (bcd_mon == 5'h03) | (bcd_mon == 5'h05) |
                     (bcd_mon == 5'h07) | (bcd_mon == 5'h08) | (bcd_mon == 5'h10) | (bcd_mon == 5'h12);
  wire v_min_month = (bcd_mon == 5'h04) | (bcd_mon == 5'h06) | (bcd_mon == 5'h09) | (bcd_mon == 5'h11);
  
  reg [5:0] v_bcd_date;
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) bcd_date <= 6'd1;
      else        bcd_date <= v_bcd_date;

  always @(*) begin
      if (rtc_config && (rtc_dinsel == 3'd0))
                v_bcd_date = system_date;
      else if (irig0_end_i && (rtc_dinsel == 3'd1))
                v_bcd_date = leapyear_hit ? leapyear_date0 : normal_date0;
      else if (irig1_end_i && (rtc_dinsel == 3'd2))
                v_bcd_date = leapyear_hit ? leapyear_date1 : normal_date1;
      else begin
          if (v_day_hit) begin
              if (((bcd_date == 6'h29) && (bcd_mon == 5'h02) &&  leapyear_hit) |
                  ((bcd_date == 6'h28) && (bcd_mon == 5'h02) && ~leapyear_hit) |
                  ((bcd_date == 6'h30) && v_min_month) | ((bcd_date == 6'h31) && v_max_month))
                      v_bcd_date = 6'd1;
              else begin
                  if (bcd_date[3:0] == 4'h9) begin
                      v_bcd_date[5:4] = bcd_date[5:4] + 2'd1;
                      v_bcd_date[3:0] = 4'h0;
                  end
                  else begin
                      v_bcd_date[5:4] = bcd_date[5:4];
                      v_bcd_date[3:0] = bcd_date[3:0] + 4'h1;
                  end
              end
          end
          else        v_bcd_date = bcd_date;
      end
  end

// =======================================================
// The BCD Data Of Month
// =======================================================
  reg [4:0] v_bcd_mon;
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) bcd_mon <= 5'd1;
      else        bcd_mon <= v_bcd_mon;
	  
  assign v_month_hit = v_day_hit && (( leapyear_hit && (bcd_mon == 5'h02) && (bcd_date == 6'h29))|
                                     (~leapyear_hit && (bcd_mon == 5'h02) && (bcd_date == 6'h28))|
                                                             (v_max_month && (bcd_date == 6'h31))|
                                                             (v_min_month && (bcd_date == 6'h30)));

  always @(*) begin
      if (rtc_config  && (rtc_dinsel == 3'd0))
                v_bcd_mon = system_mon;
      else if (irig0_end_i && (rtc_dinsel == 3'd1))
                v_bcd_mon = leapyear_hit ? leapyear_month0 : normal_month0;
      else if (irig1_end_i && (rtc_dinsel == 3'd2))
                v_bcd_mon = leapyear_hit ? leapyear_month1 : normal_month1;
      else begin
          if (v_month_hit) begin
              if (bcd_mon[4] && (bcd_mon[3:0] == 4'h2)) // 12
                    v_bcd_mon = 5'd1;
              else begin
                if (bcd_mon[3:0] == 4'h9)
                    v_bcd_mon = 5'h10;
                else begin
                    v_bcd_mon[4]   = bcd_mon[4];
                    v_bcd_mon[3:0] = bcd_mon[3:0] + 4'h1;
                end
              end
          end
          else      v_bcd_mon = bcd_mon;
      end
  end

// =======================================================
// The BCD Data Of Year
// =======================================================
  reg  [15:0] v_bcd_year;
  always @ (posedge sysclk_i, negedge rstn_i)
    if(~rstn_i) bcd_year <= 16'h2014;
    else        bcd_year <= v_bcd_year;

  assign v_year_hit = v_month_hit && bcd_mon[4] && (bcd_mon[3:0] == 4'h2); // December 12
  always @(*) begin
      if (rtc_config && (rtc_dinsel == 3'd0))
              v_bcd_year = system_year;
    else if (irig0_end_i && (rtc_dinsel == 3'd1))
              v_bcd_year = {bcd_year[15:8], irig0_dat_i[52:49], irig0_dat_i[47:44]};
    else if (irig1_end_i && (rtc_dinsel == 3'd2))
              v_bcd_year = {bcd_year[15:8], irig1_dat_i[52:49], irig1_dat_i[47:44]};
    else begin
        if (v_year_hit) begin
            case({(bcd_year[11:8] == 4'h9), (bcd_year[7:4] == 4'h9), (bcd_year[3:0] == 4'h9)})
                3'b000, 3'b010, 3'b100, 3'b110 : begin
                    v_bcd_year[15: 4] = bcd_year[15: 4];
                    v_bcd_year[ 3: 0] = bcd_year[ 3: 0] + 4'h1;
                end
                3'b001, 3'b101 : begin // XXX9
                    v_bcd_year[15: 8] = bcd_year[15: 8];
                    v_bcd_year[ 7: 4] = bcd_year[ 7: 4] + 4'h1;
                    v_bcd_year[ 3: 0] = 4'h0;
                end
                3'b011 : begin         // XX99
                    v_bcd_year[15:12] = bcd_year[15:12];
                    v_bcd_year[11: 8] = bcd_year[11: 8] + 4'h1;
                    v_bcd_year[ 7: 0] = 8'h0;
                end
                3'b111 : begin
                    v_bcd_year[15:12] = bcd_year[15:12] + 4'h1;
                    v_bcd_year[11: 0] = 12'h0;
                end
            endcase
        end
        else        v_bcd_year = bcd_year;
    end
  end

// =======================================================
// DSP/DST Enable
// =======================================================  
  reg    dsp_enable;
  wire v_dsp_enable = ((bcd_days == set_dst_days0) && (bcd_hour == set_dst_hour0) && (bcd_min == 7'h59)) |
                      ((bcd_days == set_dst_days1) && (bcd_hour == set_dst_hour1) && (bcd_min == 7'h59));
  always @ (posedge sysclk_i, negedge rstn_i)
    if(~rstn_i) dsp_enable <= 1'b0;
    else        dsp_enable <= v_dsp_enable;


  reg v_dst_enable, dst_enable;
  always @ (posedge sysclk_i, negedge rstn_i)
    if(~rstn_i) dst_enable <= 1'b0;
    else        dst_enable <= v_dst_enable;
	
  always @(*) begin
    if ((set_dst_days0 == 10'd0) | (set_dst_days1 == 10'd0))
               v_dst_enable = 1'b0;
    else
        case({(bcd_days < set_dst_days0), (bcd_days > set_dst_days1)})
            2'b00 : begin
                if (bcd_days == set_dst_days0) begin
                    if (bcd_hour < set_dst_hour0)
                       v_dst_enable = 1'b0;
                    else if (bcd_hour == set_dst_hour0)
                       v_dst_enable = minute_hit & (bcd_min == 7'h59);
                    else
                       v_dst_enable = 1'b1;
                end
                else if (bcd_days == set_dst_days1) begin
                    if (bcd_hour > set_dst_hour1)
                       v_dst_enable = 1'b0;
                    else
                       v_dst_enable = 1'b1;
                end
                else   v_dst_enable = 1'b1;
            end
            2'b01, 2'b10, 2'b11 : 
			           v_dst_enable = 1'b0;
            default :  v_dst_enable = 1'b0;
        endcase
  end

// =======================================================
// RTC Write To Memory
// =======================================================
  reg [1:0] v_rtcdat_wcnt, rtcdat_wcnt;
  
  reg v_rtc_state, rtc_state;
  always @ (*) begin
    case(rtc_state)
      1'b0 :   v_rtc_state = second_hit_o;
      1'b1 :
          if (ram_order_i == SRAM_ORDER)
               v_rtc_state = (rtcdat_wcnt == 2'd2) ? 1'b0 : rtc_state;
          else
               v_rtc_state = rtc_state;
    endcase
  end
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) rtc_state <= 1'b0;
      else        rtc_state <= v_rtc_state;

  
  always @ (*) begin
      if (~rtc_state)
	     v_rtcdat_wcnt = 2'd0;
      else
          if (ram_order_i == SRAM_ORDER)
              if (rtcdat_wcnt == 2'd2)
	              v_rtcdat_wcnt = 2'd0;
              else
	              v_rtcdat_wcnt = rtcdat_wcnt + 2'd1;
          else
		          v_rtcdat_wcnt = rtcdat_wcnt;
  end
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) rtcdat_wcnt <= 2'd0;
      else        rtcdat_wcnt <= v_rtcdat_wcnt;

  wire [2:0] v_rtc_drdy_o;
      assign v_rtc_drdy_o[0] = rtc_state & (ram_order_i == SRAM_ORDER) & (rtcdat_wcnt == 2'd0);
      assign v_rtc_drdy_o[1] = rtc_state & (ram_order_i == SRAM_ORDER) & (rtcdat_wcnt == 2'd1);
      assign v_rtc_drdy_o[2] = rtc_state & (ram_order_i == SRAM_ORDER) & (rtcdat_wcnt == 2'd2);
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) rtc_drdy_o <= 3'd0;
      else        rtc_drdy_o <= v_rtc_drdy_o;

  assign rtc_dat0_o = {2'd0, bcd_date, 2'd0, bcd_hour, 1'b0, bcd_min, 1'b0, bcd_sec};
  assign rtc_dat1_o = {rtc_commit, 7'd0, bcd_year, 3'd0, bcd_mon};
  assign rtc_dat3_o = {18'd0, irig_ctrlbit};


  wire [31:0] v_rtc_dat2_o = v_second_hit_o ? 32'd0 : rtc_dat2_o + 32'd40;
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) rtc_dat2_o <= 32'd0;
      else        rtc_dat2_o <= v_rtc_dat2_o;


// =======================================================
// RTC Data to IRIG Encoder
// =======================================================

  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) rdy_stream_o <= 1'b0;
      else        rdy_stream_o <= second_hit_o;


  wire [16:0] v_hour_sec = (bcd_hour == 6'h01) ? 17'd3600
                         : (bcd_hour == 6'h02) ? 17'd7200
                         : (bcd_hour == 6'h03) ? 17'd10800
                         : (bcd_hour == 6'h04) ? 17'd14400
                         : (bcd_hour == 6'h05) ? 17'd18000
                         : (bcd_hour == 6'h06) ? 17'd21600
                         : (bcd_hour == 6'h07) ? 17'd25200
                         : (bcd_hour == 6'h08) ? 17'd28800
                         : (bcd_hour == 6'h09) ? 17'd32400
                         : (bcd_hour == 6'h10) ? 17'd36000
                         : (bcd_hour == 6'h11) ? 17'd39600
                         : (bcd_hour == 6'h12) ? 17'd43200
                         : (bcd_hour == 6'h13) ? 17'd46800
                         : (bcd_hour == 6'h14) ? 17'd50400
                         : (bcd_hour == 6'h15) ? 17'd54000
                         : (bcd_hour == 6'h16) ? 17'd57600
                         : (bcd_hour == 6'h17) ? 17'd61200
                         : (bcd_hour == 6'h18) ? 17'd64800
                         : (bcd_hour == 6'h19) ? 17'd68400
                         : (bcd_hour == 6'h20) ? 17'd72000
                         : (bcd_hour == 6'h21) ? 17'd75600
                         : (bcd_hour == 6'h22) ? 17'd79200
                         : (bcd_hour == 6'h23) ? 17'd82800 : 17'd0;

  wire [16:0] v_min_sec  = (bcd_min == 7'h01) ? 17'd60
                         : (bcd_min == 7'h02) ? 17'd120
                         : (bcd_min == 7'h03) ? 17'd180
                         : (bcd_min == 7'h04) ? 17'd240
                         : (bcd_min == 7'h05) ? 17'd300
                         : (bcd_min == 7'h06) ? 17'd360
                         : (bcd_min == 7'h07) ? 17'd420
                         : (bcd_min == 7'h08) ? 17'd480
                         : (bcd_min == 7'h09) ? 17'd540
                         : (bcd_min == 7'h10) ? 17'd600
                         : (bcd_min == 7'h11) ? 17'd660
                         : (bcd_min == 7'h12) ? 17'd720
                         : (bcd_min == 7'h13) ? 17'd780
                         : (bcd_min == 7'h14) ? 17'd840
                         : (bcd_min == 7'h15) ? 17'd900
                         : (bcd_min == 7'h16) ? 17'd960
                         : (bcd_min == 7'h17) ? 17'd1020
                         : (bcd_min == 7'h18) ? 17'd1080
                         : (bcd_min == 7'h19) ? 17'd1140
                         : (bcd_min == 7'h20) ? 17'd1200
                         : (bcd_min == 7'h21) ? 17'd1260
                         : (bcd_min == 7'h22) ? 17'd1320
                         : (bcd_min == 7'h23) ? 17'd1380
                         : (bcd_min == 7'h24) ? 17'd1440
                         : (bcd_min == 7'h25) ? 17'd1500
                         : (bcd_min == 7'h26) ? 17'd1560
                         : (bcd_min == 7'h27) ? 17'd1620
                         : (bcd_min == 7'h28) ? 17'd1680
                         : (bcd_min == 7'h29) ? 17'd1740
                         : (bcd_min == 7'h30) ? 17'd1800
                         : (bcd_min == 7'h31) ? 17'd1860
                         : (bcd_min == 7'h32) ? 17'd1920
                         : (bcd_min == 7'h33) ? 17'd1980
                         : (bcd_min == 7'h34) ? 17'd2040
                         : (bcd_min == 7'h35) ? 17'd2100
                         : (bcd_min == 7'h36) ? 17'd2160
                         : (bcd_min == 7'h37) ? 17'd2220
                         : (bcd_min == 7'h38) ? 17'd2280
                         : (bcd_min == 7'h39) ? 17'd2340
                         : (bcd_min == 7'h40) ? 17'd2400
                         : (bcd_min == 7'h41) ? 17'd2460
                         : (bcd_min == 7'h42) ? 17'd2520
                         : (bcd_min == 7'h43) ? 17'd2580
                         : (bcd_min == 7'h44) ? 17'd2640
                         : (bcd_min == 7'h45) ? 17'd2700
                         : (bcd_min == 7'h46) ? 17'd2760
                         : (bcd_min == 7'h47) ? 17'd2820
                         : (bcd_min == 7'h48) ? 17'd2880
                         : (bcd_min == 7'h49) ? 17'd2940
                         : (bcd_min == 7'h50) ? 17'd3000
                         : (bcd_min == 7'h51) ? 17'd3060
                         : (bcd_min == 7'h52) ? 17'd3120
                         : (bcd_min == 7'h53) ? 17'd3180
                         : (bcd_min == 7'h54) ? 17'd3240
                         : (bcd_min == 7'h55) ? 17'd3300
                         : (bcd_min == 7'h56) ? 17'd3360
                         : (bcd_min == 7'h57) ? 17'd3420
                         : (bcd_min == 7'h58) ? 17'd3480
                         : (bcd_min == 7'h59) ? 17'd3540 : 17'd0;
  
  wire [16:0] v_hex_sec  = (bcd_sec == 7'h01) ? 17'd1
                         : (bcd_sec == 7'h02) ? 17'd2
                         : (bcd_sec == 7'h03) ? 17'd3
                         : (bcd_sec == 7'h04) ? 17'd4
                         : (bcd_sec == 7'h05) ? 17'd5
                         : (bcd_sec == 7'h06) ? 17'd6
                         : (bcd_sec == 7'h07) ? 17'd7
                         : (bcd_sec == 7'h08) ? 17'd8
                         : (bcd_sec == 7'h09) ? 17'd9
                         : (bcd_sec == 7'h10) ? 17'd10
                         : (bcd_sec == 7'h11) ? 17'd11
                         : (bcd_sec == 7'h12) ? 17'd12
                         : (bcd_sec == 7'h13) ? 17'd13
                         : (bcd_sec == 7'h14) ? 17'd14
                         : (bcd_sec == 7'h15) ? 17'd15
                         : (bcd_sec == 7'h16) ? 17'd16
                         : (bcd_sec == 7'h17) ? 17'd17
                         : (bcd_sec == 7'h18) ? 17'd18
                         : (bcd_sec == 7'h19) ? 17'd19
                         : (bcd_sec == 7'h20) ? 17'd20
                         : (bcd_sec == 7'h21) ? 17'd21
                         : (bcd_sec == 7'h22) ? 17'd22
                         : (bcd_sec == 7'h23) ? 17'd23
                         : (bcd_sec == 7'h24) ? 17'd24
                         : (bcd_sec == 7'h25) ? 17'd25
                         : (bcd_sec == 7'h26) ? 17'd26
                         : (bcd_sec == 7'h27) ? 17'd27
                         : (bcd_sec == 7'h28) ? 17'd28
                         : (bcd_sec == 7'h29) ? 17'd29
                         : (bcd_sec == 7'h30) ? 17'd30
                         : (bcd_sec == 7'h31) ? 17'd31
                         : (bcd_sec == 7'h32) ? 17'd32
                         : (bcd_sec == 7'h33) ? 17'd33
                         : (bcd_sec == 7'h34) ? 17'd34
                         : (bcd_sec == 7'h35) ? 17'd35
                         : (bcd_sec == 7'h36) ? 17'd36
                         : (bcd_sec == 7'h37) ? 17'd37
                         : (bcd_sec == 7'h38) ? 17'd38
                         : (bcd_sec == 7'h39) ? 17'd39
                         : (bcd_sec == 7'h40) ? 17'd40
                         : (bcd_sec == 7'h41) ? 17'd41
                         : (bcd_sec == 7'h42) ? 17'd42
                         : (bcd_sec == 7'h43) ? 17'd43
                         : (bcd_sec == 7'h44) ? 17'd44
                         : (bcd_sec == 7'h45) ? 17'd45
                         : (bcd_sec == 7'h46) ? 17'd46
                         : (bcd_sec == 7'h47) ? 17'd47
                         : (bcd_sec == 7'h48) ? 17'd48
                         : (bcd_sec == 7'h49) ? 17'd49
                         : (bcd_sec == 7'h50) ? 17'd50
                         : (bcd_sec == 7'h51) ? 17'd51
                         : (bcd_sec == 7'h52) ? 17'd52
                         : (bcd_sec == 7'h53) ? 17'd53
                         : (bcd_sec == 7'h54) ? 17'd54
                         : (bcd_sec == 7'h55) ? 17'd55
                         : (bcd_sec == 7'h56) ? 17'd56
                         : (bcd_sec == 7'h57) ? 17'd57
                         : (bcd_sec == 7'h58) ? 17'd58
                         : (bcd_sec == 7'h59) ? 17'd59
                         : (bcd_sec == 7'h60) ? 17'd59 : 17'd0;

// Straight Binary Seconds
  wire [16:0] v_irig_sbsbit = (v_hour_sec + v_min_sec + v_hex_sec);

  assign rtc_stream_o[ 7: 0] = {      bcd_sec[6:4] , 1'b0, bcd_sec[3:0]};
  assign rtc_stream_o[16: 8] = {1'b0, bcd_min[6:4] , 1'b0, bcd_min[3:0]};
  assign rtc_stream_o[25:17] = {2'd0, bcd_hour[5:4], 1'b0, bcd_hour[3:0]};
  assign rtc_stream_o[34:26] = {      bcd_days[7:4], 1'b0, bcd_days[3:0]};
  assign rtc_stream_o[43:35] = {7'd0, bcd_days[9:8]};
  assign rtc_stream_o[52:44] = {      bcd_year[7:4], 1'b0, bcd_year[3:0]};
  assign rtc_stream_o[61:53] = {irig_ctrlbit[9:6], irig_ctrlbit[4], dst_enable, v_dsp_enable, leap_sec_sign, v_leap_second};
  assign rtc_stream_o[70:62] = {4'd0, irig_ctrlbit[13:10], irig_ctrlbit[5]};
  assign rtc_stream_o[79:71] =  v_irig_sbsbit[8:0];
  assign rtc_stream_o[88:80] = {1'b0, v_irig_sbsbit[16:9]};

endmodule







