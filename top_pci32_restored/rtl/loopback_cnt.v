
module loopback_cnt(
   output reg  [31:0]    loopbackcnt_o,
   output reg            loop_dout_o,

   input  wire           loop_din_i,
   input  wire           loopbacken_i,
   input  wire           sysclk_i,
   input  wire           rstn_i
);

  reg  [1:0] loopback_fifo;

  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) loopback_fifo <= 2'd0;
    else         loopback_fifo <= {loopback_fifo[0], loopbacken_i};

  reg    loop_ready;
  wire v_loop_ready  = loop_din_i               ? 1'b0
                     : (loopback_fifo == 2'b01) ? 1'b1 : loop_ready;
					 
  always @ (posedge sysclk_i, negedge rstn_i) begin
    if (~rstn_i)    loop_ready  <= 1'b0;
    else            loop_ready  <= v_loop_ready;
  end

  reg    loop_dout_q;  
  wire v_loop_dout_o = (loopback_fifo == 2'b01) ? 1'b1 
                     : loop_dout_q ? 1'b0 : loop_dout_o;
					 
  always @ (negedge sysclk_i, negedge rstn_i) begin
    if (~rstn_i) begin
  	  loop_dout_o <= 1'b0;
	  loop_dout_q <= 1'b0;
	end  
    else begin           
	  loop_dout_o <= v_loop_dout_o;
	  loop_dout_q <= loop_dout_o;
	end  
  end

  wire [31:0] v_loopbackcnt_o = (~loopbacken_i) ? 32'd0 : (loop_ready
                              ? loopbackcnt_o + 1'b1 : loopbackcnt_o);

  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) loopbackcnt_o <= 32'd0;
    else         loopbackcnt_o <= v_loopbackcnt_o;

endmodule
