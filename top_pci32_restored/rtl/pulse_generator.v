`include "define.v"

module pulse_generator
(
   output wire [31:0]          fifo_config_o,
   output reg                  fifo_pulse_o,

   // Local Bus address
   input  wire [`BAR0_ADW-1:0] offset_ad_i, // Offset addr
   input  wire                 offset_wr_i, // Offset write
   input  wire [31:0]          write_dat_i,

   input  wire                 pciclk_i,
   input  wire                 sysclk_i,
   input  wire                 rstn_i
);

parameter          PULSE_IDLE = 1'B0,
                   PULSE_DONE = 1'B1;

				   
  reg v_sfifo_state, sfifo_state;
// --------------------------------------------------------------------
//  Serial FIFO Configuration/Data Register
// --------------------------------------------------------------------
// Serial FIFO Configuration
  wire        v_setfifo_hit = (offset_wr_i && (offset_ad_i == `SFIFOCON));

  reg    sfifo_clr;
  reg    set_clear;
  wire v_set_clear = sfifo_clr ? 1'b0 : (v_setfifo_hit ? write_dat_i[18] : set_clear);
  always @ (posedge pciclk_i, negedge rstn_i)
    if (~rstn_i) set_clear <= 1'b0;
    else         set_clear <= v_set_clear;

  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) sfifo_clr <= 1'b0;
    else         sfifo_clr <= set_clear;

// --------------------------------------------------------------------
//  Serial FIFO Interface
// --------------------------------------------------------------------
  wire v_fifodat_hit = (offset_wr_i && (offset_ad_i == `SFIFODAT));

  // Write interface
  reg  sfifo_wen;
  always @ (posedge pciclk_i, negedge rstn_i)
    if (~rstn_i) sfifo_wen <= 1'b0;
    else         sfifo_wen <= v_fifodat_hit;

  reg  [31:0]   sfifo_din;
  wire [31:0] v_sfifo_din = v_fifodat_hit ? write_dat_i : sfifo_din;
  always @ (posedge pciclk_i, negedge rstn_i)
    if (~rstn_i) sfifo_din <= 32'd0;
    else         sfifo_din <= v_sfifo_din;

  reg  [8:0]   sfifo_waddr;
  wire [8:0] v_sfifo_waddr = sfifo_clr ? 9'd0
                           : sfifo_wen ? sfifo_waddr + 9'd1 : sfifo_waddr;
  always @ (posedge pciclk_i, negedge rstn_i)
    if (~rstn_i) sfifo_waddr <= 9'd0;
    else         sfifo_waddr <= v_sfifo_waddr;

  // Read interface
  wire [31:0] sfifo_dout; 
  wire       v_sfifo_ren;
  reg  [8:0]   sfifo_raddr;
  wire [8:0] v_sfifo_raddr = sfifo_clr   ? 9'd0
                           : v_sfifo_ren ? sfifo_raddr + 9'd1 : sfifo_raddr;
  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) sfifo_raddr <= 9'd0;
    else         sfifo_raddr <= v_sfifo_raddr;

// --------------------------------------------------------------------
//  Serial FIFO Full/Empty
// --------------------------------------------------------------------
  reg    sync_wen;
  wire v_sync_wen = sync_wen ? 1'b0 : v_fifodat_hit ? 1'b1 : sync_wen;
  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) sync_wen <= 1'b0;
    else         sync_wen <= v_sync_wen;

// FIFO only 1 data, if be read fifo empty
  reg    sfifo_empty;
  wire v_sfifo_empty = sfifo_clr ? 1'b1
                     : (v_sfifo_ren & (sfifo_waddr == sfifo_raddr + 9'd1)) ? 1'b1
                     : (sync_wen ? 1'b0 : sfifo_empty);
  always @ (posedge sysclk_i, negedge rstn_i)
    if (~rstn_i) sfifo_empty <= 1'b1;
    else         sfifo_empty <= v_sfifo_empty;

  reg    sync_ren;
  wire v_sync_ren = sync_ren ? 1'b0 : (v_sfifo_ren ? 1'b1 : sync_ren);
  always @ (posedge pciclk_i, negedge rstn_i)
    if (~rstn_i) sync_ren <= 1'b0;
    else         sync_ren <= v_sync_ren;

// FIFO already write full (write number of ADDR_MAX data)
  reg    sfifo_full;
  wire v_sfifo_full = sfifo_clr ? 1'b0
                    : (sfifo_wen & (sfifo_waddr == sfifo_raddr + 9'd511)) ? 1'b1
                    : (sync_ren ? 1'b0 : sfifo_full);
  always @ (posedge pciclk_i, negedge rstn_i)
    if (~rstn_i) sfifo_full <= 1'b0;
    else         sfifo_full <= v_sfifo_full;

// --------------------------------------------------------------------
//  Pulse Generation Pulse
// --------------------------------------------------------------------
  reg  [30:0]    sfifo_count;
  wire [30:0]  v_sfifo_count = (sfifo_state == PULSE_IDLE) ? 31'd0
                             : (sfifo_count == sfifo_dout[30:0]) ? 31'd0 : sfifo_count + 31'd1;
  always @ (posedge sysclk_i, negedge rstn_i) begin
    if (~rstn_i) sfifo_count <= 31'd0;
    else         sfifo_count <= v_sfifo_count;
  end
  
  assign v_sfifo_ren = (sfifo_state == PULSE_IDLE) ? ~sfifo_empty 
                     : (~sfifo_empty & (sfifo_count == sfifo_dout[30:0]));  
  always @ (*) begin  
   case(sfifo_state)
     PULSE_IDLE : v_sfifo_state = sfifo_empty ? sfifo_state : PULSE_DONE;  
     PULSE_DONE : v_sfifo_state = (sfifo_count == sfifo_dout[30:0]) ? ~sfifo_empty : sfifo_state; 
     default    : v_sfifo_state = sfifo_state;
   endcase
  end
  
  always @ (posedge sysclk_i, negedge rstn_i) begin
    if (~rstn_i) sfifo_state <= PULSE_IDLE;
    else         sfifo_state <= v_sfifo_state;
  end

  assign fifo_config_o = {13'd0, sfifo_clr, sfifo_empty, sfifo_full, 16'd0};

  wire v_fifo_pulse_o = (sfifo_state == PULSE_IDLE) ? 1'b0
                      : (sfifo_empty & (sfifo_count == sfifo_dout)) ? 1'b0 : sfifo_dout[31];
                        

  always @ (posedge sysclk_i, negedge rstn_i) begin
    if (~rstn_i) fifo_pulse_o <= 1'b0;
    else         fifo_pulse_o <= v_fifo_pulse_o;
  end

serial_buffer sbuffer(
    .data      (sfifo_din  ),
    .rd_aclr   (sfifo_clr  ),
    .rdaddress (sfifo_raddr),
    .rdclock   (sysclk_i   ),
    .rden      (v_sfifo_ren),
    .wraddress (sfifo_waddr),
    .wrclock   (pciclk_i   ),
    .wren      (sfifo_wen  ),
    .q         (sfifo_dout ));

endmodule

