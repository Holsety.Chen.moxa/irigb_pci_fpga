// ============================================================
// IRIG-B Decoder
// ============================================================
// `define FOR_SIM

`ifdef  FOR_SIM
  `define TIME_OUT_NUM    24'D500 // 20ms/(25KHz = 40us) = 500
  // Position period max = {PERIOD_8MS, 4'b1111} x 40us = 8.28ms
  // Position period max = {PERIOD_8MS, 4'b0000} x 40ns = 7.68ms
  `define PERIOD_8MS      16'B0000_0000_0000_1100

  // Bit1 period max = {PERIOD_5MS, 3'b111} x 40ns = 5.08ms
  // Bit1 period max = {PERIOD_5MS, 3'b000} x 40ns = 4.80ms
  `define PERIOD_5MS      17'B0_0000_0000_0000_1111

  // Bit0 period max = {PERIOD_2MS, 3'b111} x 40ns = 2.20ms
  // Bit0 period max = {PERIOD_2MS, 3'b000} x 40ns = 1.92ms
  `define PERIOD_2MS      17'B0_0000_0000_0000_0110

`else
  `define TIME_OUT_NUM    24'D500000 // 20ms/(25MHz = 40ns) = 500000
  // Position period max = {PERIOD_P_NUM, 13'b1111111111111} x 40ns = 8.19196ms
  // Position period max = {PERIOD_P_NUM, 13'b0000000000000} x 40ns = 7.86432ms
  `define PERIOD_8MS       7'B0011000

  // Bit1 period max = {PERIOD_1_HSB, 13'b1111111111111} x 40ns = 5.24284ms
  // Bit1 period max = {PERIOD_1_HSB, 13'b0000000000000} x 40ns = 4.91520ms
  `define PERIOD_5MS       7'B0001111

  // Bit0 period max = {PERIOD_1_HSB, 12'b111111111111} x 40ns = 2.12988ms
  // Bit0 period max = {PERIOD_1_HSB, 12'b000000000000} x 40ns = 1.96608ms
  `define PERIOD_2MS       8'B00001100

  // Bit0 period max = {PERIOD_1_HSB, 12'b111111111111} x 40ns = 1.96604ms
  // Bit0 period max = {PERIOD_1_HSB, 12'b000000000000} x 40ns = 1.80224ms
  `define PERIOD_2MS1      8'B00001011
`endif

// ============================================================
// SRAM Order
// ============================================================
  `define IN_IRIG0         2'B00
  `define IN_IRIG1         2'B01

// ============================================================
// PCI Setting
// ============================================================
  `define BAR0_ADW         10
  `define FIFO_ADW          8
  
// Register Offset
  `define SYSCON           (10'H08 >> 2)
  `define LPBTCNT          (10'H0C >> 2)
  `define INPORTCON        (10'H10 >> 2)
  `define OUTPORTCON       (10'H14 >> 2)
  `define PORTDAT          (10'H18 >> 2)
  `define IRIG0DAT0        (10'H1C >> 2)
  `define IRIG0DAT1        (10'H20 >> 2)
  `define IRIG0DAT2        (10'H24 >> 2)
  `define IRIG0DAT3        (10'H28 >> 2)
  `define IRIG0CNT         (10'H2C >> 2)
  `define IRIG1DAT0        (10'H30 >> 2)
  `define IRIG1DAT1        (10'H34 >> 2)
  `define IRIG1DAT2        (10'H38 >> 2)
  `define IRIG1DAT3        (10'H3C >> 2)
  `define IRIG1CNT         (10'H40 >> 2)
  `define PPSCON           (10'H44 >> 2)
  `define PCI_TMCON        (10'H48 >> 2)
  `define PCI_INTSTS       (10'H4C >> 2)
  `define PCI_INTMSK       (10'H50 >> 2)
  `define PPSDETIMEOUT     (10'H54 >> 2)
  `define RTCCON           (10'H58 >> 2)
  `define RTCDAT0          (10'H5C >> 2)
  `define RTCDAT1          (10'H60 >> 2)
  `define RTCDAT2          (10'H64 >> 2)  
  `define RTCDAT3          (10'H68 >> 2)
  `define RTCLS            (10'H6C >> 2)
  `define RTCDST           (10'H70 >> 2)
  `define SFIFOCON         (10'H74 >> 2)
  `define SFIFODAT         (10'H78 >> 2)
  `define NLEDCON          (10'H7C >> 2)
  
// ============================================================
// IRIG Statemechine
// ============================================================
  `define IRIG_IDLE         3'D0
  `define IRIG_DETECT       3'D1
  `define IRIG_START        3'D2
  `define IRIG_DATA         3'D3
  `define IRIG_POSITION     3'D4
  `define IRIG_ERROR        3'D5
  `define IRIG_FINISH       3'D6

  `define FCNT_IDLE         2'D0
  `define FCNT_DONE         2'D1
  `define FCNT_LAST         2'D2
  
// ============================================================
// PCI Sram Statemechine
// ============================================================
  `define IDLE              4'B0000
  `define WR_WT             4'B0001
  `define WR_ST             4'B0010
  `define RD_ST             4'B0100

// ============================================================
// Internal Reset Time
// ============================================================  
  `define RESET_LP          4'B0011 // Reset time 4*(40ns) = 160ns   
  
// ============================================================
// RTC Unit Second Counter
// ============================================================   
`ifdef  FOR_SIM
  `define UNIT_SEC          32'd24999  // 1/(40us) = 25000 - 1
`else   
  `define UNIT_SEC          32'd24999999  // 1/(40ns) = 25000000 - 1
`endif
  `define RTCW_IDLE         2'B00
  `define RTCW_HIT          2'B01
  `define RTCW_VAILD        2'B10
  `define RTCW_END          2'B11  

// ============================================================
// PPS Codec
// ============================================================
`ifdef  FOR_SIM
  `define UNIT_MSEC          16'd24  // 1/(40us) = 25 - 1
`else   
  `define UNIT_MSEC          16'd24999  // 1/(40ns) = 25000 - 1
`endif
  
// ============================================================
// IRIG-B Encoder
// ============================================================  
`ifdef  FOR_SIM
  `define ONEBIT_UNIT      20'D249        // 250 * 40us = 10ms
  `define POSBIT_UNIT      20'D200        // 8ms
  `define HIGHBIT_UNIT     20'D125        // 5ms     
  `define LOWBIT_UNIT      20'D50         // 2ms
`else
  `define ONEBIT_UNIT      20'D249999     // 250000 * 40ns = 10ms
  `define POSBIT_UNIT      20'D200000     // 8ms
  `define HIGHBIT_UNIT     20'D125000     // 5ms     
  `define LOWBIT_UNIT      20'D50000      // 2ms
`endif
  
// ============================================================
// NLED Output
// ============================================================  
  `define NLED_TIME        32'D2499999    // 100ms


  
