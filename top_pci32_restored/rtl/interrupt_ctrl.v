`include "define.v"

module interrupt_ctrl (
   output reg  [31:0] irqs_mask_o,
   // Interrupt Register Output
   output reg  [31:0] irqs_data_o, // interrupt register  
   output wire        interrupt_o, // interrupt

   // IRIG0-B Interrupt
   input wire         irig0_off_i,
   input wire         irig0_fair_i,
   input wire         irig0_perr_i,
   input wire         irig0_right_i,

   // IRIG1-B Interrupt
   input wire         irig1_off_i,
   input wire         irig1_fair_i,
   input wire         irig1_perr_i,
   input wire         irig1_right_i,

   // PPS Decoder
   input wire         ppsdec_fair_i,
   input wire         ppsdec_done_i,

   // PPS Encoder
   input wire         ppsenc_done_i,

   // IRIG Encode
   input wire         irigenc_done_i,

   // Local Bus address
   input wire  [`BAR0_ADW-1:0] offset_ad_i, // Offset addr
   input wire                  offset_rd_i, // Offset read
   input wire                  offset_wr_i, // Offset write
   input wire  [31:0]          write_dat_i,

   input wire         pciclk_i,
   input wire         rstn_i
   );

// --------------------------------------------------------------------
//  For Interrupt Mask Register(0: enable, 1: disable, initial state: 1
// --------------------------------------------------------------------
wire [31:0] v_irqs_mask_o;
wire        v_irqs_mask_hit = (offset_wr_i && (offset_ad_i == `PCI_INTMSK));

assign v_irqs_mask_o[ 0] = v_irqs_mask_hit ? write_dat_i[ 0] : irqs_mask_o[ 0];
assign v_irqs_mask_o[ 1] = v_irqs_mask_hit ? write_dat_i[ 1] : irqs_mask_o[ 1];
assign v_irqs_mask_o[ 2] = v_irqs_mask_hit ? write_dat_i[ 2] : irqs_mask_o[ 2];
assign v_irqs_mask_o[ 3] = v_irqs_mask_hit ? write_dat_i[ 3] : irqs_mask_o[ 3];
assign v_irqs_mask_o[ 4] = v_irqs_mask_hit ? write_dat_i[ 4] : irqs_mask_o[ 4];
assign v_irqs_mask_o[ 5] = v_irqs_mask_hit ? write_dat_i[ 5] : irqs_mask_o[ 5];
assign v_irqs_mask_o[ 6] = v_irqs_mask_hit ? write_dat_i[ 6] : irqs_mask_o[ 6];
assign v_irqs_mask_o[ 7] = v_irqs_mask_hit ? write_dat_i[ 7] : irqs_mask_o[ 7];
assign v_irqs_mask_o[ 8] = v_irqs_mask_hit ? write_dat_i[ 8] : irqs_mask_o[ 8];
assign v_irqs_mask_o[ 9] = v_irqs_mask_hit ? write_dat_i[ 9] : irqs_mask_o[ 9];
assign v_irqs_mask_o[10] = v_irqs_mask_hit ? write_dat_i[10] : irqs_mask_o[10];
assign v_irqs_mask_o[11] = v_irqs_mask_hit ? write_dat_i[11] : irqs_mask_o[11];
assign v_irqs_mask_o[12] = v_irqs_mask_hit ? write_dat_i[12] : irqs_mask_o[12];
assign v_irqs_mask_o[13] = v_irqs_mask_hit ? write_dat_i[13] : irqs_mask_o[13];
assign v_irqs_mask_o[14] = v_irqs_mask_hit ? write_dat_i[14] : irqs_mask_o[14];
assign v_irqs_mask_o[15] = v_irqs_mask_hit ? write_dat_i[15] : irqs_mask_o[15];
assign v_irqs_mask_o[16] = v_irqs_mask_hit ? write_dat_i[16] : irqs_mask_o[16];
assign v_irqs_mask_o[17] = v_irqs_mask_hit ? write_dat_i[17] : irqs_mask_o[17];
assign v_irqs_mask_o[18] = v_irqs_mask_hit ? write_dat_i[18] : irqs_mask_o[18];
assign v_irqs_mask_o[19] = v_irqs_mask_hit ? write_dat_i[19] : irqs_mask_o[19];
assign v_irqs_mask_o[20] = v_irqs_mask_hit ? write_dat_i[20] : irqs_mask_o[20];
assign v_irqs_mask_o[21] = v_irqs_mask_hit ? write_dat_i[21] : irqs_mask_o[21];
assign v_irqs_mask_o[22] = v_irqs_mask_hit ? write_dat_i[22] : irqs_mask_o[22];
assign v_irqs_mask_o[23] = v_irqs_mask_hit ? write_dat_i[23] : irqs_mask_o[23];
assign v_irqs_mask_o[24] = v_irqs_mask_hit ? write_dat_i[24] : irqs_mask_o[24];
assign v_irqs_mask_o[25] = v_irqs_mask_hit ? write_dat_i[25] : irqs_mask_o[25];
assign v_irqs_mask_o[26] = v_irqs_mask_hit ? write_dat_i[26] : irqs_mask_o[26];
assign v_irqs_mask_o[27] = v_irqs_mask_hit ? write_dat_i[27] : irqs_mask_o[27];
assign v_irqs_mask_o[28] = v_irqs_mask_hit ? write_dat_i[28] : irqs_mask_o[28];
assign v_irqs_mask_o[29] = v_irqs_mask_hit ? write_dat_i[29] : irqs_mask_o[29];
assign v_irqs_mask_o[30] = v_irqs_mask_hit ? write_dat_i[30] : irqs_mask_o[30];
assign v_irqs_mask_o[31] = v_irqs_mask_hit ? write_dat_i[31] : irqs_mask_o[31];

always @ (posedge pciclk_i, negedge rstn_i) begin
  if (~rstn_i) irqs_mask_o <= 32'hffffffff;
  else         irqs_mask_o <= v_irqs_mask_o;
end

// --------------------------------------------------------------------
//  For IRIG0-B, IRIG1-B, ... Interrupt Register
// --------------------------------------------------------------------
wire [31:0] v_irqs_data_o;

reg      irqsdat_hit;
wire   v_irqsdat_hit = (offset_rd_i && (offset_ad_i == `PCI_INTSTS));

assign v_irqs_data_o[ 0] = irig0_off_i;
assign v_irqs_data_o[ 1] = irqsdat_hit ? 1'b0 : irig0_fair_i   ? 1'b1 : irqs_data_o[ 1];
assign v_irqs_data_o[ 2] = irqsdat_hit ? 1'b0 : irig0_perr_i   ? 1'b1 : irqs_data_o[ 2];
assign v_irqs_data_o[ 3] = irqsdat_hit ? 1'b0 : irig0_right_i  ? 1'b1 : irqs_data_o[ 3];
assign v_irqs_data_o[ 4] = irig1_off_i;
assign v_irqs_data_o[ 5] = irqsdat_hit ? 1'b0 : irig1_fair_i   ? 1'b1 : irqs_data_o[ 5];
assign v_irqs_data_o[ 6] = irqsdat_hit ? 1'b0 : irig1_perr_i   ? 1'b1 : irqs_data_o[ 6];
assign v_irqs_data_o[ 7] = irqsdat_hit ? 1'b0 : irig1_right_i  ? 1'b1 : irqs_data_o[ 7];
assign v_irqs_data_o[ 8] = irqsdat_hit ? 1'b0 : ppsdec_fair_i  ? 1'b1 : irqs_data_o[ 8];
assign v_irqs_data_o[ 9] = irqsdat_hit ? 1'b0 : ppsdec_done_i  ? 1'b1 : irqs_data_o[ 9];
assign v_irqs_data_o[10] = irqsdat_hit ? 1'b0 : ppsenc_done_i  ? 1'b1 : irqs_data_o[10];
assign v_irqs_data_o[11] = irqsdat_hit ? 1'b0 : irigenc_done_i ? 1'b1 : irqs_data_o[11];
assign v_irqs_data_o[31:12] = irqs_data_o[31:12];

reg  [31:0]   irqs_status;
wire [31:0] v_irqs_status;
assign v_irqs_status[ 0] = ~irqs_mask_o[0] & irig0_off_i;

assign v_irqs_status[ 1] = irqsdat_hit ? 1'b0 : irig0_fair_i   ? ~irqs_mask_o[ 1] : irqs_status[ 1];							 
assign v_irqs_status[ 2] = irqsdat_hit ? 1'b0 : irig0_perr_i   ? ~irqs_mask_o[ 2] : irqs_status[ 2];
assign v_irqs_status[ 3] = irqsdat_hit ? 1'b0 : irig0_right_i  ? ~irqs_mask_o[ 3] : irqs_status[ 3];

assign v_irqs_status[ 4] = ~irqs_mask_o[4] & irig1_off_i;

assign v_irqs_status[ 5] = irqsdat_hit ? 1'b0 : irig1_fair_i   ? ~irqs_mask_o[ 5] : irqs_status[ 5];
assign v_irqs_status[ 6] = irqsdat_hit ? 1'b0 : irig1_perr_i   ? ~irqs_mask_o[ 6] : irqs_status[ 6];
assign v_irqs_status[ 7] = irqsdat_hit ? 1'b0 : irig1_right_i  ? ~irqs_mask_o[ 7] : irqs_status[ 7];
assign v_irqs_status[ 8] = irqsdat_hit ? 1'b0 : ppsdec_fair_i  ? ~irqs_mask_o[ 8] : irqs_status[ 8];
assign v_irqs_status[ 9] = irqsdat_hit ? 1'b0 : ppsdec_done_i  ? ~irqs_mask_o[ 9] : irqs_status[ 9];							 
assign v_irqs_status[10] = irqsdat_hit ? 1'b0 : ppsenc_done_i  ? ~irqs_mask_o[10] : irqs_status[10];  
assign v_irqs_status[11] = irqsdat_hit ? 1'b0 : irigenc_done_i ? ~irqs_mask_o[11] : irqs_status[11]; 

assign v_irqs_status[31:12] = irqs_status[31:12];							 						 

assign interrupt_o = ~(|irqs_status);

always @ (posedge pciclk_i, negedge rstn_i) begin
  if (~rstn_i) begin
       irqs_data_o <= 32'd0;
       irqsdat_hit <= 1'b0;
	   irqs_status <= 32'd0;
  end
  else begin
       irqs_data_o <= v_irqs_data_o;
       irqsdat_hit <= v_irqsdat_hit;
	   irqs_status <= v_irqs_status;
  end
end

endmodule

