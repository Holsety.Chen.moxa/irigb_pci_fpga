`include "define.v"

module ledport_setting(
   output wire [7:0]           ledport_o,

   // Local Bus address
   input  wire [`BAR0_ADW-1:0] offset_ad_i, // Offset addr
   input  wire                 offset_wr_i, // Offset write
   input  wire [31:0]          write_dat_i,

   input  wire [5:0]           outport_i,
   input  wire                 inport0_i,
   input  wire                 inport1_i,
   input  wire                 inport2_i,
   input  wire                 inport3_i,
   input  wire                 inport4_i,
   input  wire                 inport5_i,
   input  wire                 inport6_i,
   input  wire                 inport7_i,
   input  wire                 irig0_end_i,
   input  wire                 irig1_end_i,

   input  wire                 pciclk_i,
   input  wire                 sysclk_i,
   input  wire                 rstn_i
);

// --------------------------------------------------------------------
// Output Port Config Register
// --------------------------------------------------------------------
  wire v_ledport_hit = (offset_wr_i && (offset_ad_i == `NLEDCON));

  // NLED Output Channel Select
  reg  [3:0]   led0_select, led1_select, led2_select, led3_select,
               led4_select, led5_select, led6_select, led7_select;

  wire [3:0] v_led0_select = v_ledport_hit ? write_dat_i[ 3: 0] : led0_select;
  wire [3:0] v_led1_select = v_ledport_hit ? write_dat_i[ 7: 4] : led1_select;
  wire [3:0] v_led2_select = v_ledport_hit ? write_dat_i[11: 8] : led2_select;
  wire [3:0] v_led3_select = v_ledport_hit ? write_dat_i[15:12] : led3_select;
  wire [3:0] v_led4_select = v_ledport_hit ? write_dat_i[19:16] : led4_select;
  wire [3:0] v_led5_select = v_ledport_hit ? write_dat_i[23:20] : led5_select;
  wire [3:0] v_led6_select = v_ledport_hit ? write_dat_i[27:24] : led6_select;
  wire [3:0] v_led7_select = v_ledport_hit ? write_dat_i[31:28] : led7_select;

  always @ (posedge pciclk_i, negedge rstn_i) begin
    if (~rstn_i) begin
       led0_select <= 4'h0;
       led1_select <= 4'h0;
       led2_select <= 4'h0;
       led3_select <= 4'h0;
       led4_select <= 4'h0;
       led5_select <= 4'h0;
       led6_select <= 4'h0;
       led7_select <= 4'h0;
    end
    else begin
       led0_select <= v_led0_select;
       led1_select <= v_led1_select;
       led2_select <= v_led2_select;
       led3_select <= v_led3_select;
       led4_select <= v_led4_select;
       led5_select <= v_led5_select;
       led6_select <= v_led6_select;
       led7_select <= v_led7_select;
    end
  end

  reg  [31:0]   irig0_ledcnt, irig1_ledcnt;

  reg    irig0_enable, irig1_enable;
  wire v_irig0_enable = irig0_end_i ? 1'b1 : (irig0_ledcnt == `NLED_TIME) ? 1'b0 : irig0_enable;
  wire v_irig1_enable = irig1_end_i ? 1'b1 : (irig1_ledcnt == `NLED_TIME) ? 1'b0 : irig1_enable;

  wire [31:0] v_irig0_ledcnt = (irig0_end_i | (irig0_ledcnt == `NLED_TIME)) ? 32'd0
                             : (irig0_enable ? irig0_ledcnt + 1'b1 : irig0_ledcnt);
  wire [31:0] v_irig1_ledcnt = (irig1_end_i | (irig1_ledcnt == `NLED_TIME)) ? 32'd0
                             : (irig1_enable ? irig1_ledcnt + 1'b1 : irig1_ledcnt);

  always @ (posedge sysclk_i, negedge rstn_i) begin
    if (~rstn_i) begin
       irig0_ledcnt <= 32'd0;
       irig1_ledcnt <= 32'd0;	
       irig0_enable <= 1'b0;
       irig1_enable <= 1'b0;
    end
    else begin
       irig0_ledcnt <= v_irig0_ledcnt;
       irig1_ledcnt <= v_irig1_ledcnt;	
       irig0_enable <= v_irig0_enable;
       irig1_enable <= v_irig1_enable;
    end
  end

  assign ledport_o[0] = (led0_select == 4'h0) ? ~outport_i[0]
                      : (led0_select == 4'h1) ? ~outport_i[1]
                      : (led0_select == 4'h2) ? ~outport_i[2]
                      : (led0_select == 4'h3) ? ~outport_i[3]
                      : (led0_select == 4'h4) ? ~outport_i[4]
                      : (led0_select == 4'h5) ? ~outport_i[5]
                      : (led0_select == 4'h6) ? ~irig0_enable
                      : (led0_select == 4'h7) ? ~irig1_enable
                      : (led0_select == 4'h8) ? ~inport0_i
                      : (led0_select == 4'h9) ? ~inport1_i
                      : (led0_select == 4'ha) ? ~inport2_i
                      : (led0_select == 4'hb) ? ~inport3_i
                      : (led0_select == 4'hc) ? ~inport4_i
                      : (led0_select == 4'hd) ? ~inport5_i
                      : (led0_select == 4'he) ? ~inport6_i
                                              : ~inport7_i;

  assign ledport_o[1] = (led1_select == 4'h0) ? ~outport_i[0]
                      : (led1_select == 4'h1) ? ~outport_i[1]
                      : (led1_select == 4'h2) ? ~outport_i[2]
                      : (led1_select == 4'h3) ? ~outport_i[3]
                      : (led1_select == 4'h4) ? ~outport_i[4]
                      : (led1_select == 4'h5) ? ~outport_i[5]
                      : (led1_select == 4'h6) ? ~irig0_enable
                      : (led1_select == 4'h7) ? ~irig1_enable
                      : (led1_select == 4'h8) ? ~inport0_i
                      : (led1_select == 4'h9) ? ~inport1_i
                      : (led1_select == 4'ha) ? ~inport2_i
                      : (led1_select == 4'hb) ? ~inport3_i
                      : (led1_select == 4'hc) ? ~inport4_i
                      : (led1_select == 4'hd) ? ~inport5_i
                      : (led1_select == 4'he) ? ~inport6_i
                                              : ~inport7_i;

  assign ledport_o[2] = (led2_select == 4'h0) ? ~outport_i[0]
                      : (led2_select == 4'h1) ? ~outport_i[1]
                      : (led2_select == 4'h2) ? ~outport_i[2]
                      : (led2_select == 4'h3) ? ~outport_i[3]
                      : (led2_select == 4'h4) ? ~outport_i[4]
                      : (led2_select == 4'h5) ? ~outport_i[5]
                      : (led2_select == 4'h6) ? ~irig0_enable
                      : (led2_select == 4'h7) ? ~irig1_enable
                      : (led2_select == 4'h8) ? ~inport0_i
                      : (led2_select == 4'h9) ? ~inport1_i
                      : (led2_select == 4'ha) ? ~inport2_i
                      : (led2_select == 4'hb) ? ~inport3_i
                      : (led2_select == 4'hc) ? ~inport4_i
                      : (led2_select == 4'hd) ? ~inport5_i
                      : (led2_select == 4'he) ? ~inport6_i
                                              : ~inport7_i;

  assign ledport_o[3] = (led3_select == 4'h0) ? ~outport_i[0]
                      : (led3_select == 4'h1) ? ~outport_i[1]
                      : (led3_select == 4'h2) ? ~outport_i[2]
                      : (led3_select == 4'h3) ? ~outport_i[3]
                      : (led3_select == 4'h4) ? ~outport_i[4]
                      : (led3_select == 4'h5) ? ~outport_i[5]
                      : (led3_select == 4'h6) ? ~irig0_enable
                      : (led3_select == 4'h7) ? ~irig1_enable
                      : (led3_select == 4'h8) ? ~inport0_i
                      : (led3_select == 4'h9) ? ~inport1_i
                      : (led3_select == 4'ha) ? ~inport2_i
                      : (led3_select == 4'hb) ? ~inport3_i
                      : (led3_select == 4'hc) ? ~inport4_i
                      : (led3_select == 4'hd) ? ~inport5_i
                      : (led3_select == 4'he) ? ~inport6_i
                                              : ~inport7_i;

  assign ledport_o[4] = (led4_select == 4'h0) ? ~outport_i[0]
                      : (led4_select == 4'h1) ? ~outport_i[1]
                      : (led4_select == 4'h2) ? ~outport_i[2]
                      : (led4_select == 4'h3) ? ~outport_i[3]
                      : (led4_select == 4'h4) ? ~outport_i[4]
                      : (led4_select == 4'h5) ? ~outport_i[5]
                      : (led4_select == 4'h6) ? ~irig0_enable
                      : (led4_select == 4'h7) ? ~irig1_enable
                      : (led4_select == 4'h8) ? ~inport0_i
                      : (led4_select == 4'h9) ? ~inport1_i
                      : (led4_select == 4'ha) ? ~inport2_i
                      : (led4_select == 4'hb) ? ~inport3_i
                      : (led4_select == 4'hc) ? ~inport4_i
                      : (led4_select == 4'hd) ? ~inport5_i
                      : (led4_select == 4'he) ? ~inport6_i
                                              : ~inport7_i;

  assign ledport_o[5] = (led5_select == 4'h0) ? ~outport_i[0]
                      : (led5_select == 4'h1) ? ~outport_i[1]
                      : (led5_select == 4'h2) ? ~outport_i[2]
                      : (led5_select == 4'h3) ? ~outport_i[3]
                      : (led5_select == 4'h4) ? ~outport_i[4]
                      : (led5_select == 4'h5) ? ~outport_i[5]
                      : (led5_select == 4'h6) ? ~irig0_enable
                      : (led5_select == 4'h7) ? ~irig1_enable
                      : (led5_select == 4'h8) ? ~inport0_i
                      : (led5_select == 4'h9) ? ~inport1_i
                      : (led5_select == 4'ha) ? ~inport2_i
                      : (led5_select == 4'hb) ? ~inport3_i
                      : (led5_select == 4'hc) ? ~inport4_i
                      : (led5_select == 4'hd) ? ~inport5_i
                      : (led5_select == 4'he) ? ~inport6_i
                                              : ~inport7_i;

  assign ledport_o[6] = (led6_select == 4'h0) ? ~outport_i[0]
                      : (led6_select == 4'h1) ? ~outport_i[1]
                      : (led6_select == 4'h2) ? ~outport_i[2]
                      : (led6_select == 4'h3) ? ~outport_i[3]
                      : (led6_select == 4'h4) ? ~outport_i[4]
                      : (led6_select == 4'h5) ? ~outport_i[5]
                      : (led6_select == 4'h6) ? ~irig0_enable
                      : (led6_select == 4'h7) ? ~irig1_enable
                      : (led6_select == 4'h8) ? ~inport0_i
                      : (led6_select == 4'h9) ? ~inport1_i
                      : (led6_select == 4'ha) ? ~inport2_i
                      : (led6_select == 4'hb) ? ~inport3_i
                      : (led6_select == 4'hc) ? ~inport4_i
                      : (led6_select == 4'hd) ? ~inport5_i
                      : (led6_select == 4'he) ? ~inport6_i
                                              : ~inport7_i;

  assign ledport_o[7] = (led7_select == 4'h0) ? ~outport_i[0]
                      : (led7_select == 4'h1) ? ~outport_i[1]
                      : (led7_select == 4'h2) ? ~outport_i[2]
                      : (led7_select == 4'h3) ? ~outport_i[3]
                      : (led7_select == 4'h4) ? ~outport_i[4]
                      : (led7_select == 4'h5) ? ~outport_i[5]
                      : (led7_select == 4'h6) ? ~irig0_enable
                      : (led7_select == 4'h7) ? ~irig1_enable
                      : (led7_select == 4'h8) ? ~inport0_i
                      : (led7_select == 4'h9) ? ~inport1_i
                      : (led7_select == 4'ha) ? ~inport2_i
                      : (led7_select == 4'hb) ? ~inport3_i
                      : (led7_select == 4'hc) ? ~inport4_i
                      : (led7_select == 4'hd) ? ~inport5_i
                      : (led7_select == 4'he) ? ~inport6_i
                                              : ~inport7_i;

endmodule
