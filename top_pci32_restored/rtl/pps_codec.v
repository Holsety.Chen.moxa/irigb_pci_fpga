`include "define.v"

module pps_codec
(
   output wire [31:0]          setpps_wid_o,
   output reg  [31:0]          pps_timeout_o,

   output reg                  decode_pps_o,
   output reg                  decpps_err_o,
   output reg                  encode_pps_o,
   output reg                  encpps_end_o,

   // Local Bus address
   input  wire [`BAR0_ADW-1:0] offset_ad_i, // Offset addr
   input  wire                 offset_wr_i, // Offset write
   input  wire [31:0]          write_dat_i,

   input  wire                 decode_pps_i,
   input  wire                 second_hit_i,
   input  wire                 pciclk_i,
   input  wire                 sysclk_i,
   input  wire                 rstn_i
);

// =======================================================
// PCI Setting PPS Codec
// =======================================================
// Setting Decoder Width
  wire v_ppscon_hit = (offset_wr_i && (offset_ad_i == `PPSCON));

  reg  [15:0]   decpps_width;
  wire [15:0] v_decpps_width = v_ppscon_hit ? write_dat_i[15:0] : decpps_width;
  always @ (posedge pciclk_i, negedge rstn_i)
      if (~rstn_i) decpps_width <= 16'd0;
      else         decpps_width <= v_decpps_width;

// Setting Encoder Width
  reg  [15:0]   encpps_width;
  wire [15:0] v_encpps_width = v_ppscon_hit ? write_dat_i[31:16] : encpps_width;
  always @ (posedge pciclk_i, negedge rstn_i)
      if (~rstn_i) encpps_width <= 16'd0;
      else         encpps_width <= v_encpps_width;

// Decoder Time Off
  wire v_timeoff_hit = (offset_wr_i && (offset_ad_i == `PPSDETIMEOUT));

  wire [31:0] v_pps_timeout_o = v_timeoff_hit ? write_dat_i : pps_timeout_o;

  always @ (posedge pciclk_i, negedge rstn_i)
      if (~rstn_i) pps_timeout_o <= 32'd0;
      else         pps_timeout_o <= v_pps_timeout_o;

  assign setpps_wid_o = {encpps_width, decpps_width};

// =======================================================
// Millisecond Counter
// =======================================================
  reg  [15:0]   msec_counter;
  wire [15:0] v_msec_counter = (msec_counter == `UNIT_MSEC) ? 16'd0 : msec_counter + 1'b1;
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) msec_counter <= 16'd0;
      else        msec_counter <= v_msec_counter;

  reg    millisec_hit;
  wire v_millisec_hit = (msec_counter == `UNIT_MSEC);
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) millisec_hit <= 1'b0;
      else        millisec_hit <= v_millisec_hit;
   
// =======================================================
//  PPS Decoder
// =======================================================
  reg  [2:0]   decpps_buf;
  wire [2:0] v_decpps_buf ={decpps_buf[1:0], decode_pps_i};
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) decpps_buf <= 3'd0;
      else        decpps_buf <= v_decpps_buf;
	  
  wire v_decpps_rise = (decpps_buf[2:1] == 2'b01);
  wire v_decpps_fall = (decpps_buf[2:1] == 2'b10);

// No signal counter
  reg [31:0] v_nopps_count, nopps_count;
  always @(*) begin
      if (v_decpps_rise | v_decpps_fall | (nopps_count == pps_timeout_o))
          v_nopps_count = 32'd0;
	  else
	      if (millisec_hit)
		      v_nopps_count = nopps_count + 32'd1;
          else
		      v_nopps_count = nopps_count;
  end
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) nopps_count <= 32'd0;
      else        nopps_count <= v_nopps_count;
 
// PPS Decoder Time-out Error
  reg v_decpps_err_o;  
  always @(*) begin
      if (nopps_count == pps_timeout_o)
          v_decpps_err_o = 1'b1;
	  else
	      if (v_decpps_rise | v_decpps_fall)
		      v_decpps_err_o = 1'b0;
          else
		      v_decpps_err_o = decpps_err_o;
  end  
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) decpps_err_o <= 1'b0;
      else        decpps_err_o <= v_decpps_err_o;  

// PPS Decoder High period	  
  reg    decpps_high;
  wire v_decpps_high = v_decpps_rise ? 1'b1 : (v_decpps_fall ? 1'b0 : decpps_high);
  always @ (posedge sysclk_i, negedge rstn_i)
      if(~rstn_i) decpps_high <= 1'b0;
      else        decpps_high <= v_decpps_high; 
 
// PPS Decoder High period Millisecond Counter 
  reg  [15:0] v_decpps_hcnt, decpps_hcnt;
  always @(*) begin
      if (v_decpps_rise | decpps_err_o)
          v_decpps_hcnt = 16'd0;
	  else
	      if (decpps_high & millisec_hit)
		      v_decpps_hcnt = decpps_hcnt + 16'd1;
          else
		      v_decpps_hcnt = decpps_hcnt;
  end  
  always @ (posedge sysclk_i, negedge rstn_i)
    if(~rstn_i) decpps_hcnt <= 16'd0;
    else        decpps_hcnt <= v_decpps_hcnt;

  wire v_decode_pps_o = v_decpps_rise ? (decpps_hcnt > decpps_width) : 1'b0;
  // wire v_decode_pps_o = v_decpps_fall ? (decpps_hcnt > decpps_width) : 1'b0;
  always @ (posedge sysclk_i, negedge rstn_i)
    if(~rstn_i) decode_pps_o <=  1'b0;
    else        decode_pps_o <= v_decode_pps_o;

// =======================================================
//  PPS Encoder
// =======================================================
  reg [15:0] v_encpps_hcnt, encpps_hcnt;
  always @(*) begin
      if (encpps_hcnt == encpps_width)
          v_encpps_hcnt = 16'd0;
	  else
	      if (encode_pps_o & millisec_hit)
		      v_encpps_hcnt = encpps_hcnt + 16'd1;
          else
		      v_encpps_hcnt = encpps_hcnt;
  end    
  always @ (posedge sysclk_i, negedge rstn_i)
    if(~rstn_i) encpps_hcnt <= 16'd0;
    else        encpps_hcnt <= v_encpps_hcnt;

  wire v_encode_pps_o = ((encpps_width == 16'd0) || (encpps_hcnt == encpps_width)) ? 1'b0 : (second_hit_i ? 1'b1 : encode_pps_o);
  always @ (posedge sysclk_i, negedge rstn_i)
    if(~rstn_i) encode_pps_o <= 1'b0;
    else        encode_pps_o <= v_encode_pps_o;

  wire v_encpps_end_o = encode_pps_o & (encpps_hcnt == encpps_width);
  always @ (posedge sysclk_i, negedge rstn_i)
    if(~rstn_i) encpps_end_o <= 1'b0;
    else        encpps_end_o <= v_encpps_end_o;


endmodule
