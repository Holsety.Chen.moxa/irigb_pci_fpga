`include "define.v"

module pci_sram_ctrl (
   output reg                  lt_rdyn_o,

   // SRAM Interface signals
   output reg                  sram_wen_o,     // Write Enable
   output reg                  sram_ren_o,     // Read enable
   output reg  [`BAR0_ADW-1:0] sram_adr_o,     // sram address 8k

   input  wire [31:0]          l_adro,         
   input  wire                 lt_framen,
   input  wire                 lt_framen_q,   
   input  wire                 lt_ackn,        
   input  wire                 lt_dxfrn,       
   input  wire                 cycle_start,
   input  wire                 bar_hit,        
   input  wire                 write_readn,    
 
   input  wire                 clk,            
   input  wire                 rstn            
   ) ;

// SRAM Controller state machine
   reg [3:0] v_sram_state, sram_state;
   reg       v_sram_wen_o, v_sram_ren_o;
   always @ (posedge clk, negedge rstn) begin
     if (~rstn) begin
     sram_state   <= `IDLE;
     sram_wen_o   <= 1'b0;
     sram_ren_o   <= 1'b0;
     end
     else begin
     sram_state   <= v_sram_state;
     sram_wen_o   <= v_sram_wen_o;
     sram_ren_o   <= v_sram_ren_o;
     end
   end

   reg          read_wait;
   wire       v_read_wait = (sram_state == `RD_ST) & lt_dxfrn & ~lt_framen; 
   always @ (posedge clk, negedge rstn)
     if (~rstn) read_wait <= 1'b1;
     else       read_wait <= v_read_wait;   
   
// Generate lt_rdyn_o signal
   wire v_lt_rdyn_o = bar_hit ? write_readn ? lt_framen 
                    : (lt_framen_q | read_wait) : 1'b1;				  
   always @ (posedge clk, negedge rstn)
     if (~rstn) lt_rdyn_o <= 1'b1;
     else       lt_rdyn_o <= v_lt_rdyn_o;

   always @(*) begin
   case(sram_state)
   `IDLE : begin
     v_sram_wen_o   = 1'b0;
     v_sram_ren_o   = (cycle_start & bar_hit & ~write_readn);
     v_sram_state   = (cycle_start & bar_hit)
                    ? (write_readn ? `WR_WT : `RD_ST) : sram_state;
   end
   `WR_WT : begin
     v_sram_wen_o   = 1'b0;
     v_sram_ren_o   = 1'b0;
     v_sram_state   = lt_framen ? `IDLE : (lt_dxfrn ? sram_state : `WR_ST);
   end
   `WR_ST : begin
     v_sram_wen_o   = 1'b1;
     v_sram_ren_o   = 1'b0;
     v_sram_state   = lt_framen ? `IDLE : (lt_dxfrn ? `WR_WT : sram_state);
   end
   `RD_ST : begin
     v_sram_wen_o   = 1'b0;
     v_sram_ren_o   = ~lt_ackn;
     v_sram_state   = lt_framen ? `IDLE : sram_state;
     end
   default : begin
     v_sram_wen_o   = 1'b0;
     v_sram_ren_o   = 1'b0;
     v_sram_state   = `IDLE;
     end
   endcase
   end

   reg    wr_burst_en;
   wire v_wr_burst_en = ((sram_state == `WR_WT) & ~lt_dxfrn) ? 1'b1
                      : lt_framen ? 1'b0 : wr_burst_en;
   always @ (posedge clk, negedge rstn)
     if (~rstn) wr_burst_en <= 1'b0;
     else       wr_burst_en <= v_wr_burst_en;

   reg    write_inc;
   wire v_write_inc = wr_burst_en & ~lt_dxfrn;
   always @ (posedge clk, negedge rstn)
     if (~rstn) write_inc <= 1'b0;
     else       write_inc <= v_write_inc;

   wire v_read_inc = ~write_readn & ~lt_dxfrn & bar_hit;

 
   wire [`BAR0_ADW-1:0] v_sram_adr_o;
   assign v_sram_adr_o = cycle_start ? l_adro[`BAR0_ADW+1:2] 
                       : (v_read_inc | write_inc) ? sram_adr_o + 1'b1 : sram_adr_o;
						 
   always @ (posedge clk, negedge rstn)
     if (~rstn) sram_adr_o <= {`BAR0_ADW{1'b0}};
     else       sram_adr_o <= v_sram_adr_o;

endmodule
