module date_to_days(
  output reg  [9:0] system_days_o,

  input  wire [4:0] month_sel_i,
  input  wire [5:0] date_sel_i,
  input  wire       leapyear_i,
  input  wire       pciclk_i,
  input  wire       rstn_i
);

reg [9:0] v_system_days_o;

always @ (posedge pciclk_i, negedge rstn_i)
  if(~rstn_i) system_days_o <= 10'd0;
  else        system_days_o <= v_system_days_o;

always @ (*) begin
  case(month_sel_i)
    5'h1 :     v_system_days_o = {4'd0, date_sel_i};
    5'h2 :
	  case(date_sel_i)
	           6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06, 6'h07, 6'h08,
        6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16, 6'h17, 6'h18,
        6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26, 6'h27, 6'h28 : begin
	           v_system_days_o[9:8] = 2'b00;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h3;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 1'b1;
		end
		6'h09, 6'h19, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b00;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h4;
			   v_system_days_o[3:0] = 4'h0;
		end
		default :
		       v_system_days_o = 10'h000;
	  endcase
    5'h3 :
      if (leapyear_i) begin // 60
	           v_system_days_o[9:8] = 2'b00;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h6;
			   v_system_days_o[3:0] = date_sel_i[3:0];
      end
      else // 59
        case(date_sel_i)
		  6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06, 6'h07, 6'h08, 6'h09,
          6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16, 6'h17, 6'h18, 6'h19,
		  6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26, 6'h27, 6'h28, 6'h29,
		  6'h31 : begin
	           v_system_days_o[9:8] = 2'b00;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h6;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h1;
		  end
          6'h10, 6'h20, 6'h30 : begin
	           v_system_days_o[9:8] = 2'b00;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h5;
			   v_system_days_o[3:0] = 4'h9;
          end
		  default :
		       v_system_days_o = 10'h000;
        endcase
    5'h4 :
      if (leapyear_i) // 91
        case(date_sel_i)
		  6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06, 6'h07, 6'h08 : begin
	           v_system_days_o[9:8] = 2'b00;
		       v_system_days_o[7:4] = 4'h9;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h1;
		  end
		  6'h09, 6'h19, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]};
			   v_system_days_o[3:0] = 4'h0;
		  end
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16, 6'h17, 6'h18,
		  6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26, 6'h27, 6'h28,
		  6'h30 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} - 4'h1;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h1;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
      else // 90
        case(date_sel_i)
		  6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06, 6'h07, 6'h08, 6'h09 : begin
	           v_system_days_o[9:8] = 2'b00;
		       v_system_days_o[7:4] = 4'h9;
			   v_system_days_o[3:0] = date_sel_i[3:0];
		  end
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16, 6'h17, 6'h18, 6'h19,
		  6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26, 6'h27, 6'h28, 6'h29,
		  6'h30 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} - 4'h1;
			   v_system_days_o[3:0] = date_sel_i[3:0];
          end
		  default :
		       v_system_days_o = 10'h000;
        endcase
    5'h5 :
      if (leapyear_i) // 121
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06, 6'h07, 6'h08,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16, 6'h17, 6'h18,
		  6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26, 6'h27, 6'h28,
		  6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h2;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h1;
		  end
		  6'h09, 6'h19, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h3;
			   v_system_days_o[3:0] = 4'h0;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
      else begin // 120
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h2;
			   v_system_days_o[3:0] = date_sel_i[3:0];
      end
    5'h6 :
      if (leapyear_i) // 152
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06, 6'h07,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16, 6'h17,
		  6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26, 6'h27,
		  6'h30 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h5;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h2;
		  end
		  6'h08, 6'h09, 6'h18, 6'h19, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h6;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h8;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
      else // 151
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06, 6'h07, 6'h08,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16, 6'h17, 6'h18,
		  6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26, 6'h27, 6'h28,
		  6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h5;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h1;
		  end
		  6'h09, 6'h19, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h6;
			   v_system_days_o[3:0] = 4'h0;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
    5'h7 :
      if (leapyear_i) // 182
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06, 6'h07,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16, 6'h17 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h8;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h2;
		  end
		  6'h08, 6'h09 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h9;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h8;
		  end
		  6'h18, 6'h19, 6'h28, 6'h29  : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} - 4'h1;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h8;
		  end
		  6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26, 6'h27,
		  6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} - 4'h2;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h2;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
      else // 181
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06, 6'h07, 6'h08,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16, 6'h17, 6'h18 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h8;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h1;
		  end
          6'h09 : begin
	           v_system_days_o[9:8] = 2'b01;
		       v_system_days_o[7:4] = 4'h9;
			   v_system_days_o[3:0] = 4'h0;
		  end
          6'h19, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} - 4'h1;
			   v_system_days_o[3:0] = 4'h0;
		  end
		  6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26, 6'h27, 6'h28,
		  6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} - 4'h2;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h1;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
    5'h8 :
      if (leapyear_i) // 213
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16,
          6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26,
          6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h1;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h3;
		  end
		  6'h07, 6'h08, 6'h09, 6'h17, 6'h18, 6'h19, 6'h27, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h2;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h7;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
      else // 212
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06, 6'h07,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16, 6'h17,
		  6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26, 6'h27,
		  6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h1;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h2;
		  end
		  6'h08, 6'h09, 6'h18, 6'h19, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h2;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h8;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
    5'h9 :
      if (leapyear_i) // 244
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15,
          6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25,
          6'h30 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h4;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h4;
		  end
		  6'h06, 6'h07, 6'h08, 6'h09,
		  6'h16, 6'h17, 6'h18, 6'h19,
		  6'h26, 6'h27, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h5;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h6;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
      else // 243
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16,
          6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26,
          6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h4;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h3;
		  end
		  6'h07, 6'h08, 6'h09, 6'h17, 6'h18, 6'h19, 6'h27, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h5;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h7;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
    5'h10 :
      if (leapyear_i) // 274
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15,
          6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h7;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h4;
		  end
		  6'h06, 6'h07, 6'h08, 6'h09,
		  6'h16, 6'h17, 6'h18, 6'h19 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h8;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h6;
		  end
		  6'h26, 6'h27, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = 4'h0;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h6;
		  end
		  6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = 4'h0;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h4;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
      else // 273
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 6'h06,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 6'h16,
          6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25, 6'h26 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h7;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h3;
		  end
		  6'h07, 6'h08, 6'h09, 6'h17, 6'h18, 6'h19 : begin
	           v_system_days_o[9:8] = 2'b10;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h8;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h7;
		  end
		  6'h27, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = 4'h0;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h7;
		  end
		  6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = 4'h0;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h3;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
   5'h11 :
      if (leapyear_i) // 305
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14,
          6'h20, 6'h21, 6'h22, 6'h23, 6'h24,
		  6'h30 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]};
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h5;
		  end
		  6'h05, 6'h06, 6'h07, 6'h08, 6'h09,
		  6'h15, 6'h16, 6'h17, 6'h18, 6'h19, 
		  6'h25, 6'h26, 6'h27, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h1;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h5;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
      else // 304
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 
          6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25,
		  6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]};
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h4;
		  end
		  6'h06, 6'h07, 6'h08, 6'h09,
		  6'h16, 6'h17, 6'h18, 6'h19, 
		  6'h26, 6'h27, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h1;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h6;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
   5'h12 :
      if (leapyear_i) // 335
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04,
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14,
          6'h20, 6'h21, 6'h22, 6'h23, 6'h24,
		  6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h3;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h5;
		  end
		  6'h05, 6'h06, 6'h07, 6'h08, 6'h09,
		  6'h15, 6'h16, 6'h17, 6'h18, 6'h19, 
		  6'h25, 6'h26, 6'h27, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h4;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h5;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
      else // 334
        case(date_sel_i)
		         6'h01, 6'h02, 6'h03, 6'h04, 6'h05, 
		  6'h10, 6'h11, 6'h12, 6'h13, 6'h14, 6'h15, 
          6'h20, 6'h21, 6'h22, 6'h23, 6'h24, 6'h25,
		  6'h30, 6'h31 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h3;
			   v_system_days_o[3:0] = date_sel_i[3:0] + 4'h4;
		  end
		  6'h06, 6'h07, 6'h08, 6'h09,
		  6'h16, 6'h17, 6'h18, 6'h19, 
		  6'h26, 6'h27, 6'h28, 6'h29 : begin
	           v_system_days_o[9:8] = 2'b11;
		       v_system_days_o[7:4] = {2'd0, date_sel_i[5:4]} + 4'h4;
			   v_system_days_o[3:0] = date_sel_i[3:0] - 4'h6;
		  end
		  default :
		       v_system_days_o = 10'h000;
        endcase
   default :
		       v_system_days_o = 10'h000;   
  endcase
end

endmodule

