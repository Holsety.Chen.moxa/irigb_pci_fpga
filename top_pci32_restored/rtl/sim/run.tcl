# This script file compiles the all the files in ModelSim 
# and run the simulation
# Make a local work directory
if {[file exists work]} { 
    vdel -lib work -all
    vlib work
} else {
    vlib work
}
# library files
set quartus_home  "c:/altera/quartus41"
vlog $quartus_home/eda/sim_lib/nopli.v 
vlog $quartus_home/eda/sim_lib/220model.v 
vlog $quartus_home/eda/sim_lib/altera_mf.v
vlog $quartus_home/eda/sim_lib/sgate.v


## Source Files
vlog ../rtl/core/t32.vo
vlog ../rtl/local/lcd_cntrl.v
vlog ../rtl/local/mem_cntrl.v
vlog ../rtl/local/perip.v
vlog ../rtl/local/temp_cntrl.v
vlog ../rtl/local/top_local.v
vlog ../rtl/top_pci32.v



# Altera Testbench Components, refer to PCI Testbench User guide for more information 
vlog ../tb/arbiter.v
vlog ../tb/monitor.v
vlog ../tb/mstr_tranx.v
vcom ../tb/CY7C109.vhdl
#vlog ../tb/128Kx8.v


# Top level file
vlog ../tb/altera_tb.v


vsim  work.altera_tb
do wave.do
run -a


