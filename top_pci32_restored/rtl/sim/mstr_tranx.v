//------------------------------------------------------------------
//  Altera PCI testbench
//  MODULE NAME: mstr_tranx
//  COMPANY:  Altera Coporation.
//            www.altera.com
//  FUNCTIONAL DESCRIPTION:
//  Master transactor initiates Master transaction on the PCI bus
//  This file is only for simulation.
//  REVISION HISTORY:
//  Revision 1.1 Description: No change.
//  Revision 1.0 Description: Initial Release.
//
//  Copyright (C) 1991-2001 Altera Corporation, All rights reserved.
//-------------------------------------------------------------------
`timescale 1 ns / 1 ns


module mstr_tranx (clk,                    // clock
                  rstn,                    // reset
                  ad,                      // Address
                  cben,                    // command byte enable
                  par,                     // parity for low dword
                  reqn,                    // Master transactor request
                  gntn,                    // Master transactor grant
                  framen,                  // framen
                  irdyn,                   // irdyn
                  devseln,                 // devseln
                  trdyn,                   // target ready signal
                  stopn,                   // stopn
                  perrn,                   // parity error
                  serrn,                   // system error
                  intan,
                  busfree,                 // indicates that the bus is idle
                  disengage_mstr,          // indicates to disengage the current transaction
                  tranx_success,           // transaction successful
                  trgt_tranx_disca,        // target TRANSACTOR disconnect-A
                  trgt_tranx_discb,        // target TRANSACTOR disconnect-B
                  trgt_tranx_retry,
           switch);       // target TRANSACTOR retry.

   input clk;
   output rstn;
   reg rstn;
   inout[31:0] ad;
   wire[31:0] ad;
   reg[31:0] ad_out;
   inout[3:0] cben;
   wire[3:0] cben;
   reg[3:0] cben_out;
   inout par;
   wire par;
   reg par_out;
   output reqn;
   wire reqn;
   input gntn;
   inout framen;
   wire framen;
   reg framen_out;
   inout irdyn;
   wire irdyn;
   reg irdyn_out;
   input devseln;
   input trdyn;
   input stopn;
   inout perrn;
   wire perrn;
   reg perrn_out;
   inout serrn;
   wire serrn;
   reg serrn_out;
   input intan;
   input busfree;
   input disengage_mstr;
   input tranx_success;
   output trgt_tranx_disca;
   reg trgt_tranx_disca;
   output trgt_tranx_discb;
   reg trgt_tranx_discb;
   output trgt_tranx_retry;
   reg trgt_tranx_retry;
   output [3:0] switch;
   reg [3:0] switch;


   parameter tdel = 6;
   reg par_en;
   reg mstr_tranx_reqn;
   wire mstr_tranx_gntn;

   reg[31:0] command_reg;         // command register of Altera PCI core
   reg[31:0] bar0;                // BAR0 of Altera PCI core
   reg[31:0] bar1;                // BAR1 of Altera PCI core
   reg[31:0] bar2;                // BAR2 of Altera PCI core
   reg[31:0] trgt_tranx_bar0;     // BAR0 of TARGET TRANSACTOR
   reg[31:0] trgt_tranx_bar1;     // BAR1 of TARGET TRANSACTOR

   assign ad = ad_out;
   assign cben = cben_out;
   assign par = par_out;
   assign framen = framen_out;
   assign irdyn = irdyn_out;
   assign perrn = perrn_out;
   assign serrn = serrn_out;

   assign reqn = mstr_tranx_reqn;
   assign mstr_tranx_gntn = gntn;

   wire [31:0] ones = 32'hffff_ffff;
   wire [31:0] zero = 32'h0;
   wire [3:0] cbe = 4'h0;

   //***************************
   //parity gen for lower dword
   //***************************
   always @(posedge clk)
   begin : parity_gen

      reg result_reg;
      result_reg = ad[31] ^ ad[30] ^ ad[29] ^ ad[28] ^
                   ad[27] ^ ad[26] ^ ad[25] ^ ad[24] ^
                   ad[23] ^ ad[22] ^ ad[21] ^ ad[20] ^
                   ad[19] ^ ad[18] ^ ad[17] ^ ad[16] ^
                   ad[15] ^ ad[14] ^ ad[13] ^ ad[12] ^
                   ad[11] ^ ad[10] ^ ad[9] ^ ad[8] ^
                   ad[7] ^ ad[6] ^ ad[5] ^ ad[4] ^
                   ad[3] ^ ad[2] ^ ad[1] ^ ad[0] ^
                   cben[3] ^ cben[2] ^ cben[1] ^ cben[0];
      if (par_en)
      begin
         par_out <= #tdel result_reg ;

      end
      else
      begin
         par_out <= #tdel 1'bz ;
      end
   end

//*******************************************************************************
//TASKS
//*******************************************************************************



   //***************
   task idle_cycle;
   //***************
      input count;
      integer count;

      integer idle_cycle_count;

      begin

         idle_cycle_count = count;

         while (idle_cycle_count > 0)
         begin
            @(posedge clk);
            idle_cycle_count = idle_cycle_count - 1;
         end
      end
   endtask



   //***********
   task cfg_wr;
   //***********
      input[31:0] address;
      input[31:0] data;
      input[3:0] byte_en;

      begin

         //Request for the bus
         mstr_tranx_reqn <= 1'b0 ;

         //Wait for the gnt to be asserted
         while (mstr_tranx_gntn)
         begin
            @(posedge clk);
         end

         //Wait for the bus to be free
         while (!busfree)
         begin
            @(posedge clk);
         end

         //Address Phase
         framen_out <= 1'b0 ;
         ad_out[31:0] <= address ;
         cben_out[3:0] <= 4'b1011 ;
         irdyn_out <= 1'b1 ;
         par_en <= 1'b1 ;

         //Data phase
         @(posedge clk);
         #tdel;
         framen_out <= 1'b1 ;
         irdyn_out <= 1'b0 ;
         cben_out[3:0] <= byte_en ;
         ad_out[31:0] <= data ;
         mstr_tranx_reqn <= 1'b1 ;

         while (!tranx_success & !disengage_mstr)
         begin
            @(posedge clk);
           // #1;
         end
         irdyn_out <= 1'b1 ;
         ad_out <= {32{1'bz}} ;
         cben_out <= {4{1'bz}} ;
         par_en <= 1'b0 ;

         @(posedge clk);
         #tdel;
         drive_z;
      end
   endtask

   //**********
   task cfg_rd;
   //**********

      input[31:0] address;

      begin

         //Request for the bus
         mstr_tranx_reqn <= 1'b0 ;

         //Wait for the gnt to be asserted
         while (mstr_tranx_gntn)
         begin
            @(posedge clk);
         end

         //wait for the bus to be free
         while (!busfree)
         begin
            @(posedge clk);
         end

         //Address Phase
         framen_out <= 1'b0 ;
         irdyn_out <= 1'b1 ;
         ad_out[31:0] <= address ;
         cben_out[3:0] <= 4'b1010 ;
         par_en <= 1'b1 ;


         //Turnaround phase
         @(posedge clk);
         #tdel;
         par_en <= 1'b0 ;
         framen_out <= 1'b1 ;
         irdyn_out <= 1'b0 ;
         cben_out <= {4{1'b0}} ;
         ad_out <= {32{1'bz}} ;
         mstr_tranx_reqn <= 1'b1 ;

         //Data Phase
         while (!tranx_success & !disengage_mstr)
         begin
            @(posedge clk);
            //#1;
         end
         irdyn_out <= 1'b1 ;
         cben_out <= {4{1'bz}} ;

         @(posedge clk);
         #tdel;
         drive_z;
      end
   endtask

   //**********
   task mycfg_rd;
   //**********

      input[31:0] address;

      begin

         //Request for the bus
         mstr_tranx_reqn <= 1'b0 ;

         //Wait for the gnt to be asserted
         while (mstr_tranx_gntn)
         begin
            @(posedge clk);
         end

         //wait for the bus to be free
         while (!busfree)
         begin
            @(posedge clk);
         end

         //Address Phase
         framen_out <= 1'b0 ;
         irdyn_out <= 1'b1 ;
         ad_out[31:0] <= address ;
         cben_out[3:0] <= 4'b1010 ;
         par_en <= 1'b1 ;


         //Turnaround phase
         @(posedge clk);
         #tdel;
         par_en <= 1'b0 ;
         framen_out <= 1'b0 ;
         irdyn_out <= 1'b1 ;
         cben_out <= {4{1'b0}} ;
         ad_out <= {32{1'bz}} ;
         mstr_tranx_reqn <= 1'b1 ;
         @(posedge clk);
     #tdel;
     @(posedge clk);
     #tdel;
     @(posedge clk);
     #tdel;
     @(posedge clk);
     #tdel;
     framen_out <= 1'b1;
     irdyn_out <= 1'b0;
         //Data Phase
         while (!tranx_success & !disengage_mstr)
         begin
            @(posedge clk);
            //#1;
         end
         irdyn_out <= 1'b1 ;
         cben_out <= {4{1'bz}} ;

         @(posedge clk);
         #tdel;
         drive_z;
      end
   endtask


   //*************
   task mem_wr_32;
   //*************
      input[31:0] address;
      input[31:0] data;
      input dword;
      integer dword;

      reg[31:0] data_inc;
      integer dword_cnt;

      begin

        data_inc = data;
        dword_cnt = dword;

         //Request for the bus
         mstr_tranx_reqn <= 1'b0 ;

         //Wait for the gnt to be asserted
         while (mstr_tranx_gntn)
         begin
            @(posedge clk);
         end

         //Wait for the bus to be free
         while (!busfree)
         begin
            @(posedge clk);
         end

         //Address Phase
         framen_out <= 1'b0 ;
         ad_out[31:0] <= address ;
         cben_out[3:0] <= 4'b0111 ;
         par_en <= 1'b1 ;

         //Data Phase
         @(posedge clk);
         #tdel;
         dword_cnt = dword_cnt - 1;
         if (dword > 1)
         begin
            framen_out <= 1'b0 ;
         end
         else
         begin
            framen_out <= 1'b1 ;
         end
         ad_out <= data ;
         cben_out <= {4{1'b0}} ;
         irdyn_out <= 1'b0 ;
         mstr_tranx_reqn <= 1'b1 ;

         while (!tranx_success & !disengage_mstr)
//   while (!tranx_success & stopn)
         begin
            @(posedge clk);
            //#1;
         end

         //--------------Burst Transaction--------------

         if (dword > 1 & !disengage_mstr)
//   if (dword > 1 & !stopn)
         begin
            while (dword_cnt > 0 & !disengage_mstr)
//      while (dword_cnt > 0 & !stopn)
            begin
               if (dword_cnt == 1)
               begin
                  framen_out <= 1'b1 ;
               end
               if (!irdyn & !trdyn)
               begin
                  dword_cnt = dword_cnt - 1;
//                  data_inc = data_inc + 32'h00000001;
          data_inc = {(data_inc[31:24] + 1'h1), (data_inc[23:16] + 1'h1), (data_inc[15:8] + 1'h1), (data_inc[7:0] + 1'h1)};

               end
               ad_out[31:0] <= data_inc ;
               @(posedge clk);
               while (!tranx_success & !disengage_mstr)
               begin
                  @(posedge clk);
                  //#1;
               end
            end
         end
         //--------------------------------------------------

         if (!irdyn & !trdyn)
         begin
//            data_inc = data_inc + 32'h00000001;
        data_inc = {(data_inc[31:24] + 1'h1), (data_inc[23:16] + 1'h1), (data_inc[15:8] + 1'h1), (data_inc[7:0] + 1'h1)};

         end
         ad_out[31:0] <= data_inc ;
         if ((disengage_mstr & dword > 1 & !framen) | (!disengage_mstr & dword_cnt == 1))
         begin
            framen_out <= 1'b1 ;
            @(posedge clk);
            while (!tranx_success & !disengage_mstr)
            begin
               @(posedge clk);
               //#1;
            end
         end
         irdyn_out <= 1'b1 ;
         ad_out <= {32{1'bz}} ;
         cben_out <= {4{1'bz}} ;
         par_en <= 1'b0 ;

         @(posedge clk);
         #tdel;
         drive_z;
         if (!devseln)
         begin
            @(posedge devseln);
         end
      end
   endtask // mem_wr_32


   task my_mem_wr_32;
   //*************
      input[31:0] address;
      input[31:0] data;
      input dword;
      input [2:0] waits;
      integer dword;

      reg[31:0] data_inc;
      integer dword_cnt;
      reg [2:0] tmp_waits;
      begin

         data_inc = data;
         dword_cnt = dword;
         tmp_waits = waits;
         //Request for the bus
         mstr_tranx_reqn <= 1'b0 ;

         //Wait for the gnt to be asserted
         while (mstr_tranx_gntn)
         begin
            @(posedge clk);
         end

         //Wait for the bus to be free
         while (!busfree)
            @(posedge clk);

         //Address Phase
     @(posedge clk);
     par_en <= 1'b1 ;
     #tdel;
         framen_out <= 1'b0 ;
         ad_out[31:0] <= address ;
         cben_out[3:0] <= 4'b0111 ;

         //Data Phase
     @(posedge clk);
     ad_out <= data_inc ;
         cben_out <= {4{1'b0}} ;
     if (|tmp_waits[2:0]) begin
        irdyn_out <= 1'b1;
        while (|tmp_waits[2:0]) begin
           @ (posedge clk);
           tmp_waits = tmp_waits - 1'b1;
        end
        #tdel;
     end
     tmp_waits = waits;
     irdyn_out <= 1'b0;

     while (dword_cnt > 1 & !disengage_mstr) begin
        ad_out <= data_inc ;
            cben_out <= 4'b0;
            irdyn_out <= 1'b0 ;
        @(posedge clk);
        // wait for successful transfer
        //while (!tranx_success & !disengage_mstr)
        if (!stopn)
          dword_cnt = 0;
        else if (trdyn)
          while(trdyn)
        @(posedge clk);

          dword_cnt = dword_cnt -1;
//      $display("%t dword_cnt = %d",$time,dword_cnt);

        data_inc = {(data_inc[31:24] + 1'b1),(data_inc[23:16] + 1'b1),
            (data_inc[15:8] + 1'b1),(data_inc[7:0] + 1'b1)} ;
        #tdel;
            // Introduce wait states
        if (|tmp_waits[2:0] & stopn) begin
           irdyn_out <= 1'b1;
           while (|tmp_waits[2:0]) begin
          @ (posedge clk);
          tmp_waits = tmp_waits - 1'b1;
           end
           #tdel;
           tmp_waits = waits;
           irdyn_out <= 1'b0;
        end
     end // while (dword_cnt > 1)

     // Final Data Phase
     framen_out <= 1'b1;
     irdyn_out <= 1'b0;
     ad_out <= data_inc;
     cben_out <= 4'b0;
     // Wait for xfer to be over
         @(posedge clk);
     #tdel;
     irdyn_out <= 1'b1;
     ad_out <= 32'bz;
     cben_out <= 4'bz;
     par_en <= 1'b0;
     @ (posedge clk);
     #tdel;
     drive_z;
         if (!devseln)
            @(posedge devseln);
     idle_cycle(1);
      end
   endtask // my_mem_wr_32


   //*************
   task mem_rd_32;
   //*************

      input[31:0] address;
      input dword;
      integer dword;

      integer dword_cnt;

      begin

         dword_cnt = dword;

         //Request for the bus
         mstr_tranx_reqn <= 1'b0 ;

         //Wait for the gnt to be asserted
         while (mstr_tranx_gntn)
         begin
            @(posedge clk);
         end

         //Wait for the bus to be free to start the transaction
         while (!busfree)
         begin
            @(posedge clk);
         end
         @(posedge clk);
     #tdel;
         //Address Phase
          framen_out <= 1'b0 ;
         ad_out[31:0] <= address ;
         cben_out[3:0] <= 4'b0110 ;
         par_en <= 1'b1 ;

         //Turnaround Phase
         @(posedge clk);
         #tdel;
         dword_cnt = dword_cnt - 1;
         if (dword > 1)
         begin
            framen_out <= 1'b0 ;
         end
         else
         begin
            framen_out <= 1'b1 ;
         end
         ad_out <= {32{1'bz}} ;
         cben_out <= {4{1'b0}} ;
         irdyn_out <= 1'b0 ;
         par_en <= 1'b0 ;
         mstr_tranx_reqn <= 1'b1 ;

         //Data phase
         while (!tranx_success & !disengage_mstr)
         begin
            @(posedge clk);
            //#1;
         end
/* -----\/----- EXCLUDED -----\/-----
     irdyn_out <= 1'b1;
     @ (posedge clk);
     irdyn_out <= 1'b0;
 -----/\----- EXCLUDED -----/\----- */

         //-----------Burst trasaction--------------------
         if (dword > 1 & !disengage_mstr)
         begin
            while (dword_cnt > 0 & !disengage_mstr)
            begin
               if (dword_cnt == 1)
               begin
                  framen_out <= 1'b1 ;
               end
               dword_cnt = dword_cnt - 1;
               @(posedge clk);
               while (!tranx_success & !disengage_mstr)
               begin
                  @(posedge clk);
                  //#1;
               end
            end
         end
         //----------------------------------------------------


         if ((disengage_mstr & dword > 1 & !framen) | (!disengage_mstr & dword_cnt == 1))
         begin
            framen_out <= 1'b1 ;
            dword_cnt = dword_cnt - 1;
            @(posedge clk);
            while (!tranx_success & !disengage_mstr)
            begin
               @(posedge clk);
               //#1;
            end
         end
         irdyn_out <= 1'b1 ;
         cben_out <= {4{1'bz}} ;

         @(posedge clk);
         #tdel;
         drive_z;
         if (!devseln)
         begin
            @(posedge devseln);
         end
      end
   endtask // mem_rd_32



   task my_mem_rd_32;
   //*************

      input[31:0] address;
      input dword;
      integer dword;
      input [2:0] waits; // wait state should be 7 or lesser
      input [31:0] exp_d; // expected Data
      integer dword_cnt;
      reg [2:0] tmp_waits;
      reg [31:0] e_data;
      begin
     if (waits > 7)
       $display ("Error: waits cannot be > 7");

     tmp_waits = waits;
         dword_cnt = dword;
     e_data = exp_d;


         //Request for the bus
         mstr_tranx_reqn <= 1'b0 ;

         //Wait for the gnt to be asserted
         while (mstr_tranx_gntn)
         begin
            @(posedge clk);
         end

         //Wait for the bus to be free to start the transaction
         while (!busfree)
         begin
            @(posedge clk);
         end

         //Address Phase
         framen_out <= 1'b0 ;
         ad_out[31:0] <= address ;
         cben_out[3:0] <= 4'b0110 ;
         par_en <= 1'b1 ;

         //Turnaround Phase
         @(posedge clk);
         #tdel;
     ad_out <= {32{1'bz}} ;
         cben_out <= {4{1'b0}} ;
         dword_cnt = dword_cnt - 1;
     // Introduce wait states
     if (|tmp_waits[2:0]) begin
        while (|tmp_waits[2:0]) begin
           irdyn_out <= 1'b1;
           @ (posedge clk);
           tmp_waits = tmp_waits - 1'b1;
        end
     end
     tmp_waits = waits;
     irdyn_out <= 1'b0;

         if (dword > 1)
           framen_out <= 1'b0 ;
         else
            framen_out <= 1'b1 ;


         //Data phase
         while (!tranx_success & !disengage_mstr)
         begin
            @(posedge clk);
            //#1;
         end
     if (ad[31:0] != e_data)
       $display ("Error: %d: Exp. Data = %h Actual = %h",$time,e_data,ad[31:0]);

       e_data = {(e_data[31:24] + 1'b1),(e_data[23:16] + 1'b1),
            (e_data[15:8] + 1'b1),(e_data[7:0] + 1'b1)} ;

     // Introduce wait states
     if (|tmp_waits[2:0]) begin
        while (|tmp_waits[2:0]) begin
           irdyn_out <= 1'b1;
           @ (posedge clk);
           tmp_waits = tmp_waits - 1'b1;
        end
        tmp_waits = waits;
        irdyn_out <= 1'b0;
     end

         //-----------Burst trasaction--------------------
         if (dword > 1 & !disengage_mstr)
         begin
            while (dword_cnt > 0 & !disengage_mstr)
            begin
               if (dword_cnt == 1)
               begin
                  framen_out <= 1'b1 ;
               end
               dword_cnt = dword_cnt - 1;
               @(posedge clk);
               while (!tranx_success & !disengage_mstr)
               begin
                  @(posedge clk);
                  //#1;
               end
           // Some checking
           if (ad[31:0] != e_data)
         $display ("Error: %d: Exp. Data = %h Actual = %h",$time,e_data,ad[31:0]);

         e_data = {(e_data[31:24] + 1'b1),(e_data[23:16] + 1'b1),
            (e_data[15:8] + 1'b1),(e_data[7:0] + 1'b1)} ;
           // Introduce wait states
           if (|tmp_waits[2:0]) begin
          while (|tmp_waits[2:0]) begin
             irdyn_out <= 1'b1;
             @ (posedge clk);
             tmp_waits = tmp_waits - 1'b1;
          end
          tmp_waits = waits;
          irdyn_out <= 1'b0;
               end
        end // while (dword_cnt > 0 & !disengage_mstr)
         end // if (dword > 1 & !disengage_mstr)
         //----------------------------------------------------


         if ((disengage_mstr & dword > 1 & !framen) | (!disengage_mstr & dword_cnt == 1))
         begin
            framen_out <= 1'b1 ;
            dword_cnt = dword_cnt - 1;
            @(posedge clk);
            while (!tranx_success & !disengage_mstr)
            begin
               @(posedge clk);
               //#1;
            end
        if (ad[31:0] != e_data)
          $display ("Error: %d:, Exp. Data = %h Actual = %h",$time,e_data,ad[31:0]);

          e_data = {(e_data[31:24] + 1'b1),(e_data[23:16] + 1'b1),
            (e_data[15:8] + 1'b1),(e_data[7:0] + 1'b1)} ;
         end // if ((disengage_mstr & dword > 1 & !framen) | (!disengage_mstr & dword_cnt == 1))

         irdyn_out <= 1'b1 ;
         cben_out <= {4{1'bz}} ;

         @(posedge clk);
         #tdel;
         drive_z;
         if (!devseln)
         begin
            @(posedge devseln);
         end
      end
   endtask // my_mem_rd_32



   //**********
   task io_wr;
   //**********
      input[31:0] address;
      input[31:0] data;

      begin

         //Request for the bus
         mstr_tranx_reqn <= 1'b0 ;

         //Wait for the gnt to be asserted
         while (mstr_tranx_gntn)
           @(posedge clk);

         //Wait for the bus to be free to start the transaction
         while (!busfree)
           @(posedge clk);

         //Address Phase
     @(posedge clk);
         framen_out <= 1'b0 ;
     irdyn_out <= 1'b1;
         ad_out[31:0] <= address ;
         cben_out[3:0] <= 4'b0011 ;
         par_en <= 1'b1 ;
         //Data phase
         @(posedge clk);
         #tdel;
         framen_out <= 1'b1 ;
         irdyn_out <= 1'b0 ;
         ad_out[31:0] <= data ;
         cben_out[3:0] <= 4'b0000 ;
         while (trdyn & stopn)
           @(posedge clk);
     // turnaround phase
         irdyn_out <= 1'b1 ;
         ad_out <= {32{1'bz}} ;
         cben_out <= {4{1'bz}} ;
         par_en <= 1'b0 ;
         @(posedge clk);
         #tdel;
         drive_z;
         if (!devseln)
         begin
            @(posedge devseln);
         end
     idle_cycle(1);
      end
   endtask // io_wr


   //*********
   task io_rd;
   //*********
      input[31:0] address;

      begin

         //Request for the bus
         mstr_tranx_reqn <= 1'b0 ;

         //Wait for the gnt to be asserted
         while (mstr_tranx_gntn)
           @(posedge clk);

         //Wait for the bus to be free
         while (!busfree)
           @(posedge clk);


         //Address Phase
     @(posedge clk);
         framen_out <= 1'b0 ;
     irdyn_out  <= 1'b1;
         ad_out[31:0] <= address ;
         cben_out[3:0] <= 4'b0010 ;
         par_en <= 1'b1 ;

         //Turn around phase
         @(posedge clk);
         #tdel;
         framen_out <= 1'b1 ;
         irdyn_out <= 1'b0 ;
         ad_out <= {32{1'bz}} ;
         cben_out <= 4'b0000 ;
         par_en <= 1'b0 ;
         mstr_tranx_reqn <= 1'b1 ;

         //Data phase
         while (trdyn & stopn)
         begin
            @(posedge clk);
            //#1;
         end
         irdyn_out <= 1'b1 ;
         ad_out <= {32{1'bz}} ;
         cben_out <= {4{1'bz}} ;

         @(posedge clk);
         #tdel;
         drive_z;
         if (!devseln)
         begin
            @(posedge devseln);
         end
     idle_cycle(1);

      end
   endtask

   //***********
   task drive_z;
   //***********

      begin
         ad_out <= {32{1'bz}} ;
         cben_out <= {4{1'bz}} ;
         framen_out <= 1'bz ;
         irdyn_out <= 1'bz ;

      end
   endtask
   task cfg_rd_wr;
      input [31:0] addr;
      input [31:0] data;
      begin
     cfg_rd(addr);
     cfg_wr(addr,data,cbe);
     cfg_rd(addr);
      end
   endtask


   //*************************************
   //Main
   //**************************************
   wire [9:2] some_ones = 8'b111111_11;

   always
   begin : main
      rstn <= 1'b0 ;
      trgt_tranx_disca <= 1'b0 ;
      trgt_tranx_discb <= 1'b0 ;
      trgt_tranx_retry <= 1'b0 ;
      par_en <= 1'b0 ;
      mstr_tranx_reqn <= 1'b1 ;
      perrn_out <= 1'bz ;
      serrn_out <= 1'bz ;
      drive_z;
      idle_cycle(10);
      switch <= 'hf;
      #6;
      rstn <= 1'b1 ;
      idle_cycle(3);

//      @(posedge clk);



//**********************************
//INITIALIZATION
//**********************************

      //SYSTEM RESET



      //CONFIG PARAMETERS OF PCI INTERFACE SPECIFIC TO THE APPLICATION DESIGN


      command_reg <= 32'h00000147 ;  //COMMAND REGISTER FOR ALTERA PCI MEGACORE
      bar0 <= 32'h10000000 ;         //BAR0 OF ALTERA PCI MEGACORE
      bar1 <= 32'hfffffff0 ;         //BAR1 OF ALTERA PCI MEGACORE
      bar2 <= 32'h55000000 ;         //BAR2 OF ALTERA PCI MEGACORE

      idle_cycle(1);

// Set up the target
      cfg_wr(32'h1000_0004,command_reg,4'h0); // Command Register enable memory and I/O response
      cfg_wr(32'h1000_0010,bar0,4'h0);        // BAR0 Memory space
      cfg_wr(32'h1000_0014,bar1,4'h0);        //BAR1  I/O Space
// Test the memory
      mem_wr_32(bar0 ,32'h40_30_20_10,2);
      #50;
      mem_wr_32(bar0 + 32'h8,32'h43_33_23_13,1);
      mem_wr_32(bar0 + 32'h8,32'h43_33_23_13,1);
      #150;

      mem_rd_32(bar0,3);

      my_mem_wr_32(bar0 +16,32'h80_70_60_50,4,2); // with wait states
      mem_rd_32(bar0+32'h8,2);
      mem_rd_32(bar0,3);

      #150;
      mem_rd_32(bar0+16,4);
      #150;
      my_mem_rd_32(bar0+16,4,3,32'h80_70_60_50); // wait states
      mem_rd_32(bar0,3);
      mem_rd_32(bar0,1);
      mem_rd_32(bar0+32'h4,1);

      #150;
      mem_wr_32(bar0 ,32'h40_30_20_10,10);
      mem_rd_32(bar0,10);
      #150;
      my_mem_rd_32(bar0, 6,1,32'h80_70_60_50);

      // mem_wr_32 (address, data, double-word)
      #150;
      mem_wr_32(bar0+(10'H08 >> 2)*4, 32'h0000_0001, 1);  // `define SYSCON           (10'H08 >> 2)
      mem_wr_32(bar0+(10'H10 >> 2)*4, 32'h0000_0088, 1);  // `define INPORTCON        (10'H10 >> 2)
      mem_wr_32(bar0+(10'H58 >> 2)*4, 32'h0000_0001, 1);  // `define RTCCON           (10'H58 >> 2)

      #150;
      mem_rd_32(bar0+(10'H4C >> 2)*4, 10);                // `define PCI_INTSTS       (10'H4C >> 2)
      mem_wr_32(bar0+(10'H74 >> 2)*4, 32'h0000_0002, 1);  // `define SFIFOCON         (10'H74 >> 2)
      mem_wr_32(bar0+(10'H78 >> 2)*4, 32'h0003_0003, 1);  // `define SFIFODAT         (10'H78 >> 2)

   end


endmodule
