//------------------------------------------------------------------
//  Altera PCI testbench
//  MODULE NAME: altera_tb
//  COMPANY:  Altera Coporation.
//            www.altera.com    

//  FUNCTIONAL DESCRIPTION:
//  This is the top level file of Altera PCI testbench

//  REVISION HISTORY:  
//  Revision 1.1 Description: No change.
//  Revision 1.0 Description: Initial Release.
//
//  Copyright (C) 1991-2001 Altera Corporation, All rights reserved.  
//-------------------------------------------------------------------

`timescale 1 ns / 1 ns
`define FOR_SIM
`define CYCLE_25MHZ 40
`define CYCLE_25KHZ 40000
`define CYCLE_33MHZ 30
`define CYCLE_33KHZ 30000
`define CYCLE_01KHZ 1000000

module altera_tb ();
parameter [88:0] IRIG_PATTERN = 89'H0DD_FFFF_1111_3322_BBCC_55AA; // parity bit = 0
parameter     program   = "irig_pci_card";


initial begin
        $dumpfile({program,".fsdb"});
        $dumpvars;
end

   reg            clk;
   // PCI Signals
   reg    	  resetn; 
   wire [31:0] 	  ad; 
   wire [3:0] 	  cben; 
   tri1 	  framen; 
   tri1 	  irdyn; 
   tri1 	  devseln; 
   tri1 	  trdyn; 
   tri1 	  stopn; 
   tri1 	  perrn; 
   tri1 	  serrn; 
   wire 	  intan; 
   wire 	  par;
   // SRAM Interface Signals
   wire [7:0] 	  sram_io;
   wire [16:0] 	  sram_address;
   // lcd interface signal
   wire [7:0] 	  lcd_db;
   assign 	  lcd_db = 8'bz;
   //temp signals
   reg [15:0] 	  temp_data;
   initial
     temp_data = 16'h5a5a;
   reg 		  temp_so;
   // switch and led
   wire [3:0] 	  leds;
   wire [3:0]  switch;
//   assign  	  switch = 3'b0;
   // Testbench control signals
   wire 	  busfree; 
   wire 	  disengage_mstr; 
   wire 	  tranx_success; 
   wire 	  trgt_tranx_disca; 
   wire 	  trgt_tranx_discb; 
   wire 	  trgt_tranx_retry;
   wire 	  mstr_tranx_reqn; 


   wire [1:0] 	  reqns;

   assign 	  reqns = {mstr_tranx_reqn, 1'b1};
   
   wire 	  idsel = ad[28];

`ifdef  FOR_SIM
  initial begin
          clk = 0;          #(`CYCLE_33KHZ/4)
		  clk = 1;  forever #(`CYCLE_33KHZ/2)
		  clk = ~clk;
  end
`else
  initial begin
          clk = 0;          #(`CYCLE_33MHZ/4)
		  clk = 1;  forever #(`CYCLE_33MHZ/2)
		  clk = ~clk;
  end
`endif
   
reg  osc_clk1;
`ifdef  FOR_SIM
  initial begin
          osc_clk1 = 0;          #(`CYCLE_25KHZ/4)
		  osc_clk1 = 1;  forever #(`CYCLE_25KHZ/2)
		  osc_clk1 = ~osc_clk1;
  end
`else
  initial begin
          osc_clk1 = 0;          #(`CYCLE_25MHZ/4)
		  osc_clk1 = 1;  forever #(`CYCLE_25MHZ/2)
		  osc_clk1 = ~osc_clk1;
  end
`endif   
   
reg irig_b;

initial begin
  irig_b = 0;
  resetn = 0;
  force top_pci32.system_rst = resetn; 
  #(`CYCLE_25KHZ)   resetn = 1;  
	  
  // Error Pulse irig_b0
  repeat (2) begin
  #(`CYCLE_33KHZ)   irig_b = 1;
  #(`CYCLE_33KHZ)   irig_b = ~irig_b;
  end
  #(`CYCLE_33KHZ)   irig_b = 1;
  #(`CYCLE_33KHZ)   irig_b = 0;  
  #(`CYCLE_01KHZ)
  
  //  Low pulse Fair
                    irig_p_format;         // Old end poaition
		            irig_p_format;
                    irig_b = 1;	
  #(`CYCLE_01KHZ*5) irig_b = 0;
  #(`CYCLE_01KHZ)   irig_b = 1;

  // High pulse Fair
                    irig_p_format;
		            irig_p_format;
                    irig_b = 1;	
  #(`CYCLE_01KHZ*6) irig_b = 0;
  #(`CYCLE_01KHZ*5) irig_b = 1;	

  
  #(`CYCLE_01KHZ)   irig_b = 0;
  #(`CYCLE_01KHZ)   irig_p_format;
                    irig_transfer;
  
  #(`CYCLE_01KHZ*10)irig_b = 0;		
  
  #2000;      
  $finish;  
end

task irig_p_format;
  begin
    irig_b = 1; #(`CYCLE_01KHZ*8) #1
    irig_b = 0; #(`CYCLE_01KHZ*2) #1
	irig_b = irig_b;
  end
endtask

task irig_initial_dat;
     reg   [7:0] init_irig_dat;
     integer     j;
begin
  init_irig_dat = IRIG_PATTERN[7:0];
  j= 0;
  irig_b = 1;
  repeat(8) begin
    if (init_irig_dat[j]) begin
      #(`CYCLE_01KHZ*5) #1 irig_b = 0;
      #(`CYCLE_01KHZ*5) #1 irig_b = 1;
    end
    else begin
      #(`CYCLE_01KHZ*2) #1 irig_b = 0;
      #(`CYCLE_01KHZ*8) #1 irig_b = 1;
    end
  j = j + 1;
  end
end
endtask

task irig_normal_dat;
     input [3:0] irig_dat_group;
     reg   [8:0] normal_irig_dat;
     integer     k;
begin
  k = 0;
  case(irig_dat_group)
    4'd0 : normal_irig_dat = IRIG_PATTERN[16: 8];
    4'd1 : normal_irig_dat = IRIG_PATTERN[25:17];
    4'd2 : normal_irig_dat = IRIG_PATTERN[34:26];
    4'd3 : normal_irig_dat = IRIG_PATTERN[43:35];
    4'd4 : normal_irig_dat = IRIG_PATTERN[52:44];
    4'd5 : normal_irig_dat = IRIG_PATTERN[61:53];
    4'd6 : normal_irig_dat = IRIG_PATTERN[70:62];
    4'd7 : normal_irig_dat = IRIG_PATTERN[79:71];
    4'd8 : normal_irig_dat = IRIG_PATTERN[88:80];	  
endcase  

  irig_b = 1;
  repeat(9) begin
    if (normal_irig_dat[k]) begin
      #(`CYCLE_01KHZ*5) #1 irig_b = 0;
      #(`CYCLE_01KHZ*5) #1 irig_b = 1;
    end
    else begin
      #(`CYCLE_01KHZ*2) #1 irig_b = 0;
      #(`CYCLE_01KHZ*8) #1 irig_b = 1;
    end
    k = k + 1;
  end
end
endtask

task irig_transfer;
integer i;
  begin
    irig_b = 0;
    irig_p_format;
    irig_initial_dat;
    irig_p_format;

    for (i=0; i<9; i=i+1) begin
      irig_normal_dat(i);
      irig_p_format;
    end
  end
endtask   

top_pci32 top_pci32(
   // PCI interface
     .perrn     (perrn),
     .devseln   (devseln),
     .trdyn     (trdyn),
     .stopn     (stopn),
     .serrn     (serrn),
     .intan     (intan),

   // Output Module Port
     .outport0_o(),
     .outport1_o(),
     .outport2_o(),
     .outport3_o(),
     .outport4_o(),
     .outport5_o(),
     .outport6_o(),
     .outport7_o(),
     .outport8_o(),
     .outport9_o(),
     .outporta_o(),
     .outportb_o(),
     .outportc_o(),
     .outportd_o(),
     .outporte_o(),
     .outportf_o(),

   // LED Output Port
     .ledport0_o(),
     .ledport1_o(),
     .ledport2_o(),
     .ledport3_o(),
     .ledport4_o(),
     .ledport5_o(),
     .ledport6_o(),
     .ledport7_o(),

     .ad        (ad      ),
     .par       (par     ),  
     .cben      (cben    ),
     .framen    (framen  ),
     .irdyn     (irdyn   ),
     .idsel     (idsel   ),
     .clk       (clk     ),   
     .rstn      (resetn  ),	 

   // Input Module Port
     .inport0_i (irig_b  ),
     .inport1_i (irig_b  ),
     .inport2_i (        ),
     .inport3_i (        ),
     .inport4_i (        ),
     .inport5_i (        ),
     .inport6_i (        ),
     .inport7_i (        ),
     .inport8_i (1'b0    ),
     .inport9_i (1'b0    ),
     .inporta_i (1'b0    ),
     .inportb_i (1'b0    ),
     .inportc_i (1'b0    ),
     .inportd_i (1'b0    ),
     .inporte_i (1'b0    ),
     .inportf_i (1'b0    ),
            
     .osc_clk1  (osc_clk1)); 
   
   mstr_tranx u4 (.clk(clk),
                  .rstn(),
                  .ad(ad),
                  .cben(cben),
                  .par(par),
                  .reqn(mstr_tranx_reqn),
                  .gntn(1'b0),
                  .framen(framen),
                  .irdyn(irdyn),
                  .devseln(devseln),
                  .trdyn(trdyn),
                  .stopn(stopn),
                  .perrn(perrn),
                  .serrn(serrn),
		          .intan(intan),
                  .busfree(busfree),
                  .disengage_mstr(disengage_mstr),
                  .tranx_success(tranx_success),
                  .trgt_tranx_disca(trgt_tranx_disca),
                  .trgt_tranx_discb(trgt_tranx_discb),
                  .trgt_tranx_retry(trgt_tranx_retry),
		          .switch(switch)); 
   
      monitor u6 (.clk(clk),
               .rstn(resetn),
               .ad(ad),
               .cben(cben),
               .framen(framen),
               .irdyn(irdyn),
               .devseln(devseln),
               .trdyn(trdyn),
               .stopn(stopn),
               .busfree(busfree),
               .disengage_mstr(disengage_mstr),
               .tranx_success(tranx_success));

endmodule
