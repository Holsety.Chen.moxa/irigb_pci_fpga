// Filename        : top_pci32.v
// Description     : This is the top level of PCI Reference
// design. This module instaniates the 32-bit/33 MHz PCI Target core
// (t32) and the module top_local which is the top level of local
// design
// This design implements 2 Base address registers
// BAR#   Mapping     Size        Address Offset      Description
// BAR0   Memory      4 Kbytes    0000_0FFF           Maps to the 128Kbyte
//                                                    Async. SRAM
// BAR1                                               Maps to the
//                                                    various peripherals

`include "define.v"

module top_pci32 (
// PCI interface
   output wire        perrn,
   output wire        devseln,
   output wire        trdyn,
   output wire        stopn,
   output wire        serrn,
   output wire        intan,

   // Output Module Port
   output wire        outport0_o,
   output wire        outport1_o,
   output wire        outport2_o,
   output wire        outport3_o,
   output wire        outport4_o,
   output wire        outport5_o,
   output wire        outport6_o,
   output wire        outport7_o,
   output wire        outport8_o,
   output wire        outport9_o,
   output wire        outporta_o,
   output wire        outportb_o,
   output wire        outportc_o,
   output wire        outportd_o,
   output wire        outporte_o,
   output wire        outportf_o,

   // LED Output Port
   output wire        ledport0_o,
   output wire        ledport1_o,
   output wire        ledport2_o,
   output wire        ledport3_o,
   output wire        ledport4_o,
   output wire        ledport5_o,
   output wire        ledport6_o,
   output wire        ledport7_o,


   inout  wire [31:0] ad,
   inout  wire        par,

   input  wire [ 3:0] cben,
   input  wire        framen,
   input  wire        irdyn,
   input  wire        idsel,
   input  wire        clk,
   input  wire        rstn,

   // Input Module Port
   input  wire        inport0_i,
   input  wire        inport1_i,
   input  wire        inport2_i,
   input  wire        inport3_i,
   input  wire        inport4_i,
   input  wire        inport5_i,
   input  wire        inport6_i,
   input  wire        inport7_i,
   input  wire        inport8_i,
   input  wire        inport9_i,
   input  wire        inporta_i,
   input  wire        inportb_i,
   input  wire        inportc_i,
   input  wire        inportd_i,
   input  wire        inporte_i,
   input  wire        inportf_i,

   input  wire        osc_clk1
   );

// Reset Control Interface
  wire        loopbacken;
  wire        system_rst;
  wire [31:0] rst_status;
  wire        rst_ready;

// Loop Bcak Counter
  wire [31:0] loopbackcnt;
  wire        looppps_out;

// Internal PCI Target Wire
  wire [31:0] l_adi;
  wire        lt_rdyn;
  wire        lirqn;
  wire [31:0] l_adro;
  wire [31:0] l_dato;
  wire [3:0]  l_beno;
  wire [3:0]  l_cmdo;
  wire        lt_framen;
  wire        lt_ackn;
  wire        lt_dxfrn;
  wire [11:0] lt_tsr;

// PCI BAR0 Memory Port-a Interface
  wire [`BAR0_ADW-1:0] sram_addr0;

  wire [31:0] sram_din0;
  wire [31:0] sram_dout0;
  wire        sram_wen0;
  wire        sram_ren0;

// PCI BAR0 Memory Port-b Interface
  wire [`BAR0_ADW-1:0] sram_addr1;
  wire [31:0] sram_din1;
  wire        sram_wen1;

// Pulse Generator Interface
  wire [31:0] fifo_config;
  wire        fifo_pulse;

// Interrupt Status Register
  wire [31:0] irqs_mask;
  wire [31:0] irqs_data;
  wire        ppsdec_done;
  wire        ppsdec_fair;
  wire        encpps_done;

// Input Decoder Interface
  wire        irigb0_input;
  wire        irigb1_input;
  wire        ppsdec_input;
  wire        loopps_input;

// PPS Encoder
  wire [31:0] setpps_wid;
  wire [31:0] pps_timeout;
  wire        encode_pps;

// IRIG Encoder Interface
  wire        encode_irig;
  wire        encirig_done;

// IRIG-B Interface
  wire [ 3:0] irig0_drdy,
              irig1_drdy;
  wire [88:0] irig0_data,
              irig1_data;

  wire [31:0] irig0_fcnt,
              irig1_fcnt;

  wire        irig0_fine,
              irig1_fine;
  
  wire        irig0_off, irig0_fair, irig0_par_err, irig0_right,
              irig1_off, irig1_fair, irig1_par_err, irig1_right;

// RTC controller
  wire [ 2:0] rtc_drdy;
  wire [31:0] rtc_dat0;
  wire [31:0] rtc_dat1;
  wire [31:0] rtc_dat2;
  wire [31:0] rtc_dat3;
  wire        rtc_irig_rdy;
  wire [88:0] rtc_irig_dat;
  wire        second_hit;

  reg    irig0_par_set ;
  wire v_irig0_par_set = (sram_wen0 && (sram_addr0 == `PCI_TMCON))
                       ? sram_din0[0] : irig0_par_set;
  reg    irig1_par_set ;
  wire v_irig1_par_set = (sram_wen0 && (sram_addr0 == `PCI_TMCON))
                       ? sram_din0[1] : irig1_par_set;
  reg    enc_set_par;
  wire v_enc_set_par   = (sram_wen0 && (sram_addr0 == `PCI_TMCON))
                       ? sram_din0[2] : enc_set_par;
  reg    irig0_non_par;
  wire v_irig0_non_par = (sram_wen0 && (sram_addr0 == `PCI_TMCON))
                       ? sram_din0[3] : irig0_non_par;
  reg    irig1_non_par;
  wire v_irig1_non_par = (sram_wen0 && (sram_addr0 == `PCI_TMCON))
                       ? sram_din0[4] : irig1_non_par;

  wire [31:0] parity_table = {27'd0, irig1_non_par, irig0_non_par, enc_set_par,
                                     irig1_par_set, irig0_par_set};

  always @ (posedge clk, negedge rstn) begin
    if (~rstn) begin
      irig0_par_set <= 1'b0;
      irig1_par_set <= 1'b0;
      enc_set_par   <= 1'b0;
      irig0_non_par <= 1'b0;
      irig1_non_par <= 1'b0;
    end
    else begin
      irig0_par_set <= v_irig0_par_set;
      irig1_par_set <= v_irig1_par_set;
      enc_set_par   <= v_enc_set_par;
      irig0_non_par <= v_irig0_non_par;
      irig1_non_par <= v_irig1_non_par;
    end
  end

  // Device Order Of Write To Sram
  reg  [1:0]   write_order;
  wire [1:0] v_write_order = write_order + 1'b1;

  always @ (posedge osc_clk1, negedge system_rst) begin
    if (~system_rst) write_order <= 2'b00;
    else             write_order <= v_write_order;
  end

  // Set GPIO Output Value
  wire [7:0] outpin;
  assign outport0_o = outpin[0];
  assign outport1_o = outpin[1];
  assign outport2_o = outpin[2];
  assign outport3_o = outpin[3];
  assign outport4_o = outpin[4];
  assign outport5_o = outpin[5];
  assign outport6_o = outpin[6];
  assign outport7_o = outpin[7];

  wire v_setport_hit = (sram_wen0 && (sram_addr0 == `PORTDAT));

  reg  [15:0]   gpio_data;
  wire [15:0] v_gpio_data = v_setport_hit ? sram_din0[31:16] : gpio_data;

  always @ (posedge clk, negedge rstn) begin
    if (~rstn) gpio_data <= 16'hff00;
    else       gpio_data <= v_gpio_data;
  end

  wire [31:0] port_data = {gpio_data, inportf_i, inporte_i, inportd_i, inportc_i,
                                      inportb_i, inporta_i, inport9_i, inport8_i,
                                      inport7_i, inport6_i, inport5_i, inport4_i,
                                      inport3_i, inport2_i, inport1_i, inport0_i};

  assign outport8_o = gpio_data[ 8];
  assign outport9_o = gpio_data[ 9];
  assign outporta_o = gpio_data[10];
  assign outportb_o = gpio_data[11];
  assign outportc_o = gpio_data[12];
  assign outportd_o = gpio_data[13];
  assign outporte_o = gpio_data[14];
  assign outportf_o = gpio_data[15];

  // PCI Read Setting
  reg    loopbackcnt_ren; // Offset 0x0C
  reg    set_portdat_ren; // Offset 0x18
  reg    irig0_count_ren; // Offset 0x2C
  reg    irig1_count_ren; // Offset 0x40
  reg    sec_ppwidth_ren; // Offset 0x44
  reg    irig_parity_ren; // Offset 0x48
  reg    irqn_status_ren; // Offset 0x4C
  reg    pci_irqmask_ren; // Offset 0x50
  reg    pps_timeout_ren; // Offset 0x54
  reg    rtc_dat2cnt_ren; // Offset 0x64
  reg    serial_fifo_ren; // Offset 0x74

  wire v_loopbackcnt_ren = (sram_ren0 && (sram_addr0 == `LPBTCNT     ));
  wire v_set_portdat_ren = (sram_ren0 && (sram_addr0 == `PORTDAT     ));
  wire v_irig0_count_ren = (sram_ren0 && (sram_addr0 == `IRIG0CNT    ));
  wire v_irig1_count_ren = (sram_ren0 && (sram_addr0 == `IRIG1CNT    ));
  wire v_sec_ppwidth_ren = (sram_ren0 && (sram_addr0 == `PPSCON      ));
  wire v_irig_parity_ren = (sram_ren0 && (sram_addr0 == `PCI_TMCON   ));
  wire v_irqn_status_ren = (sram_ren0 && (sram_addr0 == `PCI_INTSTS  ));
  wire v_pci_irqmask_ren = (sram_ren0 && (sram_addr0 == `PCI_INTMSK  ));
  wire v_pps_timeout_ren = (sram_ren0 && (sram_addr0 == `PPSDETIMEOUT));
  wire v_rtc_dat2cnt_ren = (sram_ren0 && (sram_addr0 == `RTCDAT2     ));
  wire v_serial_fifo_ren = (sram_ren0 && (sram_addr0 == `SFIFOCON    ));

  reg  [31:0]   pci_read_dat;
  wire [31:0] v_pci_read_dat = loopbackcnt_ren ? loopbackcnt
                             : set_portdat_ren ? port_data
                             : irig0_count_ren ? irig0_fcnt
                             : irig1_count_ren ? irig1_fcnt
                             : sec_ppwidth_ren ? setpps_wid
                             : irig_parity_ren ? parity_table
                             : irqn_status_ren ? irqs_data
                             : pci_irqmask_ren ? irqs_mask
                             : pps_timeout_ren ? pps_timeout
                             : rtc_dat2cnt_ren ? rtc_dat2
                             : serial_fifo_ren ? fifo_config
                             : sram_ren0       ? sram_dout0 : pci_read_dat;

  always @ (posedge clk, negedge rstn) begin
    if (~rstn) begin
      loopbackcnt_ren <=  1'b0;
      set_portdat_ren <=  1'b0;
      irig0_count_ren <=  1'b0;
      irig1_count_ren <=  1'b0;
      sec_ppwidth_ren <=  1'b0;
      irig_parity_ren <=  1'b0;
      irqn_status_ren <=  1'b0;
      pci_irqmask_ren <=  1'b0;
      pps_timeout_ren <=  1'b0;
      rtc_dat2cnt_ren <=  1'b0;
      serial_fifo_ren <=  1'b0;
      pci_read_dat    <= 32'd0;
    end
    else begin
      loopbackcnt_ren <= v_loopbackcnt_ren;
      set_portdat_ren <= v_set_portdat_ren;
      irig0_count_ren <= v_irig0_count_ren;
      irig1_count_ren <= v_irig1_count_ren;
      sec_ppwidth_ren <= v_sec_ppwidth_ren;
      irig_parity_ren <= v_irig_parity_ren;
      irqn_status_ren <= v_irqn_status_ren;
      pci_irqmask_ren <= v_pci_irqmask_ren;
      pps_timeout_ren <= v_pps_timeout_ren;
      rtc_dat2cnt_ren <= v_rtc_dat2cnt_ren;
      serial_fifo_ren <= v_serial_fifo_ren;
      pci_read_dat    <= v_pci_read_dat;
    end
  end

reset_ctrl rst_ctrl(
   .loopbacken_o   (loopbacken),
   .system_rst_o   (system_rst),
   .rst_status_o   (rst_status),
   .rst_ready_o    (rst_ready),

   .offset_ad_i    (sram_addr0),
   .offset_wr_i    (sram_wen0 ),
   .write_dat_i    (sram_din0 ),
   .pciclk_i       (clk       ),
   .sysclk_i       (osc_clk1  )
);

// 32-bit Target Core
t32 core (
   .clk            (clk),
   .rstn           (rstn),
   .idsel          (idsel),
   .l_adi          (l_adi),
   .lt_rdyn        (lt_rdyn),
   .lt_abortn      (1'b1),
   .lt_discn       (1'b1),
   .lirqn          (lirqn),
   .cben           (cben),
   .framen         (framen),
   .irdyn          (irdyn),
   .intan          (intan),
   .serrn          (serrn),
   .l_adro         (l_adro),
   .l_dato         (l_dato),
   .l_beno         (l_beno),
   .l_cmdo         (l_cmdo),
   .lt_framen      (lt_framen),
   .lt_ackn        (lt_ackn),
   .lt_dxfrn       (lt_dxfrn),
   .lt_tsr         (lt_tsr),
   .cmd_reg        (),
   .stat_reg       (),
   .perrn          (perrn),
   .devseln        (devseln),
   .trdyn          (trdyn),
   .stopn          (stopn),
   .ad             (ad),
   .par            (par));

top_local top_local (
  // Outputs
   .l_adi          (l_adi     ),
   .lt_rdyn        (lt_rdyn   ),
   .sram0_wen_o    (sram_wen0 ),
   .sram0_ren_o    (sram_ren0 ),
   .sram0_din_o    (sram_din0 ),
   .sram0_adr_o    (sram_addr0),

   .bar0_dout_i    (pci_read_dat),
   .l_adro         (l_adro    ),
   .l_dato         (l_dato    ),
   .l_cmdo         (l_cmdo    ),
   .lt_framen      (lt_framen ),
   .lt_ackn        (lt_ackn   ),
   .lt_dxfrn       (lt_dxfrn  ),
   .lt_tsr         (lt_tsr    ),
   .clk            (clk       ),
   .rstn           (rstn      ));

pci_mem pci_mem(
   .address_a      (sram_addr0),
   .address_b      (sram_addr1),
   .clock_a        (clk       ),
   .clock_b        (osc_clk1  ),
   .data_a         (sram_din0 ),
   .data_b         (sram_din1 ),
   .rden_a         (sram_ren0 ),
   .rden_b         (1'b0      ),
   .wren_a         (sram_wen0 ),
   .wren_b         (sram_wen1 ),
   .q_a            (sram_dout0),
   .q_b            ());

input_decoder in_decoder(
   .irigb0_o       (irigb0_input),
   .irigb1_o       (irigb1_input),
   .ppsdec_o       (ppsdec_input),
   .loopps_o       (loopps_input),

   // Local Bus address
   .offset_ad_i    (sram_addr0),
   .offset_wr_i    (sram_wen0 ),
   .write_dat_i    (sram_din0 ),

   // Input pin
   .inport0_i      (inport0_i ),
   .inport1_i      (inport1_i ),
   .inport2_i      (inport2_i ),
   .inport3_i      (inport3_i ),
   .inport4_i      (inport4_i ),
   .inport5_i      (inport5_i ),
   .inport6_i      (inport6_i ),
   .inport7_i      (inport7_i ),

   .pciclk_i       (clk       ),
   .rstn_i         (system_rst));

output_config out_config(
   .outpin_o       (outpin     ),

   .offset_ad_i    (sram_addr0 ),
   .offset_wr_i    (sram_wen0  ),
   .write_dat_i    (sram_din0  ),
   // Input pin
   .inport0_i      (inport0_i  ),
   .inport1_i      (inport1_i  ),
   .inport2_i      (inport2_i  ),
   .inport3_i      (inport3_i  ),
   .inport4_i      (inport4_i  ),
   .inport5_i      (inport5_i  ),
   .inport6_i      (inport6_i  ),
   .inport7_i      (inport7_i  ),
   .encode_irig_i  (encode_irig),
   .encode_pps_i   (encode_pps ),
   .fifo_pulse_i   (fifo_pulse ),
   .loopbackpps_i  (looppps_out),
   .gpio_data_i    (gpio_data[7:0]),

   .pciclk_i       (clk        ),
   .rstn_i         (system_rst ));

irig_decoder #(2'B00) irig_decoder0
(
   .irig_drdy_o    (irig0_drdy   ),
   .irig_data_o    (irig0_data   ),
   .irig_fcnt_o    (irig0_fcnt   ),
   .irig_fine_o    (irig0_fine   ),
   .irig_off_o     (irig0_off    ),
   .irig_fair_o    (irig0_fair   ),
   .parity_err_o   (irig0_par_err),
   .irig_right_o   (irig0_right  ),

   .ram_order_i    (write_order  ),
   .parity_set_i   (irig0_par_set),     // 0: EVEN, 1: Odd
   .non_parity_i   (irig0_non_par),
   .irig_i         (irigb0_input ),
   .sysclk_i       (osc_clk1     ),
   .resetn_i       (system_rst   ));

irig_decoder #(2'B01) irig_decoder1
(
   .irig_drdy_o    (irig1_drdy   ),
   .irig_data_o    (irig1_data   ),
   .irig_fcnt_o    (irig1_fcnt   ),
   .irig_fine_o    (irig1_fine   ),
   .irig_off_o     (irig1_off    ),
   .irig_fair_o    (irig1_fair   ),
   .parity_err_o   (irig1_par_err),
   .irig_right_o   (irig1_right  ),

   .ram_order_i    (write_order  ),
   .parity_set_i   (irig1_par_set),     // 0: EVEN, 1: Odd
   .non_parity_i   (irig1_non_par),
   .irig_i         (irigb1_input ),
   .sysclk_i       (osc_clk1     ),
   .resetn_i       (system_rst   ));

system_sram_ctrl system_sram_ctrl(
   // For sram interface
   .sram_adr_o     (sram_addr1),
   .sram_wen_o     (sram_wen1 ),
   .sram_din_o     (sram_din1 ),

   .rst_ready_i    (rst_ready ),
   .rst_status_i   (rst_status),

   // IRIG0-B
   .irig0_drdy_i   (irig0_drdy),
   .irig0_data_i   (irig0_data),

   // IRIG1-B
   .irig1_drdy_i   (irig1_drdy),
   .irig1_data_i   (irig1_data),

   // RTC Control
   .rtc_drdy_i     (rtc_drdy  ),
   .rtc_dat0_i     (rtc_dat0  ),
   .rtc_dat1_i     (rtc_dat1  ),
   .rtc_dat3_i     (rtc_dat3  ),

   .sysclk_i       (osc_clk1  ),
   .rstn_i         (system_rst));

// ------------------------------------------------------------
// Pulse Generator
// ------------------------------------------------------------
pulse_generator pulse_gen(
   .fifo_config_o  (fifo_config),
   .fifo_pulse_o   (fifo_pulse ),

   // Local Bus address
   .offset_ad_i    (sram_addr0),
   .offset_wr_i    (sram_wen0 ),
   .write_dat_i    (sram_din0 ),
   .pciclk_i       (clk       ),
   .sysclk_i       (osc_clk1  ),
   .rstn_i         (system_rst));

// ------------------------------------------------------------
// Interrupt Regisetr Control
// ------------------------------------------------------------
interrupt_ctrl irq_ctrl(
   .irqs_mask_o    (irqs_mask    ),
   // Interrupt Register Output
   .irqs_data_o    (irqs_data    ),
   .interrupt_o    (lirqn        ),

   // IRIG0-B Interrupt
   .irig0_off_i    (irig0_off    ),
   .irig0_fair_i   (irig0_fair   ),
   .irig0_perr_i   (irig0_par_err),
   .irig0_right_i  (irig0_right  ),

   // IRIG1-B Interrupt
   .irig1_off_i    (irig1_off    ),
   .irig1_fair_i   (irig1_fair   ),
   .irig1_perr_i   (irig1_par_err),
   .irig1_right_i  (irig1_right  ),

   .ppsdec_fair_i  (ppsdec_fair  ),
   .ppsdec_done_i  (ppsdec_done  ),
   .ppsenc_done_i  (encpps_done  ),
   .irigenc_done_i (encirig_done ),

   // Local Bus address
   .offset_ad_i    (sram_addr0   ),
   .offset_rd_i    (sram_ren0    ),
   .offset_wr_i    (sram_wen0    ),
   .write_dat_i    (sram_din0    ),

   .pciclk_i       (clk          ),
   .rstn_i         (system_rst & rstn));

// ------------------------------------------------------------
// Loop Back Pulse Counter
// ------------------------------------------------------------
loopback_cnt lb_cnt(
   .loopbackcnt_o  (loopbackcnt),
   .loop_dout_o    (looppps_out),

   .loop_din_i     (loopps_input),
   .loopbacken_i   (loopbacken  ),
   .sysclk_i       (osc_clk1    ),
   .rstn_i         (system_rst  ));

// ------------------------------------------------------------
// PPS Codec
// ------------------------------------------------------------
pps_codec pps_codec(
   .setpps_wid_o   (setpps_wid ),
   .pps_timeout_o  (pps_timeout),
   .decode_pps_o   (ppsdec_done),
   .decpps_err_o   (ppsdec_fair),
   .encode_pps_o   (encode_pps ),
   .encpps_end_o   (encpps_done),

   // Local Bus address
   .offset_ad_i    (sram_addr0  ),
   .offset_wr_i    (sram_wen0   ),
   .write_dat_i    (sram_din0   ),

   .decode_pps_i   (ppsdec_input),
   .second_hit_i   (second_hit  ),
   .pciclk_i       (clk         ),
   .sysclk_i       (osc_clk1    ),
   .rstn_i         (system_rst  ));

// ------------------------------------------------------------
// RTC Controller
// ------------------------------------------------------------
rtc_control #(2'B10) rtc_ctrl
(
   .rtc_drdy_o     (rtc_drdy  ),
   .rtc_dat0_o     (rtc_dat0  ),
   .rtc_dat1_o     (rtc_dat1  ),
   .rtc_dat2_o     (rtc_dat2  ),
   .rtc_dat3_o     (rtc_dat3  ),
   .rdy_stream_o   (rtc_irig_rdy),
   .rtc_stream_o   (rtc_irig_dat),
   .second_hit_o   (second_hit),

   .irig0_end_i    (irig0_fine ),
   .irig0_dat_i    (irig0_data ),
   .irig1_end_i    (irig1_fine ),
   .irig1_dat_i    (irig1_data ),

   .pps_decode_i   (ppsdec_done),
   .decpps_err_i   (ppsdec_fair),
   // Local Bus address
   .offset_ad_i    (sram_addr0 ),
   .offset_wr_i    (sram_wen0  ),
   .write_dat_i    (sram_din0  ),
   .ram_order_i    (write_order),
   .pciclk_i       (clk         ),
   .sysclk_i       (osc_clk1    ),
   .rstn_i         (system_rst  ));

// ------------------------------------------------------------
// IRIG Encoder
// ------------------------------------------------------------
irig_encoder irig_encoder(
   .encode_irig_o  (encode_irig ),
   .encirig_end_o  (encirig_done),

   .enc_set_par_i  (enc_set_par ),
   .rtc_irigrdy_i  (rtc_irig_rdy),
   .rtc_irigdat_i  (rtc_irig_dat),
   .sysclk_i       (osc_clk1    ),
   .rstn_i         (system_rst  ));

// ------------------------------------------------------------
// NLED Output
// ------------------------------------------------------------
  wire [7:0] nled_output;
  assign ledport0_o = nled_output[0];
  assign ledport1_o = nled_output[1];
  assign ledport2_o = nled_output[2];
  assign ledport3_o = nled_output[3];
  assign ledport4_o = nled_output[4];
  assign ledport5_o = nled_output[5];
  assign ledport6_o = nled_output[6];
  assign ledport7_o = nled_output[7];

ledport_setting nled(
   .ledport_o      (nled_output),

   .offset_ad_i    (sram_addr0  ),
   .offset_wr_i    (sram_wen0   ),
   .write_dat_i    (sram_din0   ),

   .outport_i      (outpin[5:0] ),
   .inport0_i      (inport0_i   ),
   .inport1_i      (inport1_i   ),
   .inport2_i      (inport2_i   ),
   .inport3_i      (inport3_i   ),
   .inport4_i      (inport4_i   ),
   .inport5_i      (inport5_i   ),
   .inport6_i      (inport6_i   ),
   .inport7_i      (inport7_i   ),
   .irig0_end_i    (irig0_right ),
   .irig1_end_i    (irig1_right ),
   .pciclk_i       (clk         ),
   .sysclk_i       (osc_clk1    ),
   .rstn_i         (system_rst  ));


endmodule // top_t32


