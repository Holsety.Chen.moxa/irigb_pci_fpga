
State Machine - |top_pci32|irig_decoder:irig_decoder1|fcnt_state
Name fcnt_state.00 fcnt_state.10 fcnt_state.01 
fcnt_state.00 0 0 0 
fcnt_state.01 1 0 1 
fcnt_state.10 1 1 0 

State Machine - |top_pci32|irig_decoder:irig_decoder1|irig_state
Name irig_state.110 irig_state.101 irig_state.100 irig_state.011 irig_state.010 irig_state.001 irig_state.000 
irig_state.000 0 0 0 0 0 0 0 
irig_state.001 0 0 0 0 0 1 1 
irig_state.010 0 0 0 0 1 0 1 
irig_state.011 0 0 0 1 0 0 1 
irig_state.100 0 0 1 0 0 0 1 
irig_state.101 0 1 0 0 0 0 1 
irig_state.110 1 0 0 0 0 0 1 

State Machine - |top_pci32|irig_decoder:irig_decoder0|fcnt_state
Name fcnt_state.00 fcnt_state.10 fcnt_state.01 
fcnt_state.00 0 0 0 
fcnt_state.01 1 0 1 
fcnt_state.10 1 1 0 

State Machine - |top_pci32|irig_decoder:irig_decoder0|irig_state
Name irig_state.110 irig_state.101 irig_state.100 irig_state.011 irig_state.010 irig_state.001 irig_state.000 
irig_state.000 0 0 0 0 0 0 0 
irig_state.001 0 0 0 0 0 1 1 
irig_state.010 0 0 0 0 1 0 1 
irig_state.011 0 0 0 1 0 0 1 
irig_state.100 0 0 1 0 0 0 1 
irig_state.101 0 1 0 0 0 0 1 
irig_state.110 1 0 0 0 0 0 1 

State Machine - |top_pci32|top_local:top_local|pci_sram_ctrl:bar0_ctrl|sram_state
Name sram_state.0000 sram_state.0100 sram_state.0010 sram_state.0001 
sram_state.0000 0 0 0 0 
sram_state.0001 1 0 0 1 
sram_state.0010 1 0 1 0 
sram_state.0100 1 1 0 0 
